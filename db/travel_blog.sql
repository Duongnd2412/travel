INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (1, 'Vé ưu đãi cho các chặng này được mở bán với mức giá từ 49.000 đồng/chiều (chưa bao gồm thuế, phí). Tất cả các đường bay kể trên đều được Bamboo Airways khai thác bằng máy bay A320 hiện đại.
Ngoài ra, từ 14/1/2021 hành khách thân yêu của Bamboo Airways đã có thể đặt vé bay từ TP.HCM đến Tuy Hòa (Phú Yên) với giá chính thức chỉ từ 49.000 đồng/1 chặng (chưa bao gồm thuế, phí).
Với đường bay này, Tết này người dân thành phố mang tên Bác lại có thêm lựa chọn để tạm rời xa phồn hoa sôi động đến với phố biển Tuy Hòa, miền hoa vàng trên cỏ xanh để nghỉ ngơi, thưởng thức vị ngọt ngào của những ngày đầu năm mới bên người thân yêu.', 'sang', '2021-01-15', 'Từ ngày 20/1/2021, hành khách thân yêu của Bamboo Airways sẽ không còn phải mong đợi thêm nữa để có thể bay thẳng đến Huế, Chu Lai và Tuy Hòa từ Hà Nội.', 'máy bay', 'sang', '2021-01-14', 'ACTIVE', '/images/blog-1.jpg', 'Bamboo Airways mở bán vé chặng Hà Nội – Huế, Chu Lai, Tuy Hòa chỉ từ 49k', 'https://www.ivivu.com/blog/2021/01/bamboo-airways-mo-ban-ve-chang-ha-noi-hue-chu-lai-tuy-hoa-chi-tu-49k/', 1);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (2, 'Bí quyết làm thịt viên bọc trứng sốt cà chua chuẩn vị nhà hàng
Nguyên liệu:

- Heo xay 200g

- Trứng cút 4 quả

- Cà chua 100g

- Gia vị: Hành lá, củ hành tím, tỏi, ớt, muối, đường, hạt nêm,…', 'sang', '2021-01-09', 'Bí quyết làm thịt viên bọc trứng sốt cà chua chuẩn vị nhà hàng
Nguyên liệu:

- Heo xay 200g

- Trứng cút 4 quả

- Cà chua 100g

- Gia vị: Hành lá, củ hành tím, tỏi, ớt, muối, đường, hạt nêm,…', 'ẩm thực', 'sang', '2021-01-19', 'ACTIVE', '/images/blog-2.jpg', 'Bí quyết làm thịt viên bọc trứng sốt cà chua chuẩn vị nhà hàng
', 'https://www.ivivu.com/blog/2021/01/bi-quyet-lam-thit-vien-boc-trung-sot-ca-chua-chuan-vi-nha-hang/', 1);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (3, 'Bước 1: Làm đậu rán
Đậu hũ sau khi làm sạch thì cắt thành miếng vuông nhỏ vừa ăn. Cho đậu hũ vào chảo dầu ngập nóng già, chiên cho vàng giòn một mặt rồi mới lật miếng đậu chiên tiếp mặt còn lại. Cách này sẽ giúp chiên không bị dính đáy chảo và đậu hũ có độ giòn, vàng đều giúp món đậu rán, cà pháo mắm tôm ngon hơn.
Bước 2: Muối cà pháo mắm tôm
Bạn sơ chế cà pháo bằng cách cắt sơ phần cuống, cắt làm đôi ngâm vào nước muối loãng 10 phút. Cuối cùng đổ ra rổ xả qua nước lạnh, để ráo.
Tất cả ớt, riềng, gừng, tỏi đem giã hay xay nhuyễn sau đó thêm một chút nước đun sôi để nguội vào. Đổ hỗn hợp ra tô rồi nêm nếm các gia vị lại cho vừa ăn. Sau đó, cho tiếp cà pháo vào hũ, đổ chén mắm tôm vào trộn đều tất cả. Đậy nắp kín để nơi thoáng mát vài giờ là có thể ăn ngon. Nếu không thích ăn cà muối xổi thì bạn để 2 – 3 ngày sau rồi bỏ ra ăn.
Món đậu rán, cà pháo mắm tôm thích hợp dùng kèm cùng cơm trắng vừa đậm đà, vừa kích thích khẩu vị vô cùng.
', 'thang', '2021-01-14', 'Đổi gió cho bữa tối với món đậu rán, cà pháo mắm tôm ngon khó cưỡng
Nguyên liệu:

- 4 miếng đậu hũ

- 140g cà pháo

- 40g mắm tôm

– Gia vị: ớt, riềng, gừng, tỏi, muối, đường…

', 'ẩm thực', 'thang', '2021-01-13', 'ACTIVE', '/images/blog-3.jpg', 'Đổi gió cho bữa tối với món đậu rán, cà pháo mắm tôm ngon khó cưỡng', 'https://www.ivivu.com/blog/2021/01/doi-gio-cho-bua-toi-voi-mon-dau-ran-ca-phao-mam-tom-ngon-kho-cuong/', 5);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (4, 'Ngay từ bây giờ bạn đã có thể chính thức đặt vé bay từ Thành phố mang tên Bác đến thành phố biển Miền Tây Rạch Giá rồi. Chỉ vài click thôi bạn đã có thể biến mình thành một trong những hành khách đầu tiên của Bamboo Airways trên chặng bay TP.HCM – Rạch Giá.
Theo đó, lịch bay chính thức các đường bay mới từ Sài Gòn – Rạch Giá có nội dung chi tiết như sau:
- Giai Đoạn Bay: 01/02/2021-27/03/2021
- Ngày Mở Bán: Từ 08/01/2021
- Giờ đi: 9:10
- Giờ đến: 10:10
- Tần suất: 7 chuyến/Tuần
- Tàu bay: E190
- Giá bán: Từ 299K/chiều/chưa bao gồm thuế phí 
Đặc biệt, trong tuần đầu khai trương đường bay mới, 1/2 – 8/2/2021, Bamboo Airways sẽ dành tặng khách hàng thêm 10kg hành lý kí gửi.
', 'sang', '2021-01-14', 'Hãng hàng không Bamboo Airways chính thức mở bán vé chặng TP.Hồ Chí Minh – Rạch Giá từ ngày 08/01/2021, nhân dịp khai trương đường bay mới, hãng mang đến chương trình ưu đãi hấp dẫn chỉ từ 299K/chiều/chưa bao gồm thuế phí. ', 'máy bay', 'sang', '2021-01-13', 'ACTIVE', '/images/blog-4.jpg', 'Bamboo Airways mở bán vé chặng TP. Hồ Chí Minh – Rạch Giá chỉ từ 299K/chiều', 'https://www.ivivu.com/blog/2021/01/bamboo-airways-mo-ban-ve-chang-tp-ho-chi-minh-rach-gia-chi-tu-299kchieu/', 1);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (5, 'Về Huế nghỉ dưỡng đậm chất Nhật Bản ở Kawara Mỹ An Onsen Resort
Kawara Mỹ An Onsen Resort với quy mô tổng diện tích 3,5ha, tọa lạc tại thôn Mỹ An, xã Phú Dương, huyện Phú Vang, tỉnh Thừa Thiên Huế, cách trung tâm thành phố Huế khoảng 7km. Khu phức hợp được xây dựng trên địa điểm của Khu du lịch Mỹ An cũ – nơi có nguồn nước khoáng nóng với nhiều khoáng chất đặc trưng bởi hàm lượng lưu huỳnh.
Kawara Mỹ An Onsen Resort là khu nghỉ dưỡng phức hợp đạt tiêu chuẩn quốc tế với quy mô bao gồm khu onsen, khách sạn, biệt thự, nhà hàng, spa, khu giải trí và nhiều tiện ích khác. Đặc biệt, khu tắm suối khoáng nóng được xây dựng và vận hành khắt khe theo mô hình onsen Nhật Bản nổi tiếng.

Tại đây, du khách được đắm mình trong không gian thư giãn, chiêm ngưỡng lối kiến trúc đẹp thân thiện toát ra từ cách bố trí, phân khu chức năng đậm chất Cố đô Huế, tận hưởng nguồn suối khoáng với hàm lượng khoáng hóa cao và nhiều khoáng chất quý có tác dụng tích cực trong việc điều trị các bệnh về tim mạch, xương khớp và chăm sóc da.', 'sang', '2021-01-14', 'Nhắc đến tắm Onsen thì bạn nghĩ ngay đến đất nước mặt trời mọc – Nhật Bản. Nhưng giờ đây ngay cố đô Huế cũng có Kawara Mỹ An Onsen Resort kết hợp với tắm Onsen đúng chuẩn Nhật cho bạn khám phá đó.', 'hotel', 'sang', '2021-01-13', 'ACTIVE', '/images/blog-5.jpg', 'Về Huế nghỉ dưỡng đậm chất Nhật Bản ở Kawara Mỹ An Onsen Resort', 'https://www.ivivu.com/blog/2021/01/ve-hue-nghi-duong-dam-chat-nhat-ban-o-kawara-my-an-onsen-resort/', 1);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (6, 'Nếu bạn là một người yêu thích những món bánh nướng thơm ngon mà lại có không giang đẹp mắt thì Blue Dream Bread chính là lựa chọn tốt nhất. Đây cũng là một trong những quán cà phê Đà Lạt hút khách nhất vào dịp cuối năm. Bước chân vào bên trong quán, bạn sẽ ngay lập tức cảm nhận được hương thơm bơ sữa ngào ngạt, pha lẫn mùi cà phê ấm áp. Đặc biệt, tại quán còn có sẵn góc để cho bạn có thể tự do “sống ảo” với rất nhiều món đồ trang trí siêu đáng yêu nữa đấy!', 'sang', '2021-01-14', 'Đến hẹn lại lên, những quán cà phê Đà Lạt sau đây luôn là nơi hẹn hò lí tưởng dành cho những ai yêu thích mùa lễ hội.', 'ẩm thực', 'sang', '2021-01-19', 'ACTIVE', '/images/blog-6.jpg', '5 quán cà phê Đà Lạt ‘lột xác’ trong mùa lễ hội
', 'https://www.ivivu.com/blog/2021/01/5-quan-ca-phe-da-lat-lot-xac-trong-mua-le-hoi/', 1);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (7, 'Khách sạn Four Points by Sheraton Đà Nẵng 
Khách sạn Four Points by Sheraton Đà Nẵng là khách sạn 5 sao thuộc tập đoàn quản lý khách sạn hàng đầu thế giới Marriott. Khách sạn sở hữu một vẻ đẹp sang trọng cùng không gian tinh tế được chăm chút từng chi tiết một nhờ được thiết kế bởi top 5 kiến trúc sư hàng đầu thế giới. Nằm ngay mặt tiền đường Võ Nguyên Giáp, chỉ cách biển Mỹ Khê vài phút đi bộ và cách trung tâm thành phố khảng 5 phút đi taxi, khách sạn Four Points by Sheraton Đà Nẵng thật sự là lựa chọn lý tưởng cho các du khách muốn thưởng ngoạn trọn vẹn vẻ đẹp của thành phố biển Đà Nẵng.

Khách sạn Danang Golden Bay
Không chỉ dát vàng 24K toàn bộ nội thất, Khách sạn Danang Golden Bay còn sở hữu một hồ bơi vô cực, cho phép bạn ngắm toàn cảnh 360 độ Đà Nẵng từ trên cao với tầm nhìn trực diện ra 6 cây cầu biểu tượng của thành phố. Chưa hết, khách sạn còn có hẳn một công viên kỳ quan với mô hình thu nhỏ giúp bạn check-in khắp thế giới mà không cần phải đi đâu xa.
Khách sạn SALA Đà Nẵng Beach
Khách sạn SALA Đà Nẵng Beach tọa lạc ngay khu biển Mỹ Khê, cách sông Hàn và trung tâm thành phố Đà Nẵng chỉ từ 2km. Khách sạn gồm 162 phòng mang phong cách Nhật Bản, tầm nhìn trực diện biển, sẽ cho bạn trải nghiệm tuyệt vời khi lưu trú tại SALA.
Khách sạn Altara Suites by Ri-Yaz, Đà Nẵng
Altara Suites by Ri-Yaz sở hữu vị trí đắc địa khi tọa lạc ngay cạnh bãi biển Mỹ Khê xinh đẹp, cách sân bay khoảng 15 phút di chuyển bằng ô tô và gần nhiều điểm đến nổi tiếng tại Đà Nẵng. Đây chắc chắn sẽ là lựa chọn tuyệt vời cho kỳ nghỉ dưỡng của bạn.
Vinpearl Condotel Riverfront Đà Nẵng 
Sở hữu vị trí trung tâm đắc địa ngay quận Sơn Trà, Vinpearl Condotel Riverfront nổi bật trong những khách sạn Đà Nẵng sỡ hữu tầm nhìn ngoạn mục giúp bạn tha hồ chiêm ngưỡng toàn cảnh thành phố từ trên cao.
', 'sang', '2021-01-14', 'Với mức  giá hấp dẫn, phù hợp với nhiều du khách, top 5 khách sạn Đà Nẵng có hồ bơi vô cực view biển này sẽ là điểm dừng chân lý tưởng cho cả gia đình vào dịp Tết âm lịch 2021 sắp tới.', 'hotel', 'sang', '2021-01-19', 'ACTIVE', '/images/blog-7.jpg', 'Top 5 khách sạn Đà Nẵng có hồ bơi vô cực sang chảnh giá tốt tết âm lịch 2021', 'https://www.ivivu.com/blog/2021/01/top-5-khach-san-da-nang-co-ho-boi-vo-cuc-sang-chanh-gia-tot-tet-am-lich-2021/', 1);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (8, 'Thương hiệu mới nhất của Vinpearl – VinHolidays Phú Quốc đã chính thức khai trương. Khách sạn có thiết kế vô cùng năng động, tiện nghi và trẻ trung, nằm trong trung tâm Grand World Phú Quốc – được mệnh danh là Thành phố lễ hội không ngủ. Nằm ngay vị trí trung tâm của khu phức hợp Grand World Phú Quốc rộng lớn, giúp bạn hưởng trọn những tiện ích đẳng cấp của quần thể Phú Quốc United Center như: VinWonders Phú Quốc, Vinpearl Safari Phú Quốc, Sân Golf Vinpearl Phú Quốc…', 'sang', '2021-01-14', 'Tọa lạc tại vị trí trung tâm của Phú Quốc United Center, VinHolidays Phú Quốc được thiết kế với không gian nghỉ dưỡng hiện đại, trẻ trung và chọn lọc tiện ích một cách tinh tế.', 'hotel', 'sang', '2021-01-19', 'ACTIVE', '/images/blog-8.jpg', '3N2Đ ở VinHolidays Phú Quốc + Vé máy bay khứ hồi + Bữa sáng chỉ 2.299.000 đồng/khách', 'https://www.ivivu.com/blog/2021/01/3n2d-o-vinholidays-phu-quoc-ve-may-bay-khu-hoi-bua-sang-chi-2-299-000-dongkhach/', 1);
INSERT INTO travel.blog (id, content, created_by, created_date, description, keyword, modified_by, modified_date, status, thumbnail, title, url, user_id) VALUES (9, 'Đường bay áp dụng: Thanh hóa – Côn Đảo,  Hải Phòng – Côn Đảo, Đà Nẵng – Côn Đảo

- Hạng giá vé áp dụng: Tất cả các hạng giá vé

- Thời gian loại trừ: Không có

- Điều kiện áp dụng: Theo hạng giá vé đã mua', 'sang', '2021-01-14', 'Bamboo Airways mới tung khuyến mãi lớn với chương trình “Bay chung hưởng ưu đãi khủng” cho hành trình bay đến Côn Đảo, nhanh tay đặt vé thôi nào!



', 'máy bay', 'sang', '2021-01-13', 'ACTIVE', '/images/blog-9.jpg', 'Bay đến Côn Đảo với ưu đãi “Bay chung hưởng ưu đãi khủng” của Bamboo Airways', 'https://www.ivivu.com/blog/2021/01/bay-den-con-dao-voi-uu-dai-bay-chung-huong-uu-dai-khung-cua-bamboo-airways/', 1);