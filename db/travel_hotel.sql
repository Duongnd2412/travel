INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (1, '/images/hotel-quang-binh1.jpg,/images/hotel-quang-binh2.jpg', 'Trải nghiệm dịch vụ đẳng cấp thế giới ở Sun Spa Resort & Villa. Nằm ẩn mình trên diện tích 29 ha vườn cảnh nhiệt đới, các chỗ nghỉ đầy đủ tiện nghi tại đây được bài trí trang nhã với khu vực ghế ngồi, bàn làm việc và két an toàn cá nhân.

Nhà hàng Sun Rise Restaurant phục vụ các món ăn quốc tế, trong khi nhà hàng Golden Lotus Restaurant tập trung vào các món ăn ngon của địa phương. Du khách vừa có thể thưởng thức các món ăn vừa ngắm cảnh tại nhà hàng Asian Delight Restaurant.
Với vị trí nhìn ra bãi biển của chỗ nghỉ, du khách có thể tham gia nhiều hoạt động dưới nước như lướt ván buồm, chèo thuyền và câu cá.
Thời gian áp dụng: 04.05.2020 – 05.09.2020
Giá:  1.450.000 vnđ/ người
Liên hệ tư vấn: 090.481.4066', 1, 'Sun Spa Resort & Villa Quảng Bình 5*', '0904814066', '0904814066', '/images/hotel-quang-binh1.jpg', '2021-01-17');
INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (2, '/images/hotel-phu-quoc1.jpg,/images/hotel-phu-quoc2.jpg,/images/hotel-phu-quoc3.jpg', 'Combo bao gồm
- Vé máy bay khứ hồi với 7 kg hành lý xách tay
- 02 đêm nghỉ dưỡng tại phòng Standard với thiết kế hiện đại cùng ban công thoáng đãng
- Bữa sáng Buffet theo tiêu chuẩn Quốc tế lên đến hàng trăm món tại nhà hàng Nautilus
- Tặng thêm tour lặn biển ngắm san hô tại các bãi biển xanh mướt hoặc tour khám phá các điểm đến đặc trưng của Phú Quốc (*)
- Đưa đón sân bay theo lịch trình bay của khách hàng
- Thức uống đặc biệt chào mừng khi nhận phòng
- Vui chơi thỏa thích tại Công viên nước VinOasis dành riêng cho khách hàng lưu trú
- Miễn phí ngâm bồn Jacuzzi nóng, lạnh và xông hơi tại Akoya Spa
- Shuttle Bus đưa đón thị trấn Dương Đông theo lịch trình
(*) Không áp dụng giai đoạn đỉnh điểm 12-21/02/2021

Đặc biệt: đặt trước 30 ngày giảm ngay 400,000VND/phòng/3N2Đ (*)', 1, 'Khu nghỉ dưỡng VinOasis Phú Quốc
', '098333456', '0983334565', '/images/hotel-phu-quoc1.jpg', '2021-01-17');
INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (3, '/images/hotel-nha-trang1.jpg,/images/hotel-nha-trang2.jpg,/images/hotel-nha-trang3.jpg', '
Combo bao gồm
- Vé máy bay khứ hồi
- 02 đêm nghỉ dưỡng tại phòng Chic Suite có ban công và khu vườn riêng biệt, mang lại cảm giác tươi mới và thư thái nhất cho kỳ nghỉ
- Bữa sáng "mọi lúc - mọi nơi" luôn sẵn sàng để được phục vụ
- Miễn phí 01 liệu trình Spa mỗi ngày cho các khách người lớn, chăm sóc sức khỏe và xua tan mệt mỏi thường ngày
- Tham gia các lớp học Yoga & Taichi theo lịch trình của Resort
- Miễn phí shutle bus vào trung tâm thành phố Nha Trang (theo lịch trình khu nghỉ)
- Thêm trải nghiệm thú vị tại hồ bơi vô cực với tầm nhìn trực diện ra bãi biển xinh đẹp của Vịnh Cam Ranh', 2, 'Khu nghỉ dưỡng Fusion Cam Ranh 
', '0955332488', '19002045', '/images/hotel-nha-trang1.jpg', '2021-01-17');
INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (4, '/images/hotel-quy-nhon1.jpg,/images/hotel-quy-nhon2.jpg,/images/hotel-quy-nhon3.jpg', '- 02 đêm nghỉ dưỡng tại Semi Detached Villa hướng vườn
- Thức uống chào mừng khi đến nhận phòng
- Bữa sáng tự chọn tiêu chuẩn quốc tế tại nhà hàng Vị
- Miễn phí đưa đón sân bay theo giờ bay
- Tặng 1 lần 90 phút spa cho 2 người lớn/phòng ngủ/kỳ nghỉ hoặc đổi tín dụng 1,500,000 VNĐ/phòng ngủ/kỳ nghỉ cho dịch vụ ăn uống
- Miễn phí nhận phòng sớm và trả phòng trễ tùy tình trạng
- Miễn phí đồ uống không cồn trong minibar (bổ sung 1 lần/ngày)
- Tận hưởng sự thư giãn tại hồ bơi vô cực hướng vịnh Phương Mai
- Check-in tuyệt vời giữa khu vườn nhiệt đới xanh ngắt, bãi biển hoang sơ êm đềm, và cánh đồng quạt gió ấn tượng của Quy Nhơn', 2, 'Khu nghỉ dưỡng Maia Quy Nhơn ', '097754321', '0988843332', '/images/hotel-quy-nhon1.jpg', '2021-01-17');
INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (5, '/images/hotel-1.jpg,/images/hotel-2.jpg,/images/hotel3.jpg,/images/hotel4.jpg', '- Xe đưa đón từ HCM - Melia Hồ Tràm theo lịch trình
- 01 đêm nghỉ dưỡng tại hạng phòng Deluxe Room
- Thức uống chào mừng
- Ăn sáng tự chọn tại nhà hàng trước biển Sasa
- Tham gia các lớp Yoga, Taichi tốt cho sức khỏe
- Tự do sử dụng bãi biển riêng, hồ bơi, phòng gym
*Đặc biệt:
- Từ nay đến 23.12.2020: Đơn phòng lưu trú đêm thứ hai đến thứ năm sẽ được nâng cấp lên Deluxe Ocean View (phụ thuộc vào tình trạng phòng trống, quý khách vui lòng đặt sớm để nhận được ưu đãi)', 2, 'Melia Hồ Tràm Beach Resort 
', '093393212', '0984412222', '/images/hotel-1.jpg', '2021-01-17');
INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (6, '/images/hotel-21.jpg,/images/hotel-22.jpg,/images/hotel-23.jpg', 'Combo bao gồm
- Xe đưa đón từ HCM - The Grand Hồ Tràm Strip theo lịch trình
- 01 đêm nghỉ dưỡng tại phòng Deluxe Garden View sang trọng
- Ăn sáng tự chọn tiêu chuẩn quốc tế
- Miễn phí minibar ngày đến
- Thưởng thức các bộ phim bom tấn 2D-3D với loa âm thanh vòm hiện đại
- Tăng cường thể chất với các hoạt động thể thao ngoài trời như bowling, bi sắt, sân chơi cho trẻ, sân golf mini Putt-Putt, phòng gym
- Tưng bừng khuấy động với chương trình DJ by the Pool️ nóng bỏng từ 16h đến 18h vào thứ 6 và thứ 7 hằng tuần
- Tự do tận hưởng bãi biển riêng, hồ bơi chính cực rộng và hồ bơi trẻ em đầy sắc màu', 2, 'Khu nghỉ dưỡng The Grand Hồ Tràm Strip Vũng Tàu
', '098373737', '0973732212', '/images/hotel-21.jpg', '2021-01-17');
INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (7, '/images/hotel-31.jpg,/images/hotel-32.jpg,/images/hotel-33.jpg', 'Combo bao gồm
- Vé máy bay khứ hồi, bao gồm 7kg hành lý xách tay
- 2 đêm nghỉ tại phòng Superior hướng vườn
- Ăn sáng buffet hàng ngày
- Miễn phí nước uống chào mừng khi check-in tại resort
- Miễn phí xe ​bus đưa/ đón khách theo lịch trình từ resort ra trung tâm​​ Tp. Đà Lạt & ngược lại​
- Miễn phí sử dụng các tiện ích tại khách sạn: hồ bơi nước ấm trong nhà, phòng gym, bóng rổ, cầu lông, tennis (ban ngày),...
- Tham quan vườn dâu, vườn ươm hoa và rau củ.', 1, 'Khu nghỉ dưỡng Terracotta Đà Lạt 
', '093838322', '0937272222', '/images/hotel-31.jpg', '2021-01-17');
INSERT INTO travel.hotel (id, albums, description, locale_type, name, private_phone, public_phone, thumbnail, created_date) VALUES (8, '/images/hotel-41.jpg,/images/hotel-42.jpg,/images/hotel-43.jpg,/images/hotel-44.jpg', 'Combo bao gồm
- Vé máy bay khứ hồi, bao gồm 07 kg hành lý xách tay
- 02 đêm nghỉ dưỡng tại khu nghỉ dưỡng Đà Lạt Wonder
- Ăn sáng buffet hàng ngày
​​- 01 bữa trưa hoặc tối set menu ngày đầu tiên tại nhà hàng La Fonte
- Tận hưởng hồ bơi vô cực với tầm nhìn hướng thẳng ra Hồ Tuyền Lâm và đồi thông xanh ngắt
- Tự tay lựa chọn rau sạch tại vườn và chế biến món ăn theo sở thích riêng dưới sự hướng dẫn của đầu bếp Resort (có phụ phí chế biến món ăn)
- Dịch vụ chăm sóc sức khỏe mỗi ngày
- Thoải mái sử dụng các dịch vụ giải trí của resort như: câu cá, xem phim tại phòng chiếu Titan, Bida, Bi lắc…
- Miễn phí nâng hạng phòng Deluxe Lake view (tuỳ vào điều kiện phòng trống của resort)
- Giảm 10% dịch vụ spa, giặt ủi & 10% dịch vụ ăn uống tại nhà hàng La Fonte.
- Miễn phí xe ​bus đưa/ đón khách theo lịch trình từ resort ra trung tâm​​ & ngược lại​
- Miễn phí sử dụng các tiện ích tại khách sạn: phòng gym, wifi
- Lưu ý: Khách sạn không cho phép nấu ăn trong phòng hoặc villa', 2, 'Khu nghỉ dưỡng Đà Lạt Wonder
', '092827282', '0927272622', '/images/hotel-41.jpg', '2021-01-06');