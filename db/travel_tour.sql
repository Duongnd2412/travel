INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (1, '/images/tourHaLong11.jpg,/images/tourHaLong22.jpg,/images/tourHaLong33.jpg,/images/tourHaLong44.jpg,/images/tourHaLong55.jpg', '- Thưởng thức vẻ đẹp của Hạ Long với Hòn Trống Mái, Lục Đỉnh Hương.

- Khám phá Đảo Ti Tốp. 

- Hang Sửng Sốt - Một trong những hang động đẹp nhất vịnh Hạ Long.

- Chèo Kayak hoặc đi thuyền nan thăm Hang Luồn.', 'Sang', '2020-12-12', 'Du lịch Hạ Long. Vịnh Hạ Long -  một trong 7 kỳ quan thiên nhiên mới của thế giới - nhiều lần được UNESCO công nhận là di sản thiên nhiên của thế giới với hàng nghìn hòn đảo được làm nên bởi tạo hoá kỳ vĩ và sống động. Hạ Long như bức tranh thủy mặc khổng lồ, tuyệt đẹp và là một điểm du lịch rất hấp dẫn', 'Hạ Long', true, 'Hạ Long', 'sang', '2020-12-19', 'Hạ Long', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 2750000, 3, 'Hà Nội', 'ACTIVE', '/images/tourHaLong11.jpg', '3 ngày 2 đêm', 'Tour Tết Hạ Long', 1, 'Ô Tô', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (2, '/images/da-nang11.jpg,/images/da-nang22.jpg,/images/da-nang33.jpg,/images/da-nang44.jpg,/images/da-nang55.jpg,/images/da-nang66.jpg', '- Du lịch Tết Tân Sửu 2021 Đà Nẵng - Hội An - Mỹ Khê - Bảo Tàng Tranh 3D
- Tham quan Bảo tàng tranh 3D Art In Paradise Đà Nẵng, du khách sẽ hoá thân và diễn xuất thành các nhân vật trong những tác phẩm tranh 3D đầy thú vị.
- Thưởng thức “À Ố Show” – vở diễn độc đáo với hình thức xiếc kể chuyện, sử dụng toàn bộ đạo cụ chính là tre kết hợp với âm nhạc để mang đến những bức tranh hóm hỉnh mà đầy tinh tế từ làng quê yên bình đến phố thị nhộn nhịp đậm nét đời thường của người dân Việt Nam.
- Buổi chiều khởi hành tham quan Phố cổ Hội An với các công trình tiêu biểu: Chùa Cầu Nhật Bản, chùa Ông, hội quán Phúc Kiến, khu phố đèn lồng', 'Sang', '2020-12-12', 'Du lịch Đà Nẵng. Thành phố  Đà Nẵng là một trong những điểm du lịch nghỉ dưỡng lý tưởng cho du khách với nhiều danh thắng tuyệt đẹp làm say lòng người như Ngũ Hành Sơn, Bà Nà, bán đảo Sơn Trà, đèo Hải Vân, sông Hàn thơ mộng và biển Mỹ Khê đẹp nhất hành tinh', 'Đà Nẵng', true, 'Đà Nẵng', 'sang', '2020-12-10', 'Đà Nẵng', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 2700000, 2, 'Hà Nội', 'ACTIVE', '/images/da-nang11.jpg', '3 ngày 2 đêm', 'DU LỊCH TẾT TÂN SỬU 2021 ĐÀ NẴNG - HỘI AN - BÀ NÀ - MỸ KHÊ - BẢO TÀNG TRANH 3D', 2, 'Hàng không Vietnam Airlines', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (3, '/images/sapa1.jpg,/images/sapa2.jpg,/images/sapa3.jpg,/images/sapa4.jpg,/images/sapa5.jpg', 'Nằm phía Tây Bắc tổ quốc, Sa Pa ẩn chứa bao điều kỳ diệu của cảnh sắc thiên nhiên, con người. Thị trấn trong mây hấp dẫn du khách với quang cảnh núi non hùng vĩ cùng trải nghiệm độc đáo với cuộc sống của đồng bào dân tộc thiểu số.', 'Sang', '2021-01-12', 'Du lịch Sapa. Thị trấn trong mây-Sapa ẩn chứa bao điều kỳ diệu của thiên nhiên với địa hình của núi đồi, màu xanh của rừng tạo nên một vùng có nhiều cảnh sắc thơ mộng. Núi non hùng vĩ cùng trải nghiệm độc đáo với cuộc sống của đồng bào dân tộc thiểu số luôn hấp dẫn khách du lịch khi đến với Sapa.', 'Sapa', true, 'Sapa', 'sang', '2020-12-17', 'Sapa', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 3400000, 1, 'Hà Nội', 'ACTIVE', '/images/sapa1.jpg', '3 ngày 2 đêm', 'SaPa - Điều Kỳ Diệu Của Thiên Nhiên
', 1, 'Ô Tô', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (4, '/images/ca-mau1.jpg,/images/ca-mau2.jpg,/images/ca-mau3.jpg,/images/ca-mau4.jpg,/images/ca-mau5.jpg', 'Tạo hóa đã ban tặng cho miền Tây cái đẹp mộc mạc mà nên thơ, với những cánh đồng xanh bát ngát, những miệt vườn sai quả, những ngả đường trĩu nặng phù sa. Đặc biệt, du khách sẽ được xuôi về miền sông nước hữu tình này để chu du, thưởng ngoạn và trải nghiệm những điều bình dị, chân phương. Cùng iVIVU khám phá ngay hôm nay!', 'Sang', '2020-12-18', 'Du lịch miền Tây. Miền tây luôn là một điểm đến lý tưởng với vẻ đẹp êm dịu và yên bình của những vườn cây trái trĩu quả, cánh đồng lúa thẳng cánh cò bay và sự hiếu khách của con người nơi đây. Đến miền Tây, du khách có dịp nghỉ ngơi thư giãn sau những ngày làm việc bận rộn nơi đô thị ồn ào và hòa mình vào ', 'Cà Mau', true, 'Cà Mau', 'sang', '2020-12-02', 'Cà Mau', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 2800000, 4, 'Hà Nội', 'ACTIVE', '/images/ca-mau1.jpg', '4 Ngày 3 Đêm', 'Tour Miền Tây 4N3D: Hà Nội - Cần Thơ - Bạc Liêu - Cà Mau Bay VNA', 3, 'Máy Bay và Ô Tô', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (5, '/images/nhat-ban1.jpg,/images/nhat-ban2.jpg,/images/nhat-ban3.jpg,/images/nhat-ban4.jpg,/images/nhat-ban5.jpg', '- Khám phá xứ sở mặt trời mọc mùa thu
- Trải nghiệm tàu cao tốc Shinkansen
- Trải nghiệm tắm Onsen Nhật Bản', 'Sang', '2021-01-01', 'Du lịch Nhật Bản. Khám phá một Tokyo hiện đại nhưng vẫn cảm nhận từng cung bậc cảm xúc khi dạo bước bên ngôi cổ tự Asakusa, Hoàng cung và núi Phú Sĩ hùng vĩ. Hành trình khám phá cảnh sắc từ Tokyo đến Kyoto, kết nối Osaka với đền đài, chùa thiêng đẫm không khí thiền tịnh và các công trình xuyên biển', 'Nhật Bản', true, 'Nhật Bản', 'sang', '2021-01-20', 'Nhật Bản', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 1750000, 5, 'Hà Nội', 'ACTIVE', '/images/nhat-ban1.jpg', '6 ngày 5 đêm', 'DU LỊCH NHẬT BẢN MÙA THU [OSAKA - NARA - KYOTO - FUJI - TOKYO]', 4, 'Hàng không VietJet', 2);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (6, '/images/nha-trang1.jpg,/images/nha-trang2.jpg,/images/nha-trang3.jpg,/images/nha-trang4.jpg,/images/nha-trang5.jpg', '- Tham quan Vi- Tham quan Viện Hải Dương học,  Khu vui chơi giải trí Vinpearl Land,
              - Tham quan Đường hầm điêu khắc đất đỏ (Đà Lạt Star) - tái hiện lịch sử Đà Lạt qua hơn 120 năm. Xe theo cung đường Mang - Ling, ngang qua khu Vườn Hồng Cam Ly, tham quan khu dã ngoại núi Langbian, tham quan đồi Mimosa, Thung lũng Trăm Năm, chinh phục núi Langbian 
              - Tham quan nhà thờ Domain de Marie, Phân viện sinh học ...ện Hải Dương học', 'Sang', '2021-01-01', 'Du lịch Nha Trang. Đến với thành phố biển Nha Trang, du khách sẽ bị đánh gục bởi sự quyến rũ và năng động của thành phố xinh đẹp này, với hòn Mun Hòn Tằm nước trong veo, tháp Bà Ponagar hoàn mỹ của người Chăm, cảng Vũng Rô, làng Đại Lãnh và chợ Đầm nhộn nhịp.', 'Đà Lạt - Nha Trang', true, 'Nha Trang', 'sang', '2021-01-11', 'Nha Trang', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 2500000, 6, 'Hà Nội', 'ACTIVE', '/images/nha-trang1.jpg', '5 ngày 4 đêm', 'Tour Nha Trang', 2, 'Hàng không VietNam Airlines + Ô tô', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (7, '/images/phu-quoc1.jpg,/images/phu-quoc2.jpg,/images/phu-quoc3.jpg,/images/phu-quoc4.jpg,/images/phu-quoc5.jpg', '- Tham quan Dinh Cậu,Cơ sở nuôi cấy ngọc trai, Cơ sở Rượu sim, Cơ sở nước mắm Phú Quốc Khải Hoàn - Thịnh Phát, Di tích lịch sử nhà tù Phú Quốc, Cơ sở sản xuất khô, Làng chài cổ Hàm Ninh
- Tắm biển tại Bãi Sao, tham quan Chùa Hùng Long, khu vui chơi giải trí Vinwonders', 'Sang', '2021-01-13', 'Du lịch Phú Quốc. Phú Quốc là điểm đến dành cho du khách yêu thích hình thức du lịch nghỉ dưỡng và khám phá sinh thái tuyệt vời. Hành trình đến với thiên nhiên và chiêm ngưỡng Mũi Ông Đội, Đá Chào - thế giới san hô và cá biển sặc sỡ, biển Bãi Sao cát trắng mịn, dáng cong, nước xanh ngọc bích. ', 'Phú Quốc', false, 'Phú Quốc', 'sang', '2021-01-19', 'Phú Quốc', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 1750000, 5, 'Hà Nội', 'ACTIVE', '/images/phu-quoc1.jpg', '4 ngày 3 đêm', 'DU LỊCH PHÚ QUỐC', 3, 'Hàng không VietNam Airlines + Ô tô', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (8, '/images/con-dao1.jpg,/images/con-dao2.jpg,/images/con-dao3.jpg,/images/con-dao4.jpg,/images/con-dao5.jpg', '- Đến với Côn Đảo (hay với các tên khác như Côn Sơn, Côn Lôn) đã gợi cho người nghe hình ảnh mịt mù của một khu vực xa xôi, mờ mịt, có lúc được ví như địa ngục trần gian nổi tiếng từ thời Pháp thuộc đến giữa năm 1975. 
- Tham quan Cầu tàu 914, Trại Phú Hải, Chuồng Cọp Pháp - Mỹ, Mũi Cá Mập, Đỉnh Tình Yêu.', 'Sang', '2021-01-12', 'Du lịch Côn Đảo. Ngày nay Côn Đảo được biết đến như địa điểm du lịch biển đảo hấp dẫn du khách trong và ngoài nước.  Những bãi biển trong vắt cùng bờ cát trắng mịn làm say mê biêt bao khách dụ lịch khi đặt chân tới Côn Đảo', 'Côn Đảo', false, 'Côn Đảo', 'sang', '2021-01-18', 'Côn Đảo', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 3200000, 5, 'Hà Nội', 'ACTIVE', '/images/con-dao.jpg', '4 ngày 3 đêm', 'DU LỊCH TẾT TÂN SỬU 2021 CÔN ĐẢO', 3, 'Máy Bay', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (9, '/images/tay-nguyen1.jpg,/images/tay-nguyen2.jpg,/images/tay-nguyen3.jpg,/images/tay-nguyen4.jpg,/images/tay-nguyen5.jpg', '- Tham quan thác D''ray Nur - ngọn thác hùng vỹ của Tây Nguyên đại ngàn.
- Thăm buôn làng Ê đê Ako Dhong - tìm hiểu cuộc sống cộng đồng dân tộc Ê đê
- Nghe giới thiệu và tìm hiểu qui trình sản xuất cà phê Chồn tại Cơ sở nuôi chồn và sản xuất sản phẩm cà phê Chồn.', 'Sang', '2021-01-01', 'Du lịch Tây Nguyên. Mảnh đất Tây Nguyên sẽ là sự lựa chọn độc đáo cho chuyến hành trình nghỉ dưỡng của du khách. Du khách sẽ có dịp chiêm ngưỡng vẻ đẹp Thác DrayNur - ngọn thác lớn và đẹp nhất Tây Nguyên, trải nghiệm cưỡi voi tại Buôn Đôn và thưởng thức chương trình biểu diễn Cồng chiêng.', 'Buôn Ma Thuột', false, 'Tây Nguyên', 'sang', '2021-01-18', 'Tây Nguyên', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 2550000, 3, 'TP. Hồ Chí Minh', 'ACTIVE', '/images/tay-nguyen1.jpg', '5 ngày 4 đêm', 'DU LỊCH BUÔN MA THUỘT - BUÔN ĐÔN - PLEIKU - KOMTUM', 2, 'Đi về bằng xe Ô Tô', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (10, '/images/phan-thiet1.jpg,/images/phan-thiet2.jpg,/images/phan-thiet3.jpg,/images/phan-thiet4.jpg,/images/phan-thiet5.jpg,/images/phan-thiet6.jpg', '- Tham quan không gian trưng bày nghệ thuật “Làng chài xưa”
- Chụp hình và thường thức rượu tại  Lâu Đài Rượu Vang
- Xem show nhạc kịch nước Huyền Thoại Làng Chài', 'Sang', '2021-01-01', 'Du lịch Phan Thiết. Không chỉ sở hữu những khung cảnh thiên nhiên xinh đẹp như Lầu Ông Hoàng, tháp Chàm, Hòn Rơm, đỉnh Đồi Hồng, vùng đất này còn là một điểm đến văn hóa hấp dẫn với cộng đồng dân cư đa dạng người Hoa, người Chăm. Cùng với những món ăn đặc sắc và hải sản tươi ngon.', 'Phan Thiết', false, 'Phan Thiết', 'sang', '2021-01-18', 'Phan Thiết', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 5450000, 3, 'TP. Hồ Chí Minh', 'ACTIVE', '/images/phan-thiet1.jpg', '5 ngày 4 đêm', 'DU LỊCH PHAN THIẾT - MŨI NÉ - LÂU ĐÀI RƯỢU VANG
', 2, 'Đi về bằng xe Ô Tô', 1);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (11, '/images/singapore1.jpg,/images/singapore2.jpg,/images/singapore3.jpg,/images/singapore4.jpg,/images/singapore5.jpg', '- Khám phá Singapore - đất nước tươi trẻ, năng động, nơi giao thoa, hội tụ những nét tinh túy, đặc sắc của 2 nền văn hóa phương Đông lẫn phương Tây. 
- Chiêm ngưỡng những công trình vượt mọi giới hạn được thực hiện bởi người Singapore.
- Thưởng thức chương trình nhạc nước MBS Spectra Show đặc sắc
- Thỏa sức mua sắm tại các trung tâm thương mại lớn.', 'Sang', '2021-01-01', 'Du lịch Singapore. Đảo quốc sư tử Singapore được mệnh danh là đất nước xanh - sạch - đẹp nhất thế giới với nhiều điểm đến hấp dẫn như Công viên Merlion, Vương quốc côn trùng, Thủy cung S.E.A - thủy cung lớn nhất thế giới, vườn chim Jurong - với những màn biểu diễn xiếc chim đẹp mắt. ', 'Singapore', false, 'Singapore', 'sang', '2021-01-18', 'Singapore', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 4500000, 3, 'Hà Nội', 'ACTIVE', '/images/singapore1.jpg', '5 ngày 4 đêm', 'DU LỊCH SINGAPORE [THỦY CUNG S.E.A AQUARIUM - GARDENS BY THE BAY]', 4, 'Hàng không VietJet', 2);
INSERT INTO travel.tour (id, albums, content, created_by, created_date, description, destination_place, is_hot, keyword, modified_by, modified_date, name, policy, price, regions, start_place, status, thumbnail, time, title, tour_type, vehicle, tour_category_id) VALUES (12, '/images/thai-lan1.jpg,/images/thai-lan2.jpg,/images/thai-lan3.jpg,/images/thai-lan4.jpg,/images/thai-lan5.jpg', '- Trải nghiệm hoàn toàn khác biệt tại Chiang Mai với văn hóa, ẩm thực và các điệu múa đặc trưng....vùng núi phía Bắc Thái Lan.
- Vãn cảnh chùa Doi Suthep đẹp rực rỡ hơn 600 năm trên đỉnh núi cao hơn 1.000m so với mực nước biển hay Chùa Trắng Wat Rong Khun cùng lối kiến trúc cổ điển pha lẫn phong cách siêu thực khác lạ so với các ngôi đền Phật giáo thông thường... 
- Thỏa sức mua sắm các sản vật địa phương, khám phá thiên đường của những chiếc dù Bo Sang “món quà lưu niệm được ưa thích nhất” đối với du khách khi đến Chiang Rai xinh đẹp.', 'Sang', '2021-01-01', 'Du lịch Thái Lan. Với con người thân thiện và vui vẻ, nền văn hóa và lịch sử lâu đời, thiên đường du lịch Thái Lan luôn là điểm dừng chân lý tưởng cho du khách. Thành phố Bangkok, du khách có thể thỏa sức mua sắm tại Siam Paragon,... Phố biển Pattaya thu hút bởi sự tuyệt diệu của bãi biển cát trắng', 'Thái Lan', false, 'Thái Lan', 'sang', '2021-01-18', 'Thái Lan', '* Giá tour bao gồm:  
- Chi phí xe máy lạnh phục vụ theo chương trình.
- Chi phí ăn - uống theo chương trình.
- Chi phí khách sạn tiêu chuẩn 2 khách/phòng. Lẻ khách ngủ giường phụ hoặc chịu phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Chi phí tham quan, hướng dẫn viên tiếng Việt
* Quà tặng: Nón, nước suối, khăn lạnh, viết…

* Giá tour không bao gồm:  
- Ăn uống ngoài chương trình, giặt ủi, điện thoại và các chi phí cá nhân.
- Phí phụ thu phòng đơn: + 1.340.000 đ/ khách/ tour.
- Phí phụ thu người nước ngoài: +300.000 đ/ khách/ tour.

THÔNG TIN HƯỚNG DẪN
* Vé trẻ em:  
- Vé tour: Trẻ em từ  6 đến 11 tuổi mua một nửa giá vé người lớn, trẻ em trên 11 tuổi mua vé như người lớn.
- Đối với trẻ em dưới 6 tuổi, gia đình tự lo cho bé ăn ngủ và tự trả phí tham quan (nếu có). Hai người lớn chỉ được kèm một trẻ em. Từ trẻ thứ 2 trở lên, mỗi em phải 50% giá vé người lớn
- Tiêu chuẩn 50% giá tour bao gồm: Suất ăn, ghế ngồi và ngủ ghép chung với gia đình.

* Hành lý và giấy tờ tùy thân:  
- Du khách mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu. Đối với du khách là Việt kiều, Quốc tế nhập cảnh Việt Nam bằng visa rời, vui lòng mang theo visa khi đăng ký và đi tour.
- Khách lớn tuổi (từ 70 tuổi trở lên), khách tàn tật tham gia tour, phải có thân nhân đi kèm và cam kết đảm bảo đủ sức khỏe khi tham gia tour du lịch.
- Trẻ em dưới 14 tuổi khi đi tour phải mang theo Giấy khai sinh hoặc Hộ chiếu. Trẻ em từ 14 tuổi trở lên phải mang theo giấy Chứng Minh Nhân Dân hoặc Hộ chiếu riêng
- Tất cả giấy tờ tùy thân mang theo đều phải bản chính
- Du khách mang theo hành lý gọn nhẹ và phải tự bảo quản hành lý, tiền bạc, tư trang trong suốt thời gian đi du lịch.
- Khách Việt Nam ở cùng phòng với khách Quốc tế  hoặc Việt kiều yêu cầu phải có giấy hôn thú.

* Trường hợp hủy vé tour, du khách vui lòng thanh toán các khoản lệ phí hủy tour như sau:
a) Đối với dịp Lễ, Tết:
- Du khách chuyển đổi tour sang ngày khác và báo trước ngày khởi hành trước 15 ngày sẽ không chịu phí (không áp dụng các tour KS 4- 5 sao), nếu trễ hơn sẽ căn cứ theo qui định hủy phạt  phía dưới và chỉ được chuyển ngày khởi hành tour 1 lần.
- Hủy vé trong vòng 24 giờ hoặc ngay ngày khởi hành, chịu phạt 100% tiền tour.
- Hủy vé trước ngày khởi hành từ 2 - 7 ngày, chịu phạt 80% tiền tour.
- Hủy vé trước ngày khởi hành từ 8 - 15 ngày, chịu phạt 50% tiền tour.
- Hủy vé trước ngày khởi hành trước 15 ngày, chịu phạt 20% tiền tour.

b) Sau khi hủy tour, du khách vui lòng đến nhận tiền trong vòng 15 ngày kể từ ngày kết thúc tour. Chúng tôi chỉ  thanh toán trong khỏang thời gian nói trên.

c) Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Lữ hành Saigontourist sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.

* Ghi chú khác:
- Công ty xuất hóa đơn cho du khách có nhu cầu (Trong thời hạn 7 ngày sau khi kết thúc chương trình du lịch). Du khách được chọn một trong những chương trình khuyến mãi dành cho khách lẻ định kỳ (Nếu có).
- Du khách có mặt tại điểm đón trước 15 phút. Du khách đến trễ khi xe đã khởi hành hoặc hủy tour không báo trước vui lòng chịu phí như ‘hủy vé ngay ngày khởi hành’.', 5750000, 3, 'Hà Nội', 'ACTIVE', '/images/thai-lan1.jpg', '6 ngày 5 đêm', 'DU LỊCH THÁI LAN', 4, 'Hàng không VietJet', 2);