INSERT INTO travel.locale (id, name, country_id) VALUES (1, 'Tiếng Việt', 1);
INSERT INTO travel.locale (id, name, country_id) VALUES (2, 'Tiếng Nhật', 2);
INSERT INTO travel.locale (id, name, country_id) VALUES (3, 'Singapore', 3);
INSERT INTO travel.locale (id, name, country_id) VALUES (4, 'Thái Lan', 4);
INSERT INTO travel.locale (id, name, country_id) VALUES (5, 'Anh', 5);
INSERT INTO travel.locale (id, name, country_id) VALUES (6, 'Đức', 6);
INSERT INTO travel.locale (id, name, country_id) VALUES (7, 'Trung Quốc', 7);
INSERT INTO travel.locale (id, name, country_id) VALUES (8, 'Hàn', 8);
INSERT INTO travel.locale (id, name, country_id) VALUES (9, 'Pháp', 9);
INSERT INTO travel.locale (id, name, country_id) VALUES (10, 'Mỹ', 10);
INSERT INTO travel.locale (id, name, country_id) VALUES (11, 'Cam-Pu-Chia', 11);
INSERT INTO travel.locale (id, name, country_id) VALUES (12, 'Lào', 12);