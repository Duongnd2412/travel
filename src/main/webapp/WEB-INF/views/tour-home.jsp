<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">
    <head>
        <title>Tour Homepage</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" href="<c:url value="/images/favicon.png"/>"type="image/x-icon">
        
        <!-- Google Fonts -->	
        <link href="<c:url value="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i"/>" rel="stylesheet">
        
        <!-- Bootstrap Stylesheet -->	
        <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">
        
        <!-- Font Awesome Stylesheet -->
        <link rel="stylesheet" href="<c:url value="/css/font-awesome.min.css"/>">
            
        <!-- Custom Stylesheets -->	
        <link rel="stylesheet" href="<c:url value="/css/style.css"/>">
        <link rel="stylesheet" id="cpswitch" href="<c:url value="/css/orange.css"/>">
        <link rel="stylesheet" href="<c:url value="/css/responsive.css"/>">
    
        <!-- Owl Carousel Stylesheet -->
        <link rel="stylesheet" href="<c:url value="/css/owl.carousel.css"/>">
        <link rel="stylesheet" href="<c:url value="/css/owl.theme.css"/>">
        
        <!-- Flex Slider Stylesheet -->
        <link rel="stylesheet" href="<c:url value="/css/flexslider.css"/>" type="text/css" />
        
        <!--Date-Picker Stylesheet-->
        <link rel="stylesheet" href="<c:url value="/css/datepicker.css"/>">
    </head>
    
    
    <body id="tour-homepage">
    
        <!--====== LOADER =====-->
        <div class="loader"></div>
    
    
    	<!--======== SEARCH-OVERLAY =========-->       
        <div class="overlay">
            <a href="javascript:void(0)" id="close-button" class="closebtn">&times;</a>
            <div class="overlay-content">
                <div class="form-center">
                    <form>
                    	<div class="form-group">
                        	<div class="input-group">
                        		<input type="text" class="form-control" placeholder="Search..." required />
                            	<span class="input-group-btn"><button type="submit" class="btn"><span><i class="fa fa-search"></i></span></button></span>
                            </div><!-- end input-group -->
                        </div><!-- end form-group -->
                    </form>
                </div><!-- end form-center -->
            </div><!-- end overlay-content -->
        </div><!-- end overlay -->
        
        
        <!--============= TOP-BAR ===========-->
        <div id="top-bar" class="tb-text-white">
            <div class="container">
                <div class="row">          
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div id="info">
                            <ul class="list-unstyled list-inline">
                                <li><span><i class="fa fa-map-marker"></i></span>29 Land St, Lorem City, CA</li>
                                <li><span><i class="fa fa-phone"></i></span>+00 123 4567</li>
                            </ul>
                        </div><!-- end info -->
                    </div><!-- end columns -->
                    
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div id="links">
                            <ul class="list-unstyled list-inline">
                                <li><a href="login"><span><i class="fa fa-lock"></i></span>Login</a></li>
                                <li><a href="registration"><span><i class="fa fa-plus"></i></span>Sign Up</a></li>
                            </ul>
                        </div><!-- end links -->
                    </div><!-- end columns -->				
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end top-bar -->

        <nav class="navbar navbar-default main-navbar navbar-custom navbar-colored" id="mynavbar">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" id="menu-button">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>                        
                    </button>
                    <div class="header-search hidden-lg">
                    	<a href="javascript:void(0)" class="search-button"><span><i class="fa fa-search"></i></span></a>
                    </div>
                    <a href="tour" class="navbar-brand"><span><i class="fa fa-plane"></i>OHANA</span>TRAVELS</a>
                </div><!-- end navbar-header -->
                
                <div class="collapse navbar-collapse" id="myNavbar1">
                    <ul class="nav navbar-nav navbar-right navbar-search-link">
                        <li class="dropdown active"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Home<span><i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html">Main Homepage</a></li>
                                <li><a href="flight-homepage.html">Flight Homepage</a></li>
                                <li><a href="hotel-homepage.html">Hotel Homepage</a></li>
                                <li class="active"><a href="#">Tour Homepage</a></li>
                                <li><a href="cruise-homepage.html">Cruise Homepage</a></li>
                                <li><a href="car-homepage.html">Car Homepage</a></li>
                                <li><a href="landing-page.html">Landing Page</a></li>
                            </ul>			
                        </li>
<%--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Flights<span><i class="fa fa-angle-down"></i></span></a>--%>
<%--                            <ul class="dropdown-menu">--%>
<%--                            	<li><a href="flight-homepage.html">Flight Homepage</a></li>--%>
<%--                                <li><a href="flight-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
<%--                                <li><a href="flight-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
<%--                                <li><a href="flight-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
<%--                                <li><a href="flight-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
<%--                                <li><a href="flight-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
<%--                                <li><a href="flight-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
<%--                            </ul>			--%>
<%--                        </li>--%>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tours<span><i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown-menu mega-dropdown-menu row">
                                <li>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Miền Bắc</span></li>
                                                <c:forEach items="${tours}" var="tour">
                                                    <c:if test="${tour.tourType == 1}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>

                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Miền Trung</span></li>
                                                <c:forEach items="${tours}" var="tour">
                                                    <c:if test="${tour.tourType == 2}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>

                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Miền Nam</span></li>
                                                <c:forEach items="${tours}" var="tour">
                                                    <c:if test="${tour.tourType == 3}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Ngoài Nước</span></li>
                                                <c:forEach items="${tours}" var="tour">
                                                    <c:if test="${tour.tourType == 4}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
<%--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cruise<span><i class="fa fa-angle-down"></i></span></a>--%>
<%--                            <ul class="dropdown-menu">--%>
<%--                            	<li><a href="cruise-homepage.html">Cruise Homepage</a></li>--%>
<%--                                <li><a href="cruise-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
<%--                                <li><a href="cruise-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
<%--                                <li><a href="cruise-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
<%--                                <li><a href="cruise-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
<%--                                <li><a href="cruise-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
<%--                                <li><a href="cruise-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
<%--                            </ul>			--%>
<%--                        </li>--%>
<%--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cars<span><i class="fa fa-angle-down"></i></span></a>--%>
<%--                            <ul class="dropdown-menu">--%>
<%--                            	<li><a href="car-homepage.html">Car Homepage</a></li>--%>
<%--                                <li><a href="car-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
<%--                                <li><a href="car-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
<%--                                <li><a href="car-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
<%--                                <li><a href="car-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
<%--                                <li><a href="car-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
<%--                                <li><a href="car-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
<%--                            </ul>			--%>
<%--                        </li>--%>
<%--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages<span><i class="fa fa-angle-down"></i></span></a>--%>
<%--                            <ul class="dropdown-menu mega-dropdown-menu row">--%>
<%--                                <li>--%>
<%--                                    <div class="row">--%>
<%--                                        <div class="col-md-3">--%>
<%--                                            <ul class="list-unstyled">--%>
<%--                                                <li class="dropdown-header">Standard <span>Pages</span></li>--%>
<%--                                                <li><a href="about-us.html">About Us</a></li>--%>
<%--                                				<li><a href="contact-us.html">Contact Us</a></li>--%>
<%--                                                <li><a href="blog-listing-left-sidebar.html">Blog Listing Left Sidebar</a></li>--%>
<%--                                                <li><a href="blog-listing-right-sidebar.html">Blog Listing Right Sidebar</a></li>--%>
<%--                                                <li><a href="blog-detail-left-sidebar.html">Blog Detail Left Sidebar</a></li>--%>
<%--                                                <li><a href="blog-detail-right-sidebar.html">Blog Detail Right Sidebar</a></li>--%>
<%--                                            </ul>--%>
<%--                                        </div>--%>
<%--                                        --%>
<%--                                        <div class="col-md-3">--%>
<%--                                            <ul class="list-unstyled">	--%>
<%--                                                <li class="dropdown-header">User <span>Dashboard</span></li>--%>
<%--                                                <li><a href="dashboard.html">Dashboard</a></li>--%>
<%--                                                <li><a href="user-profile.html">User Profile</a></li>--%>
<%--                                                <li><a href="booking.html">Booking</a></li>--%>
<%--                                                <li><a href="wishlist.html">Wishlist</a></li>--%>
<%--                                                <li><a href="cards.html">Cards</a></li>--%>
<%--                                            </ul>--%>
<%--                                        </div>--%>
<%--                                        --%>
<%--                                        <div class="col-md-3">--%>
<%--                                            <ul class="list-unstyled">--%>
<%--                                                <li class="dropdown-header">Special <span>Pages</span></li>--%>
<%--                                                <li><a href="login.html">Login</a></li>--%>
<%--                                                <li><a href="registration.jsp">Registration</a></li>--%>
<%--                                                <li><a href="forgot-password.html">Forgot Password</a></li>--%>
<%--                                                <li><a href="error-page.html">404 Page</a></li>--%>
<%--                                                <li><a href="coming-soon.html">Coming Soon</a></li>   --%>
<%--                                            </ul>--%>
<%--                                        </div>--%>
<%--                                        --%>
<%--                                        <div class="col-md-3">--%>
<%--                                            <ul class="list-unstyled">--%>
<%--                                                <li class="dropdown-header">Extra <span>Pages</span></li>--%>
<%--                                                <li><a href="before-you-fly.html">Before Fly</a></li>--%>
<%--                                                <li><a href="travel-insurance.html">Travel Insurance</a></li>--%>
<%--                                                <li><a href="holidays.html">Holidays</a></li>--%>
<%--                                                <li><a href="thank-you.html">Thank You</a></li>--%>
<%--                                            </ul>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </li>--%>
<%--                            </ul>			--%>
<%--                        </li>--%>
                        <li><a href="javascript:void(0)" class="search-button"><span><i class="fa fa-search"></i></span></a></li>
                    </ul>
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </nav><!-- end navbar -->

		<div class="sidenav-content">
            <div id="mySidenav" class="sidenav" >
                <h2 id="web-name"><span><i class="fa fa-plane"></i></span>Ohana Travel</h2>

                <div id="main-menu">
                	<div class="closebtn">
                        <button class="btn btn-default" id="closebtn">&times;</button>
                    </div><!-- end close-btn -->
                    
                    <div class="list-group panel">
                    
                        <a href="#home-links" class="list-group-item active" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-home link-icon"></i></span>Home<span><i class="fa fa-chevron-down arrow"></i></span></a>
                        <div class="collapse sub-menu" id="home-links">
                            <a href="index.html" class="list-group-item">Main Homepage</a>
                            <a href="flight-homepage.html" class="list-group-item">Flight Homepage</a>
                            <a href="hotel-homepage.html" class="list-group-item">Hotel Homepage</a>
                            <a href="#" class="list-group-item active">Tour Homepage</a>
                            <a href="cruise-homepage.html" class="list-group-item">Cruise Homepage</a>
                            <a href="car-homepage.html" class="list-group-item">Car Homepage</a>
                            <a href="landing-page.html" class="list-group-item">Landing Page</a>
                        </div><!-- end sub-menu -->
                        
                        <a href="#flights-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-plane link-icon"></i></span>Flights<span><i class="fa fa-chevron-down arrow"></i></span></a>
                        <div class="collapse sub-menu" id="flights-links">
                            <a href="flight-homepage.html" class="list-group-item">Flight Homepage</a>
                            <a href="flight-listing-left-sidebar.html" class="list-group-item">List View Left Sidebar</a>
                            <a href="flight-listing-right-sidebar.html" class="list-group-item">List View Right Sidebar</a>
                            <a href="flight-grid-left-sidebar.html" class="list-group-item">Grid View Left Sidebar</a>
                            <a href="flight-grid-right-sidebar.html" class="list-group-item">Grid View Right Sidebar</a>
                            <a href="flight-detail-left-sidebar.html" class="list-group-item">Detail Left Sidebar</a>
                            <a href="flight-detail-right-sidebar.html" class="list-group-item">Detail Right Sidebar</a>
                        </div><!-- end sub-menu -->
                        
                        <a href="#hotels-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-building link-icon"></i></span>Hotels<span><i class="fa fa-chevron-down arrow"></i></span></a>
                        <div class="collapse sub-menu" id="hotels-links">
                            <a href="hotel-homepage.html" class="list-group-item">Hotel Homepage</a>
                            <a href="hotel-listing-left-sidebar.html" class="list-group-item">List View Left Sidebar</a>
                            <a href="hotel-listing-right-sidebar.html" class="list-group-item">List View Right Sidebar</a>
                            <a href="hotel-list.jsp" class="list-group-item">Grid View Left Sidebar</a>
                            <a href="hotel-grid-right-sidebar.html" class="list-group-item">Grid View Right Sidebar</a>
                            <a href="hotel-detail.jsp" class="list-group-item">Detail Left Sidebar</a>
                            <a href="hotel-detail-right-sidebar.html" class="list-group-item">Detail Right Sidebar</a>
                        </div><!-- end sub-menu -->
                        
                        <a href="#tours-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-globe link-icon"></i></span>Tours<span><i class="fa fa-chevron-down arrow"></i></span></a>
                        <div class="collapse sub-menu" id="tours-links">
<%--                            <c:forEach items="${tours}" var="tour">--%>
<%--                            <div class="list-group-heading list-group-item"><span>Miền Bắc</span></div>--%>
<%--                                <c:if test="${tour.tourType == 1}">--%>
<%--                                    <a href="tour/${tour.id}" class="list-group-item"></a>--%>
<%--                                </c:if>--%>
<%--                            <div class="list-group-heading list-group-item"><span>Miền Trung</span></div>--%>
<%--                                <c:if test="${tour.tourType == 2}">--%>
<%--                                    <a href="tour/${tour.id}" class="list-group-item"></a>--%>
<%--                                </c:if>--%>
<%--                            <div class="list-group-heading list-group-item"><span>Miền Nam</span></div>--%>
<%--                                <c:if test="${tour.tourType == 3}">--%>
<%--                                    <a href="tour/${tour.id}" class="list-group-item"></a>--%>
<%--                                </c:if>--%>
<%--                            <div class="list-group-heading list-group-item"><span>Ngoài Nước</span></div>--%>
<%--                                <c:if test="${tour.tourType == 4}">--%>
<%--                                    <a href="tour/${tour.id}" class="list-group-item"></a>--%>
<%--                                </c:if>--%>
<%--                            </c:forEach>--%>
                        </div>

                        

                        

                        
                        <a href="#pages-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-clone link-icon"></i></span>Pages<span><i class="fa fa-chevron-down arrow"></i></span></a>
                        <div class="collapse sub-menu" id="pages-links">
                        	<div class="list-group-heading list-group-item">Standard <span>Pages</span></div>
                            <a href="about-us.html"  class="list-group-item">About Us</a>
                            <a href="contact-us.html"  class="list-group-item">Contact Us</a>
                            <a href="blog-listing-left-sidebar.html"  class="list-group-item">Blog Listing Left Sidebar</a>
                            <a href="blog-listing-right-sidebar.html"  class="list-group-item">Blog Listing Right Sidebar</a>
                            <a href="blog-detail-left-sidebar.html"  class="list-group-item">Blog Detail Left Sidebar</a>
                            <a href="blog-detail-right-sidebar.html"  class="list-group-item">Blog Detail Right Sidebar</a>
                            <div class="list-group-heading list-group-item">User <span>Dashboard</span></div>
                            <a href="dashboard.html"  class="list-group-item">Dashboard</a>
                            <a href="user-profile.html"  class="list-group-item">User Profile</a>
                            <a href="booking.html"  class="list-group-item">Booking</a>
                            <a href="wishlist.html"  class="list-group-item">Wishlist</a>
                            <a href="cards.html"  class="list-group-item">Cards</a>
                            <div class="list-group-heading list-group-item">Special <span>Pages</span></div>
                            <a href="login.html"  class="list-group-item">Login</a>
                            <a href="registration.jsp" class="list-group-item">Registration</a>
                            <a href="forgot-password.html"  class="list-group-item">Forgot Password</a>
                            <a href="error-page.html"  class="list-group-item">404 Page</a>
                            <a href="coming-soon.html"  class="list-group-item">Coming Soon</a>
                            <div class="list-group-heading list-group-item">Extra <span>Pages</span></div>
                            <a href="before-you-fly.html" class="list-group-item">Before Fly</a>
                            <a href="travel-insurance.html" class="list-group-item">Travel Insurance</a>
                            <a href="holidays.html" class="list-group-item">Holidays</a>
                            <a href="thank-you.html" class="list-group-item">Thank You</a>
                        </div><!-- end sub-menu -->
                    </div><!-- end list-group -->
                </div><!-- end main-menu -->
            </div><!-- end mySidenav -->
        </div><!-- end sidenav-content -->
        
        
		<!--========================= FLEX SLIDER =====================-->
        <section class="flexslider-container" id="flexslider-container-4">
            
            <div class="flexslider slider tour-slider" id="slider-4">
                <ul class="slides">
                    <li class="item-1 back-size" style="background:	linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0)),url(<c:url value="/images/sapa99.jpg"/>) 50% 50%;
	background-size:cover;
	height:100%;">
                    	<div class="meta">         
                            <div class="container">
                            	<span class="highlight-price highlight-2">Giá Từ 1.000.000đ</span>
                                <h3>Du Lịch Sapa</h3>
                                <p>Du lịch Sapa. Thị trấn trong mây-Sapa ẩn chứa bao điều kỳ diệu của thiên nhiên với địa hình của núi đồi, màu xanh của rừng tạo nên một vùng có nhiều cảnh sắc thơ mộng. Núi non hùng vĩ cùng trải nghiệm độc đáo với cuộc sống của đồng bào dân tộc thiểu số luôn hấp dẫn khách du lịch khi đến với Sapa.</p>
                            </div><!-- end container -->  
                        </div><!-- end meta -->
                    </li><!-- end item-1 -->
                    
                    <li class="item-2 back-size" style="background:	linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0)),url(<c:url value="/images/phu_quoc_999.jpg"/>) 50% 50%;background-size:cover;height:100%;">
                        <div class=" meta">         
                            <div class="container">
                            	<span class="highlight-price highlight-2">Giá Từ 2.100.000đ</span>
                                <h3>Du Lịch Phú Quốc</h3>
                                <p>Du lịch Phú Quốc. Phú Quốc là điểm đến dành cho du khách yêu thích hình thức du lịch nghỉ dưỡng và khám phá sinh thái tuyệt vời. Hành trình đến với thiên nhiên và chiêm ngưỡng Mũi Ông Đội, Đá Chào - thế giới san hô và cá biển sặc sỡ, biển Bãi Sao cát trắng mịn, dáng cong, nước xanh ngọc bích.</p>
                            </div><!-- end container -->  
                        </div><!-- end meta -->
                    </li><!-- end item-2 -->
                   
                </ul>
            </div><!-- end slider -->
            
            <div class="search-tabs" id="search-tabs-4">
            	<div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                        
                            <ul class="nav nav-tabs">
<%--                                <li><a href="#flights" data-toggle="tab"><span><i class="fa fa-plane"></i></span><span class="st-text">Flights</span></a></li>--%>
                                <li><a href="#hotels" data-toggle="tab"><span><i class="fa fa-building"></i></span><span class="st-text">Hotels</span></a></li>
                                <li class="active"><a href="#tours" data-toggle="tab"><span><i class="fa fa-suitcase"></i></span><span class="st-text">Tours</span></a></li>
<%--                                <li><a href="#cruise" data-toggle="tab"><span><i class="fa fa-ship"></i></span><span class="st-text">Cruise</span></a></li>--%>
<%--                                <li><a href="#cars" data-toggle="tab"><span><i class="fa fa-car"></i></span><span class="st-text">Cars</span></a></li>--%>
                            </ul>
        
                            <div class="tab-content">
                                <div id="tours" class="tab-pane in active">
                                    <form>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control" placeholder="Thành Phố, Quốc Gia" />
                                                    <i class="fa fa-map-marker"></i>
                                                </div>
                                            </div><!-- end columns -->
                                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                                <div class="form-group right-icon">
                                                    <select class="form-control">
                                                        <option selected>Month</option>
                                                        <option>January</option>
                                                        <option>February</option>
                                                        <option>March</option>
                                                        <option>April</option>
                                                        <option>May</option>
                                                        <option>June</option>
                                                        <option>July</option>
                                                        <option>August</option>
                                                        <option>September</option>
                                                        <option>October</option>
                                                        <option>November</option>
                                                        <option>December</option>
                                                    </select>
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div><!-- end columns -->
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 search-btn">
                                                <button class="btn btn-orange">Search</button>
                                            </div><!-- end columns -->
                                            
                                        </div><!-- end row -->
                                    </form>
                                </div><!-- end tours -->
                                
                                <div id="cruise" class="tab-pane">
                                    <form>
                                        <div class="row">
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                                                <div class="row">
                                                
                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control" placeholder="From" >
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div><!-- end columns -->
                                                    
                                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control" placeholder="To" >
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div><!-- end columns -->
        
                                                </div><!-- end row -->								
                                            </div><!-- end columns -->
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                                                <div class="row">
                                                
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control dpd1" placeholder="Check In" >
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                    </div><!-- end columns -->
                                                    
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control dpd2" placeholder="Check Out" >
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                    </div><!-- end columns -->
        
                                                </div><!-- end row -->								
                                            </div><!-- end columns -->
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                <div class="form-group right-icon">
                                                    <select class="form-control">
                                                    <option selected>Người Lớn</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                        <option>4</option>
                                                    </select>
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div><!-- end columns -->
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 search-btn">
                                                <button class="btn btn-orange">Search</button>
                                            </div><!-- end columns -->
                                            
                                        </div><!-- end columns -->
                                    </form>
                                </div><!-- end cruises -->

                                <div id="cars" class="tab-pane">
                                    <form>					
                                        <div class="row">
                                        
                                            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6">
                                                <div class="row">
                                                
                                                    <div class="col-sm-6 col-md-4">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control" placeholder="Country" />
                                                            <i class="fa fa-globe"></i>
                                                        </div>
                                                    </div><!-- end columns -->
                                                    
                                                    <div class="col-sm-6 col-md-4">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control" placeholder="City" />
                                                            <i class="fa fa-map-marker"></i>
                                                        </div>
                                                    </div><!-- end columns -->
                                                    
                                                    <div class="col-sm-12 col-md-4">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control" placeholder="Location" />
                                                            <i class="fa fa-street-view"></i>
                                                        </div>
                                                    </div><!-- end columns -->
                                                    
                                                </div><!-- end row -->
                                            </div><!-- end columns -->
                                            
                                            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                                                <div class="row">
                                                
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control dpd1" placeholder="Check In" >
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                    </div><!-- end columns -->
                                                    
                                                    <div class="col-xs-6 col-sm-6 col-md-6">
                                                        <div class="form-group left-icon">
                                                            <input type="text" class="form-control dpd2" placeholder="Check Out" >
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                    </div><!-- end columns -->
                                                    
                                                </div><!-- end row -->
                                            </div><!-- end columns -->
    
                                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 search-btn">
                                                <button class="btn btn-orange">Search</button>
                                            </div><!-- end columns -->
                                            
                                        </div><!-- end row -->					
                                    </form>
                                </div><!-- end cars -->
                                
                            </div><!-- end tab-content -->
                            
                        </div><!-- end columns -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div>
            <!-- end search-tabs -->
            
        </section><!-- end flexslider-container -->


        <!--=============== TOUR OFFERS ===============-->
        <section id="tour-offers" class="section-padding">
        	<div class="container">
        		<div class="row">
        			<div class="col-sm-12">
                    	<div class="page-heading">
                        	<h2>Ưu Đãi Tour</h2>
                            <hr class="heading-line" />
                        </div><!-- end page-heading -->
                        
                         <div class="owl-carousel owl-theme owl-custom-arrow" id="owl-tour-offers">
                            
                            <div class="item">
                                <div class="main-block tour-block">
                                    <div class="main-img">
                                    	<a href="tour/1">
                                        	<img src="<c:url value="/images/halongsss.jpg"/>" class="img-responsive" alt="tour-img" />
                                    	</a>
                                    </div><!-- end offer-img -->
                                    
                                    <div class="offer-price-2">
                                        <ul class="list-unstyled">
                                            <li class="price">2.350.000đ<a href="tour/1" ><span class="arrow"><i class="fa fa-angle-right"></i></span></a></li>
                                        </ul>
                                    </div><!-- end offer-price-2 -->
                                        
                                    <div class="main-info tour-info">
                                    	<div class="main-title tour-title">
                                            <a href="tour/1">Tham quan Hạ Long</a>
                                            <p>Hạ Long</p>
                                            <div class="rating">
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star grey"></i></span>
                                            </div>
                                        </div><!-- end tour-title -->
                                    </div><!-- end tour-info -->
                                </div><!-- end tour-block -->
                            </div><!-- end item -->
                            
                            <div class="item">
                                <div class="main-block tour-block">
                                    <div class="main-img">
                                        <a href="tour/6">
                                        	<img src="<c:url value="/images/NhaTrang.jpg"/>" class="img-responsive" alt="tour-img" />
                                    	</a>
                                    </div><!-- end offer-img -->
                                    
                                    <div class="offer-price-2">
                                        <ul class="list-unstyled">
                                            <li class="price">2.550.000đ<a href="tour/6" ><span class="arrow"><i class="fa fa-angle-right"></i></span></a></li>
                                        </ul>
                                    </div><!-- end offer-price-2 -->
                                        
                                    <div class="main-info tour-info">
                                    	<div class="main-title tour-title">
                                            <a href="tour/6">Tham Quan Nha Trang</a>
                                            <p>Nha Trang</p>
                                            <div class="rating">
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star grey"></i></span>
                                            </div>
                                        </div><!-- end tour-title -->
                                    </div><!-- end tour-info -->
                                </div><!-- end tour-block -->
                            </div><!-- end item -->
                            
                            <div class="item">
                                <div class="main-block tour-block">
                                    <div class="main-img">
                                        <a href="tour/5">
                                        	<img src="<c:url value="/images/tokyo.jpg"/>" class="img-responsive" alt="tour-img" />
                                    	</a>
                                    </div><!-- end offer-img -->
                                    
                                    <div class="offer-price-2">
                                        <ul class="list-unstyled">
                                            <li class="price">6.500.000<a href="tour/5" ><span class="arrow"><i class="fa fa-angle-right"></i></span></a></li>
                                        </ul>
                                    </div><!-- end offer-price-2 -->
                                        
                                    <div class="main-info tour-info">
                                    	<div class="main-title tour-title">
                                            <a href="tour/5">Tham Quan Nhật Bản</a>
                                            <p>Nhật Bản</p>
                                            <div class="rating">
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star grey"></i></span>
                                            </div>
                                        </div><!-- end tour-title -->
                                    </div><!-- end tour-info -->
                                </div><!-- end tour-block -->
                            </div><!-- end item -->
                            
                            <div class="item">
                                <div class="main-block tour-block">
                                    <div class="main-img">
                                        <a href="tour/7">
                                        	<img src="<c:url value="/images/PhuQuoc.jpg"/>" class="img-responsive" alt="tour-img" />
                                    	</a>
                                    </div><!-- end offer-img -->
                                    
                                    <div class="offer-price-2">
                                        <ul class="list-unstyled">
                                            <li class="price">3.250.000<a href="tour/7" ><span class="arrow"><i class="fa fa-angle-right"></i></span></a></li>
                                        </ul>
                                    </div><!-- end offer-price-2 -->
                                        
                                    <div class="main-info tour-info">
                                    	<div class="main-title tour-title">
                                            <a href="tour/7">Tham Quan Phú Quốc</a>
                                            <p>Phú Quốc</p>
                                            <div class="rating">
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star orange"></i></span>
                                                <span><i class="fa fa-star grey"></i></span>
                                            </div>
                                        </div><!-- end tour-title -->
                                    </div><!-- end tour-info -->
                                </div><!-- end tour-block -->
                            </div><!-- end item -->
                            
                        </div><!-- end owl-tour-offers -->
                        
                        <div class="view-all text-center">
                        	<a href="/page/tour-list" class="btn btn-black">View All</a>
                        </div><!-- end view-all -->
                    </div><!-- end columns -->
                </div><!-- end row -->
        	</div><!-- end container -->
        </section><!-- end tour-offers -->
        
        
        <!--======================= BEST FEATURES ======================-->
        <section id="best-features" class="banner-padding black-features">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="b-feature-block">
                            <span><i class="fa fa-dollar"></i></span>
                            <h3>Đảm bảo giá tốt nhất</h3>
                            <p>Ưu đãi và quà tặng hấp dẫn khi mua tour online</p>
                        </div><!-- end b-feature-block -->
                    </div><!-- end columns -->

                    <div class="col-sm-6 col-md-3">
                        <div class="b-feature-block">
                            <span><i class="fa fa-lock"></i></span>
                            <h3>An toàn và Bảo mật</h3>
                            <p>Được bảo mật bởi tổ chức quốc tế Global Sign</p>
                        </div><!-- end b-feature-block -->
                    </div><!-- end columns -->

                    <div class="col-sm-6 col-md-3">
                        <div class="b-feature-block">
                            <span><i class="fa fa-thumbs-up"></i></span>
                            <h3>Đại lý du lịch tốt nhất</h3>
                            <p>Thương hiệu lữ hành hàng đầu Việt Nam</p>
                        </div><!-- end b-feature-block -->
                    </div><!-- end columns -->

                    <div class="col-sm-6 col-md-3">
                        <div class="b-feature-block">
                            <span><i class="fa fa-bars"></i></span>
                            <h3>Tư Vấn Miễn Phí</h3>
                            <p>Hỗ trợ tư vấn online miễn phí</p>
                        </div><!-- end b-feature-block -->
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end best-features -->
		
        
        <!--================ HOT-TOUR ==============-->
        <section id="hot-tour" class="section-padding">
        	<div class="container">
        		<div class="row">
        			<div class="col-sm-12 col-md-12 col-lg-7 hot-tour-carousel">
                    	<div id="hot-tour-carousel" class="carousel slide" data-ride="carousel">

                            <div class="carousel-inner">
                                <div class="item active">
                                	<img src="<c:url value="/images/con-dao.jpg"/>" alt="Los Angeles">
                                </div>
                                <div class="item">
                                	<img src="<c:url value="/images/Mo-CHiSau.jpg"/>" alt="Chicago">
                                </div>
                                <div class="item">
                                    <img src="<c:url value="/images/Nhatu-ConDao.jpg"/>" alt="Chicago">
                                </div>
                            </div><!-- end carousel-inner -->
                            
                            <a class="left carousel-control" href="#hot-tour-carousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#hot-tour-carousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div><!-- end hot-tour-carousel -->
                    </div><!-- end columns -->
                    
                    <div class="col-sm-12 col-md-12 col-lg-5 hot-tour-text">
                    	<h3>ƯU ĐÃI HẤP DẪN</h3>
                        <h2 class="hot-tour-title">Côn Đảo</h2>
                        <p>6.300.000đ / 2 Người / 4 Đêm</p>
                        <a href="tour/8" class="btn btn-orange">Đặt Bây Giờ</a>
                        <a href="tour/8" class="btn btn-details">Xem Chi Tiết</a>
                    </div><!-- end columns -->
                    
                </div><!-- end row -->
        	</div><!-- end container -->
        </section><!-- end hot-tour -->
                
		
        <!--=================== PACKAGES ================-->
        <section id="tour-packages" class="section-padding"> 
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">         	
                        <div class="page-heading">
                        	<h2>Gói Tham Quan Hàng Đầu</h2>
                            <hr class="heading-line" />
                        </div><!-- end page-heading -->
                        
                        <div class="row" id="tour-package-tables">

                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
                                <div class="package tour-package">        
                                    <div class="t-pkg-heading">
                                        <h2 class="t-pkg-title">ĐỘC THÂN</h2>
                                        <h2 class="t-pkg-price">1.000.000 / Đêm</h2>
                                    </div><!-- end t-pkg-heading -->
                                    
                                    <div class="pkg-features">
                                        <ul class="list-unstyled text-center">
                                            <li>Bữa ăn sáng</li>
                                            <li>Ban công riêng</li>
                                            <li>Hướng Biển</li>
                                            <li>WIFI miễn phí</li>
                                            <li>Phòng Tắm</li>
                                            <li>Hồ bơi nước nóng</li>
                                        </ul>
                                        <button class="btn">Chọn Gói</button>
                                    </div><!-- end features --> 
                                </div><!-- end tour-package -->
                            </div><!-- end columns -->
                            
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
                                <div class="package tour-package  best-package">        
                                    <div class="t-pkg-heading">
                                        <h2 class="t-pkg-title">2 NGƯỜI</h2>
                                        <h2 class="t-pkg-price">1.350.000đ / Đêm</h2>
                                    </div><!-- end t-pkg-heading -->

                                    <div class="pkg-features">
                                        <ul class="list-unstyled text-center">
                                            <li>Bữa ăn sáng</li>
                                            <li>Ban công riêng</li>
                                            <li>Hướng Biển</li>
                                            <li>WIFI miễn phí</li>
                                            <li>Phòng Tắm</li>
                                            <li>Hồ bơi nước nóng</li>
                                        </ul>
                                        <button class="btn">Chọn Gói</button>
                                    </div><!-- end features --> 
                                </div><!-- end tour-package -->
                            </div><!-- end columns -->
                            
                            <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 text-center">
                                <div class="package tour-package">        
                                    <div class="t-pkg-heading">
                                        <h2 class="t-pkg-title">GIA ĐÌNH</h2>
                                        <h2 class="t-pkg-price">$2.500.000 / Đêm</h2>
                                    </div><!-- end t-pkg-heading -->

                                    <div class="pkg-features">
                                        <ul class="list-unstyled text-center">
                                            <li>Bữa ăn sáng</li>
                                            <li>Ban công riêng</li>
                                            <li>Hướng Biển</li>
                                            <li>WIFI miễn phí</li>
                                            <li>Phòng Tắm</li>
                                            <li>Hồ bơi nước nóng</li>
                                        </ul>
                                        <button class="btn">Chọn Gói</button>
                                    </div><!-- end features --> 
                                </div><!-- end tour-package -->
                            </div><!-- end columns -->

                        </div><!-- end row -->
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end tour-packages -->
        
        
        <!--=============== TESTIMONIALS ===============-->
        <section id="testimonials" class="section-padding">
        	<div class="container">
        		<div class="row">
        			<div class="col-sm-12">
                    	<div class="page-heading white-heading">
                        	<h2>Testimonials</h2>
                            <hr class="heading-line" />
                        </div><!-- end page-heading -->

                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <div class="carousel-inner text-center">
                            
                                <div class="item active">
                                    <blockquote>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum at, pro an eros perpetua ullamcorper Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum at, pro an eros perpetua ullamcorper.</blockquote>
                                    <div class="rating">
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star lightgrey"></i></span>
                                    </div><!-- end rating -->
                                    
                                    <small>Jhon Smith</small>
                                </div><!-- end item -->
                                
                                <div class="item">
                                    <blockquote>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum at, pro an eros perpetua ullamcorper Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum at, pro an eros perpetua ullamcorper.</blockquote>
                                    <div class="rating">
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star lightgrey"></i></span>
                                    </div><!-- end rating -->
                                            
                                    <small>Jhon Smith</small>
                                </div><!-- end item -->
                                
                                <div class="item">
                                    <blockquote>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum at, pro an eros perpetua ullamcorper Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum at, pro an eros perpetua ullamcorper.</blockquote>
                                    <div class="rating">
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star orange"></i></span>
                                        <span><i class="fa fa-star lightgrey"></i></span>
                                    </div><!-- end rating -->
                                    
                                    <small>Jhon Smith</small>
                                </div><!-- end item -->
                                
                            </div><!-- end carousel-inner -->
                            
                            <ol class="carousel-indicators">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active"><img src="<c:url value="/images/client-1.jpg"/>" class="img-responsive"  alt="client-img">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="1"><img src="<c:url value="/images/client-2.jpg"/>" class="img-responsive"  alt="client-img">
                                </li>
                                <li data-target="#quote-carousel" data-slide-to="2"><img src="<c:url value="/images/client-3.jpg"/>" class="img-responsive"  alt="client-img">
                                </li>
                            </ol>
        
                        </div><!-- end quote-carousel -->
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end testimonials --> 

        
        <!--============== HIGHLIGHTS =============-->
        <section id="highlights" class="highlights-2"> 
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="row">
                            <div id="boxes">
                            
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">  
                                    <div class="highlight-box">
                                        <div class="h-icon">
                                            <span><i class="fa fa-plane"></i></span>
                                        </div><!-- end h-icon -->
                                        
                                        <div class="h-text">
                                            <span class="numbers">100</span>
                                            <p>Chuyến Tham Quan Nổi Bật</p>
                                        </div><!-- end h-text -->                           
                                    </div><!-- end highlight-box -->                       
                                </div><!-- end columns -->
                                
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">  
                                    <div class="highlight-box">
                                        <div class="h-icon">
                                            <span><i class="fa fa-ship"></i></span>
                                        </div><!-- end h-icon -->
                                        
                                        <div class="h-text cruise">
                                            <span class="numbers">255</span>
                                            <p>Du Thuyền Trên Toàn Thế Giới</p>
                                        </div><!-- end h-text -->                           
                                    </div><!-- end highlight-box -->                       
                                </div><!-- end columns -->
                                
                                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">  
                                    <div class="highlight-box">
                                        <div class="h-icon">
                                            <span><i class="fa fa-taxi"></i></span>
                                        </div><!-- end h-icon -->
                                        
                                        <div class="h-text taxi">
                                            <span class="numbers">198</span>
                                            <p>Xe Sang Trọng</p>
                                        </div><!-- end h-text -->                           
                                    </div><!-- end highlight-box -->                       
                                </div><!-- end columns -->
                                
                            </div><!-- end boxes -->
                    	</div><!-- end row -->
            		</div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end highlights -->
        
        
        <!--========================= NEWSLETTER-1 ==========================-->
        <section id="newsletter-1" class="section-padding back-size newsletter">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <h2>ĐĂNG KÝ BẢN TIN CỦA CHÚNG TÔI</h2>
                        <p>ĐĂNG KÝ ĐỂ NHẬN NHỮNG CẬP NHẬT THÚ VỊ CỦA CHÚNG TÔI</p>
                        <form>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="email" class="form-control input-lg" placeholder="Enter your email address" required/>
                                    <span class="input-group-btn"><button class="btn btn-lg"><i class="fa fa-envelope"></i></button></span>
                                </div>
                            </div>
                        </form>
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end newsletter-1 -->
        
        
        <!--======================= FOOTER =======================-->
        <section id="footer" class="ftr-heading-w ftr-heading-mgn-2">
        
            <div id="footer-top" class="banner-padding ftr-top-grey ftr-text-grey">
                <div class="container">
                    <div class="row">

                        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-5 footer-widget ftr-about ftr-our-company">
                            <h3 class="footer-heading">Công Ty Của Chúng Tôi</h3>
                            <p>Lữ hành OHANATRAVELS, thương hiệu lữ hành hàng đầu Việt Nam Thương hiệu quốc gia</p>
                            <ul class="social-links list-inline list-unstyled">
                            	<li><a href="#"><span><i class="fa fa-facebook"></i></span></a></li>
                            	<li><a href="#"><span><i class="fa fa-twitter"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-google-plus"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-pinterest-p"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-instagram"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-linkedin"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-youtube-play"></i></span></a></li>
                            </ul>
                        </div><!-- end columns -->
                        
<%--                        <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 footer-widget ftr-instagram">--%>
<%--                            <h3 class="footer-heading">Instagram Post</h3>--%>
<%--                            <ul class="list-unstyled instagram-list list-inline">--%>
<%--                            	<li><a href="#"><img src="<c:url value="/images/ftr-instagram-1.jpg"/>" class="img-responsive" alt="instagram-img" /></a></li>--%>
<%--                            	<li><a href="#"><img src="<c:url value="/images/ftr-instagram-2.jpg"/>" class="img-responsive" alt="instagram-img" /></a></li>--%>
<%--                                <li><a href="#"><img src="<c:url value="/images/ftr-instagram-3.jpg"/>" class="img-responsive" alt="instagram-img" /></a></li>--%>
<%--                                <li><a href="#"><img src="<c:url value="/images/ftr-instagram-4.jpg"/>" class="img-responsive" alt="instagram-img" /></a></li>--%>
<%--                                <li><a href="#"><img src="<c:url value="/images/ftr-instagram-5.jpg"/>" class="img-responsive" alt="instagram-img" /></a></li>--%>
<%--                                <li><a href="#"><img src="<c:url value="/images/ftr-instagram-6.jpg"/>" class="img-responsive" alt="instagram-img" /></a></li>--%>
<%--                            </ul>--%>
<%--                        </div><!-- end columns -->--%>
                        
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 footer-widget ftr-contact">
                            <h3 class="footer-heading">Liên Hệ Tới Chúng Tôi</h3>
                            <ul class="list-unstyled">
                            	<li><span><i class="fa fa-map-marker"></i></span>Trâu Quỳ, Gia Lâm, Hà Nội</li>
                            	<li><span><i class="fa fa-phone"></i></span>+00 123 4567</li>
                                <li><span><i class="fa fa-envelope"></i></span>info@starhotel.com</li>
                            </ul>
                        </div><!-- end columns -->
                        
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end footer-top -->

            <div id="footer-bottom" class="ftr-bot-black">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="copyright">
                            <p>© 2017 <a href="#">OhanaTravel</a>. All rights reserved.</p>
                        </div><!-- end columns -->
                        
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="terms">
                            <ul class="list-unstyled list-inline">
                            	<li><a href="#">Terms & Condition</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div><!-- end columns -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end footer-bottom -->
            
        </section><!-- end footer -->
        <!-- Page Scripts Starts -->
        <script src="<c:url value="/js/jquery.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/js/jquery.flexslider.js"/>"></script>
        <script src="<c:url value="/js/bootstrap-datepicker.js"/>"></script>
        <script src="<c:url value="/js/owl.carousel.min.js"/>"></script>
        <script src="<c:url value="/js/custom-navigation.js"/>"></script>
        <script src="<c:url value="/js/custom-flex.js"/>"></script>





            t>
        <script src="<c:url value="/js/custom-owl.js"/>"></script>
        <script src="<c:url value="/js/custom-date-picker.js"/>"></script>
        <!-- Page Scripts Ends -->

    </body>
</html>