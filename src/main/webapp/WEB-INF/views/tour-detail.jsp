<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!doctype html>
<html lang="en">
    <head>
        <title>Tour Detail Left Sidebar</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="icon" href="<c:url value="/images/favicon.png"/>" type="image/x-icon">
        
        <!-- Google Fonts -->	
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">
        
        <!-- Bootstrap Stylesheet -->	
        <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">
        
        <!-- Font Awesome Stylesheet -->
        <link rel="stylesheet" href="<c:url value="/css/font-awesome.min.css"/>">
            
        <!-- Custom Stylesheets -->	
        <link rel="stylesheet" href="<c:url value="/css/style.css"/>">
        <link rel="stylesheet" id="cpswitch" href="<c:url value="/css/orange.css"/>">
        <link rel="stylesheet" href="<c:url value="/css/responsive.css"/>">
        
        <!--Date-Picker Stylesheet-->
        <link rel="stylesheet" href="<c:url value="/css/datepicker.css"/>">
        
        <!-- Slick Stylesheet -->
		<link rel="stylesheet" href="<c:url value="/css/slick.css"/>">
        <link rel="stylesheet" href="<c:url value="/css/slick-theme.css"/>">
    </head>
    
    
    <body>
    
        <!--====== LOADER =====-->
        <div class="loader"></div>
    
    
    	<!--======== SEARCH-OVERLAY =========-->       
        <div class="overlay">
            <a href="javascript:void(0)" id="close-button" class="closebtn">&times;</a>
            <div class="overlay-content">
                <div class="form-center">
                    <form>
                    	<div class="form-group">
                        	<div class="input-group">
                        		<input type="text" class="form-control" placeholder="Search..." required />
                            	<span class="input-group-btn"><button type="submit" class="btn"><span><i class="fa fa-search"></i></span></button></span>
                            </div><!-- end input-group -->
                        </div><!-- end form-group -->
                    </form>
                </div><!-- end form-center -->
            </div><!-- end overlay-content -->
        </div><!-- end overlay -->
        
        
        <!--============= TOP-BAR ===========-->
        <div id="top-bar" class="tb-text-white">
            <div class="container">
                <div class="row">          
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div id="info">
                            <ul class="list-unstyled list-inline">
                                <li><span><i class="fa fa-map-marker"></i></span>Trâu Quỳ, Gia Lâm, Hà Nội</li>
                                <li><span><i class="fa fa-phone"></i></span>+0987776555</li>
                            </ul>
                        </div><!-- end info -->
                    </div><!-- end columns -->
                    
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <div id="links">
                            <ul class="list-unstyled list-inline">
                                <li><a href="login"><span><i class="fa fa-lock"></i></span>Login</a></li>
                                <li><a href="registration.jsp"><span><i class="fa fa-plus"></i></span>Sign Up</a></li>
<%--                                <li>--%>
<%--                                	<form>--%>
<%--                                    	<ul class="list-inline">--%>
<%--                                        	<li>--%>
<%--                                                <div class="form-group currency">--%>
<%--                                                    <span><i class="fa fa-angle-down"></i></span>--%>
<%--                                                    <select class="form-control">--%>
<%--                                                        <option value="">$</option>--%>
<%--                                                        <option value="">£</option>--%>
<%--                                                    </select>--%>
<%--                                                </div><!-- end form-group -->--%>
<%--											</li>--%>
<%--                                            --%>
<%--                                            <li>--%>
<%--                                                <div class="form-group language">--%>
<%--                                                    <span><i class="fa fa-angle-down"></i></span>--%>
<%--                                                    <select class="form-control">--%>
<%--                                                        <option value="">EN</option>--%>
<%--                                                        <option value="">UR</option>--%>
<%--                                                        <option value="">FR</option>--%>
<%--                                                        <option value="">IT</option>--%>
<%--                                                    </select>--%>
<%--                                                </div><!-- end form-group -->--%>
<%--                                            </li>--%>
<%--										</ul>--%>
<%--                                    </form>--%>
<%--                                </li>--%>
                            </ul>
                        </div><!-- end links -->
                    </div><!-- end columns -->				
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end top-bar -->

        <nav class="navbar navbar-default main-navbar navbar-custom navbar-white" id="mynavbar-1">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" id="menu-button">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="header-search hidden-lg">
                        <a href="javascript:void(0)" class="search-button"><span><i class="fa fa-search"></i></span></a>
                    </div>
                    <a href="tour" class="navbar-brand"><span><i class="fa fa-plane"></i>OHANA</span>TRAVELS</a>
                </div><!-- end navbar-header -->
                
                <div class="collapse navbar-collapse" id="myNavbar1">
                    <ul class="nav navbar-nav navbar-right navbar-search-link">
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Home<span><i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="tour">Homepage</a></li>
                            </ul>			
                        </li>
<%--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Flights<span><i class="fa fa-angle-down"></i></span></a>--%>
<%--                            <ul class="dropdown-menu">--%>
<%--                            	<li><a href="flight-homepage.html">Flight Homepage</a></li>--%>
<%--                                <li><a href="flight-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
<%--                                <li><a href="flight-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
<%--                                <li><a href="flight-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
<%--                                <li><a href="flight-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
<%--                                <li><a href="flight-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
<%--                                <li><a href="flight-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
<%--                            </ul>			--%>
<%--                        </li>--%>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Hotels<span><i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown-menu">
                            	<li><a href="tour">Homepage</a></li>
                                <li><a href="hotel-listing-left-sidebar.html">List View Left Sidebar</a></li>
                                <li><a href="hotel-listing-right-sidebar.html">List View Right Sidebar</a></li>
                                <li><a href="hotel-list.jsp">Grid View Left Sidebar</a></li>
                                <li><a href="hotel-grid-right-sidebar.html">Grid View Right Sidebar</a></li>
                                <li><a href="hotel-detail.jsp">Detail Left Sidebar</a></li>
                                <li><a href="hotel-detail-right-sidebar.html">Detail Right Sidebar</a></li>
                            </ul>			
                        </li>
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tours<span><i class="fa fa-angle-down"></i></span></a>
                            <ul class="dropdown-menu mega-dropdown-menu row">
                                <li>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Miền Bắc</span></li>
                                                <c:forEach items="${data}" var="tour">
                                                    <c:if test="${tour.tourType == 1}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>

                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Miền Trung</span></li>
                                                <c:forEach items="${data}" var="tour">
                                                    <c:if test="${tour.tourType == 2}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>

                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Miền Nam</span></li>
                                                <c:forEach items="${data}" var="tour">
                                                    <c:if test="${tour.tourType == 3}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                        <div class="col-md-3">
                                            <ul class="list-unstyled">
                                                <li class="dropdown-header"><span>Ngoài Nước</span></li>
                                                <c:forEach items="${data}" var="tour">
                                                    <c:if test="${tour.tourType == 4}">
                                                        <li><a href="tour/${tour.id}">${tour.name}</a></li>
                                                    </c:if>
                                                </c:forEach>
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>

                    <%--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cruise<span><i class="fa fa-angle-down"></i></span></a>--%>
<%--                            <ul class="dropdown-menu">--%>
<%--                            	<li><a href="cruise-homepage.html">Cruise Homepage</a></li>--%>
<%--                                <li><a href="cruise-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
<%--                                <li><a href="cruise-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
<%--                                <li><a href="cruise-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
<%--                                <li><a href="cruise-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
<%--                                <li><a href="cruise-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
<%--                                <li><a href="cruise-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
<%--                            </ul>			--%>
<%--                        </li>--%>
<%--                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cars<span><i class="fa fa-angle-down"></i></span></a>--%>
<%--                            <ul class="dropdown-menu">--%>
<%--                            	<li><a href="car-homepage.html">Car Homepage</a></li>--%>
<%--                                <li><a href="car-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
<%--                                <li><a href="car-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
<%--                                <li><a href="car-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
<%--                                <li><a href="car-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
<%--                                <li><a href="car-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
<%--                                <li><a href="car-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
<%--                            </ul>			--%>
<%--                        </li>--%>
                        <li><a href="javascript:void(0)" class="search-button"><span><i class="fa fa-search"></i></span></a></li>
                    </ul>
                </div><!-- end navbar collapse -->
            </div><!-- end container -->
        </nav><!-- end navbar -->
        <!--================ PAGE-COVER ================-->
        <section class="page-cover" id="cover-tour-detail">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                    	<h1 class="page-title">${tour.title}</h1>
                        <ul class="breadcrumb">
                            <li><a href="tour">Home</a></li>
<%--                            <li class="active"></li>--%>
                        </ul>
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end page-cover -->
        
        
        <!--===== INNERPAGE-WRAPPER ====-->
        <section class="innerpage-wrapper">
        	<div id="tour-details" class="innerpage-section-padding">
                <div class="container">
                    <div class="row">        	
                        
                        <div class="col-xs-12 col-sm-12 col-md-3 side-bar left-side-bar">
                            
                            <div class="side-bar-block booking-form-block">
                            	<h2 class="selected-price"><span>${tour.name}</span></h2>
                            
                            	<div class="booking-form">
                                	<h3>Đặt Tour</h3>
                                    <p>Tìm chuyến du lịch mơ ước của bạn ngay hôm nay</p>
                                    
                                    <form id = "formAdd">
                                        <div class="form-group">
                                            <h5 class="selected-price"><span>Giá Người Lớn ${tours.price}đ</span></h5>
                                        </div>
                                        <div class="form-group">
                                            <h5 class="selected-price"><span>Giá Trẻ Em    ${tours.priceBaby}đ</span></h5>
                                        </div>
                                        <div class="form-group">
                                    		<input type="text" class="form-control" placeholder="Họ Và Tên" required/>
                                        </div>
                                        
                                        <div class="form-group">
                                    		<input type="email" class="form-control" placeholder="Email" required/>
                                        </div>
                                        
                                        <div class="form-group">
                                    		<input type="text" class="form-control" placeholder="Phone" required/>
                                        </div>
                                        
                                        <div class="form-group">
                                    		<input type="text" class="form-control dpd3" placeholder="Chọn Thời Gian" required/>
                                        </div>
                                        
                                        <div class="row">
                                        	<div class="col-sm-6 col-md-12 col-lg-6 no-sp-r">
                                                <div class="form-group right-icon">
                                                    <select class="form-control">
                                                        <option selected>Người Lớn</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                    </select>
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-6 col-md-12 col-lg-6 no-sp-l">    
                                                <div class="form-group right-icon">
                                                    <select class="form-control">
                                                        <option selected>Trẻ em</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                    </select>
                                                    <i class="fa fa-angle-down"></i>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group right-icon">
                                            <select class="form-control">
                                                <option selected>Phương Thức Thanh </option>
                                                <option>Thẻ tín dụng</option>
                                                <option>Paypal</option>
                                            </select>
                                            <i class="fa fa-angle-down"></i>
                                        </div>
                                        
                                        <div class="checkbox custom-check">
                                        	<input type="checkbox" id="check01" name="checkbox"/>
                                            <label for="check01"><span><i class="fa fa-check"></i></span>Bằng cách tiếp tục, bạn đồng ý với các<a href="#"> Điều khoản & Điều kiện.</a></label>
                                        </div>
                                        <button class="btn btn-block btn-orange" id="btnAdd">Xác nhận tại chỗ</button>
                                    </form>
                         
                                </div><!-- end booking-form -->
                            </div><!-- end side-bar-block -->
                            
                            <div class="row">
<%--                                <div class="col-xs-12 col-sm-6 col-md-12">--%>
<%--                                    <div class="side-bar-block main-block ad-block">--%>
<%--                                        <div class="main-img ad-img">--%>
<%--                                            <a href="#">--%>
<%--                                                <img src="<c:url value="/images/car-ad.jpg"/>" class="img-responsive" alt="car-ad" />--%>
<%--                                                <div class="ad-mask">--%>
<%--                                                    <div class="ad-text">--%>
<%--                                                        <span>Luxury</span>--%>
<%--                                                        <h2>Car</h2>--%>
<%--                                                        <span>Offer</span>--%>
<%--                                                    </div><!-- end ad-text -->--%>
<%--                                                </div><!-- end columns -->--%>
<%--                                            </a>--%>
<%--                                        </div><!-- end ad-img -->--%>
<%--                                    </div><!-- end side-bar-block -->--%>
<%--                                </div><!-- end columns -->--%>
                                
                                <div class="col-xs-12 col-sm-6 col-md-12">    
                                    <div class="side-bar-block support-block">
                                        <h3>Cần giúp đỡ</h3>
                                        <p>Nếu bạn cần giúp đỡ hãy gọi tới số điện thoại</p>
                                        <div class="support-contact">
                                            <span><i class="fa fa-phone"></i></span>
                                            <p>+1 123 1234567</p>
                                        </div><!-- end support-contact -->
                                    </div><!-- end side-bar-block -->
                                </div><!-- end columns -->
                                
                            </div><!-- end row -->
                        </div><!-- end columns -->
                        
                        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">
                            
                            <div class="detail-slider">
                                <div class="feature-slider">
                                    <c:forEach items="${tour.albums}" var="img">
                                    <div><img src="<c:url value="${img}"/>" class="img-responsive" alt="feature-img"/></div>
                                    </c:forEach>
                                </div><!-- end feature-slider -->
                                <div class="feature-slider-nav">
                                    <c:forEach items="${tour.albums}" var="imgs">
                                    <div><img src="<c:url value="${imgs}"/>" class="img-responsive" alt="feature-thumb"/></div>
                                    </c:forEach>
                                </div><!-- end feature-slider-nav -->
                                
                                <ul class="list-unstyled features tour-features">
                                	<li><div class="f-icon"><i class="fa fa-wheelchair"></i></div><div class="f-text"><p class="f-heading">CÒN</p><p class="f-data">${tours.seats} NGƯỜI</p></div></li>
                                    <li><div class="f-icon"><i class="fa fa-clock-o"></i></div><div class="f-text"><p class="f-heading">GIẢM GÍA</p><p class="f-data">${tours.discount}</p></div></li>
                                </ul>
                            </div><!-- end detail-slider -->  

                            <div class="detail-tabs">
                            	<ul class="nav nav-tabs nav-justified">
                                    <li class="active"><a href="#tour-information" data-toggle="tab">THÔNG TIN TOUR</a></li>
                                    <li><a href="#flight" data-toggle="tab">CHUYẾN BAY</a></li>
<%--                                    <li><a href="#map-overview" data-toggle="tab">MAP</a></li>--%>
                                </ul>
                            	
                                <div class="tab-content">

                                    <div id="tour-information" class="tab-pane in active">
                                    	<div class="row">
                                    		<div class="col-sm-4 col-md-4 tab-img">
                                        		<img src="<c:url value="/images/dinh-tinh-yeu-condao%20(1).jpg"/>" class="img-responsive" alt="flight-detail-img" />
                                            </div><!-- end columns -->
                                            <div class="col-sm-8 col-md-8 tab-text">
                                        		<h3>Thông Tin Tour</h3>
                                                <p>${tour.content}</p>
                                            </div><!-- end columns -->
                                        </div><!-- end row -->
                                    </div><!-- end hotel-overview -->
                                	
                                    <div id="flight" class="tab-pane">
                                    	<div class="row">
                                            <div class="col-sm-8 col-md-8 tab-text">
                                        		<h3>Chuyến Bay</h3>
                                                <p>${tour.vehicle}</p>
                                            </div><!-- end columns -->
                                        </div><!-- end row -->
                                    </div><!-- end restaurant -->
                                    
<%--                                    <div id="map-overview" class="tab-pane">--%>
<%--                                    	<div class="row">--%>
<%--                                    		<div class="col-sm-4 col-md-4 tab-img">--%>
<%--                                        		<img src="<c:url value="/images/hotel-detail-tab-3.jpg"/>" class="img-responsive" alt="flight-detail-img" />--%>
<%--                                            </div><!-- end columns -->--%>
<%--                                        	--%>
<%--                                            <div class="col-sm-8 col-md-8 tab-text">--%>
<%--                                        		<h3>Xem Bản Đồ</h3>--%>
<%--                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>--%>
<%--                                            </div><!-- end columns -->--%>
<%--                                        </div><!-- end row -->--%>
<%--                                    </div><!-- end pick-up -->--%>
                                    
                                </div><!-- end tab-content -->
                            </div><!-- end detail-tabs -->
                            
                            <div class="available-blocks" id="available-tours">
                            	<h2>Các chuyến tham quan có sẵn</h2>
                            	<div class="list-block main-block t-list-block">
                                    <div class="list-content">
                                        <div class="main-img list-img t-list-img">
                                            <a href="tour/2">
                                                <img src="<c:url value="/images/da-nang11.jpg"/>" class="img-responsive" alt="tour-img" />
                                            </a>
                                            <div class="main-mask">
                                                <ul class="list-unstyled list-inline offer-price-1">
                                                    <li class="price">2.200.000đ<span class="divider">|</span><span class="pkg">3 Ngày</span></li>
                                                    <li class="rating">
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star lightgrey"></i></span>
                                                    </li>
                                                </ul>
                                            </div><!-- end main-mask -->
                                        </div><!-- end t-list-img -->
                                        
                                        <div class="list-info t-list-info">
                                            <h3 class="block-title"><a href="tour/2">Đà Nẵng</a></h3>
                                            <p class="block-minor">2 Người Lớn</p>
                                            <p>DU LỊCH ĐÀ NẴNG - HỘI AN - RỪNG DỪA BẨY MẪU</p>
                                            <a href="tour/2" class="btn btn-orange btn-lg">View More</a>
                                         </div><!-- end t-list-info -->
                                    </div><!-- end list-content -->
                                </div><!-- end t-list-block -->
								
                                <div class="list-block main-block t-list-block">
                                    <div class="list-content">
                                        <div class="main-img list-img t-list-img">
                                            <a href="tour-detail.jsp">
                                                <img src="<c:url value="/images/sapa1.jpg"/>" class="img-responsive" alt="tour-img" />
                                            </a>
                                            <div class="main-mask">
                                                <ul class="list-unstyled list-inline offer-price-1">
                                                    <li class="price">2.200.000đ<span class="divider">|</span><span class="pkg">2 Ngày</span></li>
                                                    <li class="rating">
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star lightgrey"></i></span>
                                                    </li>
                                                </ul>
                                            </div><!-- end main-mask -->
                                        </div><!-- end t-list-img -->
                                        
                                        <div class="list-info t-list-info">
                                            <h3 class="block-title"><a href="tour/3">Sapa</a></h3>
                                            <p class="block-minor">2 Người</p>
                                            <p>DU LỊCH HÀ NỘI - SAPA - CHINH PHỤC FANSIPAN</p>
                                            <a href="tour/3" class="btn btn-orange btn-lg">View More</a>
                                         </div><!-- end t-list-info -->
                                    </div><!-- end list-content -->
                                </div><!-- end t-list-block -->
                                
                                <div class="list-block main-block t-list-block">
                                    <div class="list-content">
                                        <div class="main-img list-img t-list-img">
                                            <a href="tour/6">
                                                <img src="<c:url value="/images/nha-trang1.jpg"/>" class="img-responsive" alt="tour-img" />
                                            </a>
                                            <div class="main-mask">
                                                <ul class="list-unstyled list-inline offer-price-1">
                                                    <li class="price">3.500.000đ<span class="divider">|</span><span class="pkg">3 Ngày</span></li>
                                                    <li class="rating">
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star lightgrey"></i></span>
                                                    </li>
                                                </ul>
                                            </div><!-- end main-mask -->
                                        </div><!-- end t-list-img -->
                                        
                                        <div class="list-info t-list-info">
                                            <h3 class="block-title"><a href="tour/8">Nha Trang</a></h3>
                                            <p class="block-minor">2 Người</p>
                                            <p>Du Lịch Nha Trang - Đà Lạt</p>
                                            <a href="tour/6" class="btn btn-orange btn-lg">View More</a>
                                         </div><!-- end t-list-info -->
                                    </div><!-- end list-content -->
                                </div><!-- end t-list-block -->
                                
                                <div class="list-block main-block t-list-block">
                                    <div class="list-content">
                                        <div class="main-img list-img t-list-img">
                                            <a href="tour/8">
                                                <img src="<c:url value="/images/con-dao.jpg"/>" class="img-responsive" alt="tour-img" />
                                            </a>
                                            <div class="main-mask">
                                                <ul class="list-unstyled list-inline offer-price-1">
                                                    <li class="price">5.550.000<span class="divider">|</span><span class="pkg">4 Ngày</span></li>
                                                    <li class="rating">
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star orange"></i></span>
                                                        <span><i class="fa fa-star lightgrey"></i></span>
                                                    </li>
                                                </ul>
                                            </div><!-- end main-mask -->
                                        </div><!-- end t-list-img -->
                                        
                                        <div class="list-info t-list-info">
                                            <h3 class="block-title"><a href="tour/8">Côn Đảo</a></h3>
                                            <p class="block-minor">2 Người</p>
                                            <p>Du Lịch Côn Đảo</p>
                                            <a href="tour/8" class="btn btn-orange btn-lg">View More</a>
                                         </div><!-- end t-list-info -->
                                    </div><!-- end list-content -->
                                </div><!-- end t-list-block -->
                                
                            </div><!-- end available-tours -->
                                                       
                            <div class="pages">
                                <ol class="pagination">
                                    <li><a href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-angle-left"></i></span></a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#" aria-label="Next"><span aria-hidden="true"><i class="fa fa-angle-right"></i></span></a></li>
                                </ol>
                            </div><!-- end pages -->
                        </div><!-- end columns -->

                    </div><!-- end row -->
            	</div><!-- end container -->
            </div><!-- end tour-detail -->
        </section><!-- end innerpage-wrapper -->
        
        
        <!--======================= BEST FEATURES =====================-->
        <section id="best-features" class="banner-padding black-features">
        	<div class="container">
        		<div class="row">
        			<div class="col-sm-6 col-md-3">
                    	<div class="b-feature-block">
                    		<span><i class="fa fa-dollar"></i></span>
                        	<h3>Đảm bảo giá tốt nhất</h3>
                            <p></p>
                        </div><!-- end b-feature-block -->
                   </div><!-- end columns -->
                   
                   <div class="col-sm-6 col-md-3">
                    	<div class="b-feature-block">
                    		<span><i class="fa fa-lock"></i></span>
                        	<h3>An toàn và Bảo mật</h3>
                            <p></p>
                        </div><!-- end b-feature-block -->
                   </div><!-- end columns -->
                   
                   <div class="col-sm-6 col-md-3">
                    	<div class="b-feature-block">
                    		<span><i class="fa fa-thumbs-up"></i></span>
                        	<h3>Đại lý du lịch tốt nhất</h3>
                            <p></p>
                        </div><!-- end b-feature-block -->
                   </div><!-- end columns -->
                   
                   <div class="col-sm-6 col-md-3">
                    	<div class="b-feature-block">
                    		<span><i class="fa fa-bars"></i></span>
                        	<h3>Hướng dẫn du lịch</h3>
                            <p></p>
                        </div><!-- end b-feature-block -->
                   </div><!-- end columns -->
                </div><!-- end row -->
        	</div><!-- end container -->
        </section><!-- end best-features -->
        
        
        <!--========================= NEWSLETTER-1 ==========================-->
        <section id="newsletter-1" class="section-padding back-size newsletter"> 
            <div class="container">
                <div class="row">
                	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <h2>ĐĂNG KÝ BẢN TIN CỦA CHÚNG TÔI</h2>
                        <p>ĐĂNG KÝ ĐỂ NHẬN NHỮNG CẬP NHẬT THÚ VỊ CỦA CHÚNG TÔI</p>
                        <form>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="email" class="form-control input-lg" placeholder="Enter your email address" required/>
                                    <span class="input-group-btn"><button class="btn btn-lg"><i class="fa fa-envelope"></i></button></span>
                                </div>
                            </div>
                        </form>
                    </div><!-- end columns -->
                </div><!-- end row -->
            </div><!-- end container -->
        </section><!-- end newsletter-1 -->
        
        
        <!--======================= FOOTER =======================-->
        <section id="footer" class="ftr-heading-o ftr-heading-mgn-1">
        
            <div id="footer-top" class="banner-padding ftr-top-black ftr-text-white">
                <div class="container">
                    <div class="row">
						
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 footer-widget ftr-contact">
                            <h3 class="footer-heading">LIÊN HỆ CHÚNG TÔI</h3>
                            <ul class="list-unstyled">
                            	<li><span><i class="fa fa-map-marker"></i></span>29 Trâu quỳ, Gia Lâm, Hà Nội</li>
                            	<li><span><i class="fa fa-phone"></i></span>+0974631555</li>
                                <li><span><i class="fa fa-envelope"></i></span>info@starhotel.com</li>
                            </ul>
                        </div><!-- end columns -->
                        
                        <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 footer-widget ftr-links">
                            <h3 class="footer-heading">CÔNG TY</h3>
                            <ul class="list-unstyled">
                            	<li><a href="#">TRANG CHỦ</a></li>
                            	<li><a href="#">VÉ MÁY BAY</a></li>
                                <li><a href="#">KHÁCH SẠN</a></li>
                                <li><a href="#">Tours</a></li>
                                <li><a href="#">DU THUYỀN</a></li>
                                <li><a href="#">Ô TÔ</a></li>
                            </ul>
                        </div><!-- end columns -->
                        
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 footer-widget ftr-links ftr-pad-left">
                        	<h3 class="footer-heading">TÀI NGUYÊN</h3>
                            <ul class="list-unstyled">
                            	<li><a href="#">Blogs</a></li>
                            	<li><a href="#">LIÊN HỆ CHÚNG TÔI</a></li>
                                <li><a href="#">ĐĂNG NHẬP</a></li>
                                <li><a href="#">ĐĂNG KÝ</a></li>
                            </ul>
                        </div><!-- end columns -->

                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 footer-widget ftr-about">
                            <h3 class="footer-heading">VỀ CHÚNG TÔI</h3>
                            <p>“Chúng tôi cam kết luôn nỗ lực đem đến những giá trị dịch vụ tốt nhất cho khách hàng và đối tác để tiếp tục khẳng định vị trí hàng đầu của thương hiệu Lữ hành OHANATRAVEL.”</p>
                            <ul class="social-links list-inline list-unstyled">
                            	<li><a href="#"><span><i class="fa fa-facebook"></i></span></a></li>
                            	<li><a href="#"><span><i class="fa fa-twitter"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-google-plus"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-pinterest-p"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-instagram"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-linkedin"></i></span></a></li>
                                <li><a href="#"><span><i class="fa fa-youtube-play"></i></span></a></li>
                            </ul>
                        </div><!-- end columns -->
                        
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end footer-top -->

            <div id="footer-bottom" class="ftr-bot-black">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="copyright">
                            <p>© 2017 <a href="#">OhanaTravel</a>. All rights reserved.</p>
                        </div><!-- end columns -->
                        
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="terms">
                            <ul class="list-unstyled list-inline">
                            	<li><a href="#">Terms & Condition</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div><!-- end columns -->
                    </div><!-- end row -->
                </div><!-- end container -->
            </div><!-- end footer-bottom -->
            
        </section><!-- end footer -->
        
        
        <!-- Page Scripts Starts -->
        <script src="<c:url value="/js/jquery.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap.min.js"/>"></script>
        <script src="<c:url value="/js/bootstrap-datepicker.js"/>"></script>
        <script src="<c:url value="/js/slick.min.js"/>"></script>
        <script src="<c:url value="/js/custom-navigation.js"/>"></script>
        <script src="<c:url value="/js/custom-date-picker.js"/>"></script>
        <script src="<c:url value="/js/custom-slick.js"/>"></script>
        <!-- Page Scripts Ends -->

    </body>
</html>
