<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<%@include file="/common/admin/head.jsp" %>
<%@include file="/common/admin/header.jsp" %>
<body>
<%@include file="/common/admin/menu.jsp" %>
<div id="wrapper" style="width: 100%;height: 90vh;overflow: auto;">
    <div class="main-content" style="width: 100%">
        <div class="main-content-inner">
            <div class="page-content container">
                <div class="row">
                    <div class="col-md-12">
                        <c:if test="${not empty message}">
                            <div class="alert alert-${alert}">
                                    ${message}
                            </div>
                        </c:if>
                        <h2>Khách sạn</h2>
                        <form id="formSubmit" class="mt-3">
                            <input type="hidden" value="${idHotel}" id="idHotel" name="idHotel" />
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <div class="avatar">
                                            <img id="thumbnail"
                                                 src="https://yt3.ggpht.com/-f6NCDKG2Ukw/AAAAAAAAAAI/AAAAAAAAAAA/MqMm3rgmqCY/s48-c-k-no-mo-rj-c0xffffff/photo.jpg"
                                                 class="img-fluid" style="max-width: 300px; max-height: 300px;" />
                                        </div>
                                        <div class="file-field">
                                            <p>
                                                <strong id="Ithumbnail">Chọn ảnh đại diện</strong><br/>
                                                <button
                                                        class="btn btn-primary btn-sm waves-effect waves-light"
                                                        type="button" value="Browse Image"
                                                        onclick="BrowseServer( 'Images:/', 'Ithumbnail' );" >Browse Image</button>
                                            </p>
                                            <input type="hidden" name="thumbnail" id="image_src"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label class=" control-label float-right mt-2">Số máy bàn</label>

                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="publicPhone" name="publicPhone" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-3">
                                            <label class=" control-label float-right mt-2">Số di động</label>

                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="privatePhone" name="privatePhone" />
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-4">
                                    <label class="col-md-12 control-label">Loại khách sạn:</label>
                                    <div class="col-md-12">
                                        <select class="form-control" id="localeType" name="localeType">
                                            <option disabled selected>--Loại khách sạn--</option>
                                            <option value="1">Trong nước</option>
                                            <option value="2">Nước ngoài</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-sm-3 control-label no-padding-right">Hot</label>
                                    <div class="col-sm-12">
                                        <select class="form-control" id="isHot" name="isHot">
                                            <option value="0">No Hot</option>
                                            <option value="1">Hot</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="col-sm-12control-label no-padding-right">Đơn giá / 24h</label>
                                    <input type="text" class="col-md-12 form-control" id="price" name="price"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Khu vực</label>
                                <div class="col-sm-12">
                                    <textarea type="text" class="form-control" id="address" name="address" ></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Tên khách sạn</label>
                                <div class="col-sm-12">
                                    <textarea type="text" class="form-control" id="name" name="name" ></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right">Mô tả</label>
                                <div class="col-sm-12">
                                    <textarea required type="text" id="description" name="description"></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <c:if test="${idHotel == null}">
                                    <div class="col-sm-12">
                                        <button type="button"  class="btn btn-white btn-warning btn-bold" value="Thêm" id="btnAdd">Thêm mới</button>
                                    </div>
                                </c:if>
                                <c:if test="${idHotel != null}">
                                    <div class="col-sm-12">
                                        <button type="button"  class="btn btn-white btn-warning btn-bold" value="Cập nhật" id="btnUpdate">Cập nhật</button>
                                    </div>
                                </c:if>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@include file="/common/admin/footer.jsp" %>
</div>
<%--<script src="https://code.jquery.com/jquery-3.1.1.min.js"/>--%>
<%@include file="/common/admin/scrip.jsp" %>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script src="<c:url value='/js/admin/hotel/edit.js'/> " ></script>
<script>
    var editor='';
    $(document).ready(function(){
        editor= CKEDITOR.replace('description',
            {
                filebrowserBrowseUrl : 'ckfinder/ckfinder.html',
                filebrowserImageBrowseUrl : 'ckfinder/ckfinder.html?type=Images',
                filebrowserFlashBrowseUrl : 'ckfinder/ckfinder.html?type=Flash',
                filebrowserUploadUrl : 'ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Files',
                filebrowserImageUploadUrl : 'ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Images',
                filebrowserFlashUploadUrl : 'ckfinder/core/connector/java/connector.java?command=QuickUpload&type=Flash'
            });
    });
    /*Avatar start*/
    function BrowseServer(startupPath, functionData) {
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();

        // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.basePath = '../';

        //Startup path in a form: "Type:/path/to/directory/"
        finder.startupPath = startupPath;

        // Name of a function which is called when a file is selected in CKFinder.
        finder.selectActionFunction = SetFileField;

        // Additional data to be passed to the selectActionFunction in a second argument.
        // We'll use this feature to pass the Id of a field that will be updated.
        finder.selectActionData = functionData;

        // Name of a function which is called when a thumbnail is selected in CKFinder. Preview img
        // finder.selectThumbnailActionFunction = ShowThumbnails;

        // Launch CKFinder
        finder.popup();
    }

    function SetFileField(fileUrl, data) {
        document.getElementById(data["selectActionData"]).innerHTML = this
            .getSelectedFile().name;
        document.getElementById("thumbnail").src = fileUrl;
        $('#thumbnail').val(fileUrl);
        $('#image_src').val(fileUrl);

    }


</script>
</body>
</html>

