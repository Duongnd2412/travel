<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<%@include file="/common/admin/head.jsp" %>
<%@include file="/common/admin/header.jsp" %>
<body>
<%@include file="/common/admin/menu.jsp" %>
<style>
    .btn-group{
        width: 100% !important;
    }
    .dropdown-menu.show{
        width: 100% ;
    }
</style>
<div id="wrapper" style="width: 100%;height: 90vh;overflow: auto;">
    <div class="main-content" style="width: 100%">
        <div class="main-content-inner">
            <div class="page-content container">
                <div class="row">
                    <div class="col-md-12">
                        <h2>User</h2>
                        <form id="formSubmit" class="mt-3">
                            <input type="hidden" name="id" id="idUser" value="${idUser}"/>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="col-sm-12">
                                        <div class="avatar">
                                            <img id="thumbnail"
                                                 src="https://yt3.ggpht.com/-f6NCDKG2Ukw/AAAAAAAAAAI/AAAAAAAAAAA/MqMm3rgmqCY/s48-c-k-no-mo-rj-c0xffffff/photo.jpg"
                                                 class="img-fluid" style="max-width: 300px; max-height: 300px;" />
                                        </div>
                                        <div class="file-field">
                                            <p>
                                                <strong id="Ithumbnail">Chọn ảnh đại diện</strong><br/>
                                                <button
                                                        class="btn btn-primary btn-sm waves-effect waves-light"
                                                        type="button" value="Browse Image"
                                                        onclick="BrowseServer( 'Images:/', 'Ithumbnail' );" >Browse Image</button>
                                            </p>
                                            <input type="hidden" name="avatar" id="image_src"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label no-padding-right">Họ tên</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" id="name" name="name" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label class="col-sm-3 control-label no-padding-right">Trạng thái</label>
                                            <div class="col-sm-12">
                                                <select class="form-control" id="status" name="status">
                                                    <option disabled>--Trạng thái --</option>
                                                    <option value="ACTIVE">On</option>
                                                    <option value="OFF">Off</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="col-sm-3 control-label no-padding-right">Quyền hạn</label>
                                            <div class="col-sm-12">
                                                <select id="roleId" name="roleId" multiple="multiple">

                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-sm-3 control-label no-padding-right">Tài khoản</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="userName" name="userName" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <c:if test="${idUser == null}">
                                        <label class="col-sm-3 control-label no-padding-right">Mật khẩu</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" id="password" name="password" />
                                        </div>
                                    </c:if>
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-sm-3 control-label no-padding-right">Email</label>
                                    <div class="col-sm-12">
                                        <input required class="form-control" id="email" name="email"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-sm-3 control-label no-padding-right">Điện thoại</label>
                                    <div class="col-sm-12">
                                        <input required class="form-control" id="phone" name="phone"/>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <div class="col-md-6">
                                    <label class="col-sm-3 control-label no-padding-right">Địa chỉ</label>
                                    <div class="col-sm-12">
                                        <input class="form-control"  type="text" id="address" name="address"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-sm-3 control-label no-padding-right">Giới tính</label>
                                    <div class="col-sm-12">
                                        <select class="form-control" id="sex" name="sex">
                                            <option selected disabled>-- Giới tính --</option>
                                            <option value="nam">Nam</option>
                                            <option VALUE="nu">Nữ</option>
                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <c:if test="${idUser == null}">
                                        <button type="button"  class="btn btn-white btn-warning btn-bold" value="Thêm mới" id="btnAdd">Thêm mới</button>
                                    </c:if>
                                    <c:if test="${idUser != null}">
                                        <button type="button"  class="btn btn-white btn-warning btn-bold" value="Cập nhật" id="btnUpdate">Cập nhật</button>
                                    </c:if>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%@include file="/common/admin/footer.jsp" %>
</div>
<%--<script src="https://code.jquery.com/jquery-3.1.1.min.js"/>--%>
<%@include file="/common/admin/scrip.jsp" %>

<%--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>--%>
<script type="text/javascript" src="<c:url value='/js/admin/bootstrap-multiselect.js'/> "></script>
<script src="<c:url value='/js/admin/user/edit.js'/> " ></script>
<script>

    $(document).ready(function(){

        $('#roleId').multiselect();

    });
    /*Avatar start*/
    function BrowseServer(startupPath, functionData) {
        // You can use the "CKFinder" class to render CKFinder in a page:
        var finder = new CKFinder();

        // The path for the installation of CKFinder (default = "/ckfinder/").
        finder.basePath = '../';

        //Startup path in a form: "Type:/path/to/directory/"
        finder.startupPath = startupPath;

        // Name of a function which is called when a file is selected in CKFinder.
        finder.selectActionFunction = SetFileField;

        // Additional data to be passed to the selectActionFunction in a second argument.
        // We'll use this feature to pass the Id of a field that will be updated.
        finder.selectActionData = functionData;

        // Name of a function which is called when a thumbnail is selected in CKFinder. Preview img
        // finder.selectThumbnailActionFunction = ShowThumbnails;

        // Launch CKFinder
        finder.popup();
    }

    function SetFileField(fileUrl, data) {
        document.getElementById(data["selectActionData"]).innerHTML = this
            .getSelectedFile().name;
        document.getElementById("thumbnail").src = fileUrl;
        $('#thumbnail').val(fileUrl);
        $('#image_src').val(fileUrl);

    }


</script>
</body>
</html>

