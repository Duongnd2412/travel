<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="/common/taglib.jsp"%>

<html>

<%@include file="/common/admin/head.jsp" %>
<%@include file="/common/admin/header.jsp" %>

<body>
<%@include file="/common/admin/menu.jsp" %>
<div class="content-wrapper" style="height: 90vh;overflow: auto">
    <div class="card">
        <div class="card-header">
            <div class="caption">
                <i class="fas fa-layer-group font-green"></i>
                <span class="caption-subject font-green sbold uppercase">Danh sách khách hàng book tour</span>

            </div>
        </div>
    </div>

    <div class="card-header">
        <div class="container-fluid">
            <form id="search">
                <div class="d-flex justify-content-end">
                    <div class="p-3 flex-grow-1">
                        <input type="text"  class="form-control" id="fullName" name="fullName" placeholder="Họ tên">
                    </div>
                    <div class="p-3 flex-grow-1">
                        <input type="text"  class="form-control" id="phone" name="phone" placeholder="Điện thoại">
                    </div>


                    <div class="p-3">
                        <button id="btn_search" type="button" title="Tìm kiếm" class="btn btn-outline-info btn-sm" style="font-size: 26px"><i class="fas fa-search"></i></button>
                    </div>

                </div>
            </form>

            <!-- /.card-header -->
            <div class="card-body " style="overflow: auto">
                <table class="table table-striped " id="dtHorizontalVerticalExample" cellspacing="0" style="min-width: 1400px">
                    <thead>
                     <tr>
                        <%--<th> <input type='checkbox' value="0" class='check-box-element'></th>--%>
                        <th >Ngày đặt</th>
                        <th >Họ tên</th>
                        <th >Email</th>
                        <th >Số điện thoại</th>
                        <th >Tour đã đặt</th>
                        <th class="text-center">Người lớn</th>
                        <th class="text-center">Trẻ em</th>
                        <th class="text-center">Số phòng</th>
                    </tr>
                    </thead>
                    <tbody id="data-list">

                    </tbody>
                </table>


            </div>
            <!-- /.card-body -->
            <div class="col-sm-12 col-xs-12">
                <ul id="pagination-demo" class="pagination"></ul>
            </div>
        </div>
    </div>


    <div class="position-absolute loader " style="">
    <div class="spinner-border " style="position:fixed;width: 3rem; height: 3rem;top: 50%;left: 50%"
    role="status">
    <span class="sr-only">Loading...</span>
    </div>
    </div>
    <%@include file="/common/admin/footer.jsp" %>
</div>


<%@include file="/common/admin/scrip.jsp" %>

<script src="<c:url value='/js/admin/customer/listCustomerTour.js'/> " ></script>
<script>

    $("#checkAllCustomAdd").change(function(){  //"select all" change
        var status = this.checked; // "select all" checked status
        $('#data-list input[type=checkbox]').each(function(){ //iterate all listed checkbox items
            this.checked = status; //change ".checkbox" checked status
        });
    });

    $('#data-list input[type=checkbox]').change(function(){ //".checkbox" change
        //uncheck "select all", if one of the listed checkbox item is unchecked
        if(this.checked == false){ //if this item is unchecked
            $("#checkAllCustomAdd")[0].checked = false; //change "select all" checked status to false
        }
        //check "select all" if all checkbox items are checked
        if ($('#data-list input[type=checkbox]:checked').length == $('#data-list input[type=checkbox]').length ){
            $("#checkAllCustomAdd")[0].checked = true; //change "select all" checked status to true
        }
    });

</script>
</body>
</html>
