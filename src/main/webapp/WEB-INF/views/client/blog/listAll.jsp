<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 14-Jan-21
  Time: 3:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Danh sách tour</title>
    <%@include file="/common/font/head.jsp" %>
</head>
<body id="main-homepage">



<!--============= TOP-BAR ===========-->
<%@include file="/common/font/header.jsp" %>
<!-- end navbar -->

<!--==== INNERPAGE-WRAPPER =====-->
<section class="innerpage-wrapper">
    <div id="blog-listings" class="innerpage-section-padding">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3 side-bar blog-sidebar left-side-bar">
                    <div class="row">

                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block main-block ad-block">
                                <div class="main-img ad-img">
                                    <a href="#">
                                        <img src="images/car-ad.jpg" class="img-responsive" alt="car-ad" />
                                        <div class="ad-mask">
                                            <div class="ad-text">
                                                <span>Luxury</span>
                                                <h2>Car</h2>
                                                <span>Offer</span>
                                            </div><!-- end ad-text -->
                                        </div><!-- end columns -->
                                    </a>
                                </div><!-- end ad-img -->
                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                        <!-- end columns -->
                    </div><!-- end row -->

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block contact">
                                <h2 class="side-bar-heading">Contact Us</h2>
                                <div class="c-list">
                                    <div class="icon"><span><i class="fa fa-envelope"></i></span></div>
                                    <div class="text"><p>support@star-hotel.com</p></div>
                                </div><!-- end c-list -->

                                <div class="c-list">
                                    <div class="icon"><span><i class="fa fa-phone"></i></span></div>
                                    <div class="text"><p>+222 – 5548 656</p></div>
                                </div><!-- end c-list -->

                                <div class="c-list">
                                    <div class="icon"><span><i class="fa fa-map-marker"></i></span></div>
                                    <div class="text"><p>Street No: 1234/A, Blu Vard Area, Main Double Road, UK</p></div>
                                </div><!-- end c-list -->
                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                        <%--<div class="col-xs-12 col-sm-6 col-md-12">--%>
                            <%--<div class="side-bar-block recent-post">--%>
                                <%--<h2 class="side-bar-heading">Recent Post</h2>--%>

                                <%--<div class="recent-block">--%>
                                    <%--<div class="recent-img">--%>
                                        <%--<a href="blog-detail-left-sidebar.html"><img src="images/recent-post-1.jpg" class="img-reponsive" alt="recent-blog-image" /></a>--%>
                                    <%--</div><!-- end recent-img -->--%>

                                    <%--<div class="recent-text">--%>
                                        <%--<a href="blog-detail-left-sidebar.html"><h5>Host a Family  Party</h5></a>--%>
                                        <%--<span class="date">27 May, 2017</span>--%>
                                    <%--</div><!-- end recent-text -->--%>
                                <%--</div><!-- end recent-block -->--%>

                                <%--<div class="recent-block">--%>
                                    <%--<div class="recent-img">--%>
                                        <%--<a href="blog-detail-left-sidebar.html"><img src="images/recent-post-2.jpg" class="img-reponsive" alt="recent-blog-image" /></a>--%>
                                    <%--</div><!-- end recent-img -->--%>

                                    <%--<div class="recent-text">--%>
                                        <%--<a href="blog-detail-left-sidebar.html"><h5>Host a Family  Party</h5></a>--%>
                                        <%--<span class="date">27 May, 2017</span>--%>
                                    <%--</div><!-- end recent-text -->--%>
                                <%--</div><!-- end recent-block -->--%>

                                <%--<div class="recent-block">--%>
                                    <%--<div class="recent-img">--%>
                                        <%--<a href="blog-detail-left-sidebar.html"><img src="images/recent-post-3.jpg" class="img-reponsive" alt="recent-blog-image" /></a>--%>
                                    <%--</div><!-- end recent-img -->--%>

                                    <%--<div class="recent-text">--%>
                                        <%--<a href="blog-detail-left-sidebar.html"><h5>Host a Family  Party</h5></a>--%>
                                        <%--<span class="date">27 May, 2017</span>--%>
                                    <%--</div><!-- end recent-text -->--%>
                                <%--</div><!-- end recent-block -->--%>

                            <%--</div><!-- end side-bar-block -->--%>
                        <%--</div><!-- end columns -->--%>
                    </div><!-- end row -->

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block follow-us">
                                <h2 class="side-bar-heading">Follow Us</h2>
                                <ul class="list-unstyled list-inline">
                                    <li><a href="#"><span><i class="fab fa-facebook"></i></span></a></li>
                                    <li><a href="#"><span><i class="fab fa-twitter"></i></span></a></li>
                                    <li><a href="#"><span><i class="fab fa-linkedin"></i></span></a></li>
                                    <li><a href="#"><span><i class="fab fa-google-plus"></i></span></a></li>
                                    <li><a href="#"><span><i class="fab fa-pinterest-p"></i></span></a></li>
                                </ul>
                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block tags">
                                <h2 class="side-bar-heading">Tags</h2>
                                <ul class="list-unstyled list-inline">
                                    <li><a href="#" class="btn btn-g-border">SPA</a></li>
                                    <li><a href="#" class="btn btn-g-border">Restaurant</a></li>
                                    <li><a href="#" class="btn btn-g-border">Searvices</a></li>
                                    <li><a href="#" class="btn btn-g-border">Wifi</a></li>
                                    <li><a href="#" class="btn btn-g-border">Tv</a></li>
                                    <li><a href="#" class="btn btn-g-border">Rooms</a></li>
                                    <li><a href="#" class="btn btn-g-border">Pools</a></li>
                                    <li><a href="#" class="btn btn-g-border">Work</a></li>
                                    <li><a href="#" class="btn btn-g-border">Sports</a></li>
                                </ul>
                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                    </div><!-- end row -->
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">
                    <div class="space-right " id="list-blog">




                    </div><!-- end space-right -->
                    <div class="col-sm-12 col-xs-12">
                        <ul id="pagination-demo" class="pagination"></ul>
                    </div>
                </div><!-- end columns -->

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end blog-listings -->
</section><!-- end innerpage-wrapper -->


<%@include file="/common/font/footer.jsp" %>
<%@include file="/common/font/scrip.jsp" %>
<script src="<c:url value='/js/fonts/blog/listAll.js'/> "></script>
</body>
</html>
