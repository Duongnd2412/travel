<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 14-Jan-21
  Time: 3:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Danh sách tour</title>
    <%@include file="/common/font/head.jsp" %>
</head>
<body id="main-homepage">



<!--============= TOP-BAR ===========-->
<%@include file="/common/font/header.jsp" %>
<!-- end navbar -->

<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="tour-details" class="innerpage-section-padding">
        <input type="hidden" value="${idHotel}" id="idHotel"/>
        <div class="container" style="width: 1400px">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">

                    <div  id="data-tour" >

                    </div><!-- end detail-tabs -->
                    <span>--</span>
                    <div class="available-blocks" style="margin-top: 100px" id="available-tours">
                        <h2>Các khách sạn khác</h2>
                        <div class="list-block main-block t-list-block" id="hotel1">

                        </div>

                        <div class="list-block main-block t-list-block" id="hotel2">

                        </div>

                        <div class="list-block main-block t-list-block" id="hotel3">

                        </div>

                        <div class="list-block main-block t-list-block" id="hotel4">

                        </div>
                        <div class="list-block main-block t-list-block" id="hotel5">

                        </div>
                        <div class="list-block main-block t-list-block" id="hotel6">

                        </div>

                    </div><!-- end available-tours -->

                    <div class="text-center">
                        <a href="/page/hotel-all" class="btn btn-black">View All Hotel</a>
                    </div>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-12 col-md-3 side-bar right-side-bar">

                    <div class="side-bar-block booking-form-block">
                        <div id="price">

                        </div>
                        <div class="booking-form">
                            <h3>Book Hotel</h3>
                            <p>Find your dream tour today</p>

                            <form id="formSubmit">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Họ tên" required/>
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required/>
                                </div>

                                <div class="form-group">
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" required/>
                                </div>

                                <div class="form-group">
                                    <p>Số phòng</p>
                                    <div class="row">
                                        <input type="number" min="0" class="col-md-6" name="numberRoom" id="numberRoom"/>
                                        <div class="col-md-6" id="prices">

                                        </div>
                                    </div>
                                </div>

                                <button type="button" class="btn btn-block btn-orange" id="btnAdd">Xác nhận</button>
                            </form>



                        </div><!-- end booking-form -->
                    </div><!-- end side-bar-block -->

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block main-block ad-block">
                                <div class="main-img ad-img">
                                    <a href="#">
                                        <img src="images/car-ad.jpg" class="img-responsive" alt="car-ad" />
                                        <div class="ad-mask">
                                            <div class="ad-text">
                                                <span>Luxury</span>
                                                <h2>Car</h2>
                                                <span>Offer</span>
                                            </div><!-- end ad-text -->
                                        </div><!-- end columns -->
                                    </a>
                                </div><!-- end ad-img -->
                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block support-block" id="infor">

                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                    </div><!-- end row -->
                </div><!-- end columns -->

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end tour-details -->
</section><!-- end innerpage-wrapper -->


<%@include file="/common/font/footer.jsp" %>
<%@include file="/common/font/scrip.jsp" %>
<script src="<c:url value='/js/fonts/hotel/hotelDetail.js'/> "></script>
</body>
</html>
