<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 14-Jan-21
  Time: 3:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Danh sách tour</title>
    <%@include file="/common/font/head.jsp" %>
</head>
<body id="main-homepage">



<!--============= TOP-BAR ===========-->
<%@include file="/common/font/header.jsp" %>
<!-- end navbar -->

<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="tour-grid" class="innerpage-section-padding">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side">
                    <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 content-side" id="noTour" style="width: 100%"></div>
                    <div class="row" >

                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="grid-block main-block t-grid-block" id="tour1">

                            </div><!-- end t-grid-block -->
                        </div><!-- end columns -->
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="grid-block main-block t-grid-block" id="tour2">

                            </div><!-- end t-grid-block -->
                        </div><!-- end columns -->
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="grid-block main-block t-grid-block" id="tour3">

                            </div><!-- end t-grid-block -->
                        </div><!-- end columns -->
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="grid-block main-block t-grid-block" id="tour4">

                            </div><!-- end t-grid-block -->
                        </div><!-- end columns -->
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="grid-block main-block t-grid-block" id="tour5">

                            </div><!-- end t-grid-block -->
                        </div><!-- end columns -->
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="grid-block main-block t-grid-block" id="tour6">

                            </div><!-- end t-grid-block -->
                        </div><!-- end columns -->



                    </div><!-- end row -->

                    <div class="col-sm-12 col-xs-12">
                        <ul id="pagination-demo" class="pagination"></ul>
                    </div>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-12 col-md-3 side-bar right-side-bar">

                    <div class="side-bar-block filter-block">
                        <h3>Tìm kiếm khách sạn</h3>
                        <p>Tìm chuyến bay mơ ước của bạn ngay hôm nay</p>

                        <form id="search">
                            <input id="name" name="name" class="form-control" placeholder="Tên khách sạn">
                            <button style="margin-top: 15px" type="button" class="btn btn-white btn-warning btn-bold" id="btn_search">Tìm</button>
                        </form>
                    </div><!-- end side-bar-block -->

                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block main-block ad-block">
                                <div class="main-img ad-img">
                                    <a href="#">
                                        <img src="#" class="img-responsive" alt="car-ad" />
                                        <div class="ad-mask">
                                            <div class="ad-text">
                                                <span>Luxury</span>
                                                <h2>Car</h2>
                                                <span>Offer</span>
                                            </div><!-- end ad-text -->
                                        </div><!-- end columns -->
                                    </a>
                                </div><!-- end ad-img -->
                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                        <div class="col-xs-12 col-sm-6 col-md-12">
                            <div class="side-bar-block support-block">
                                <h3>Need Help</h3>
                                <p>Lorem ipsum dolor sit amet, ad duo fugit aeque fabulas, in lucilius prodesset pri. Veniam delectus ei vis. Est atqui timeam mnesarchum.</p>
                                <div class="support-contact">
                                    <span><i class="fa fa-phone"></i></span>
                                    <p>+1 123 1234567</p>
                                </div><!-- end support-contact -->
                            </div><!-- end side-bar-block -->
                        </div><!-- end columns -->

                    </div><!-- end row -->
                </div><!-- end columns -->

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end tour-grid -->
</section><!-- end innerpage-wrapper -->


<%@include file="/common/font/footer.jsp" %>
<%@include file="/common/font/scrip.jsp" %>
<script src="<c:url value='/js/fonts/hotel/listAll.js'/> "></script>
</body>
</html>
