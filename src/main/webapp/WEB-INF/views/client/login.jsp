  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: SonDX
  Date: 1/13/2021
  Time: 9:57 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Đăng nhập</title>
</head>
<body>
<!doctype html>
<html lang="en">
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="<c:url value="/images/favicon.png"/> " type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i%7CMerriweather:300,300i,400,400i,700,700i,900,900i" rel="stylesheet">

    <!-- Bootstrap Stylesheet -->
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">

    <!-- Font Awesome Stylesheet -->
    <link rel="stylesheet" href="<c:url value='/plugins/fontawesome-free/css/all.min.css'/>">

    <!-- Custom Stylesheets -->
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>">
    <link rel="stylesheet" id="cpswitch" href="<c:url value="/css/orange.css"/>">
    <link rel="stylesheet" href="css/responsive.css">
</head>


<body>

<!--====== LOADER =====-->
<div class="loader"></div>


<!--======== SEARCH-OVERLAY =========-->
<div class="overlay">
    <a href="javascript:void(0)" id="close-button" class="closebtn">&times;</a>
    <div class="overlay-content">
        <div class="form-center">
            <form>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search..." required />
                        <span class="input-group-btn"><button type="submit" class="btn"><span><i class="fa fa-search"></i></span></button></span>
                    </div><!-- end input-group -->
                </div><!-- end form-group -->
            </form>
        </div><!-- end form-center -->
    </div><!-- end overlay-content -->
</div><!-- end overlay -->


<!--============= TOP-BAR ===========-->
<div id="top-bar" class="tb-text-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div id="info">
                    <ul class="list-unstyled list-inline">
                        <li><span><i class="fa fa-map-marker"></i></span>29 Land St, Lorem City, CA</li>
                        <li><span><i class="fa fa-phone"></i></span>+00 123 4567</li>
                    </ul>
                </div><!-- end info -->
            </div><!-- end columns -->

            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div id="links">
                    <ul class="list-unstyled list-inline">
                        <li><a href="login.html"><span><i class="fa fa-lock"></i></span>Login</a></li>
                        <li><a href="registration.html"><span><i class="fa fa-plus"></i></span>Sign Up</a></li>
                        <li>
                            <form>
                                <ul class="list-inline">
                                    <li>
                                        <div class="form-group currency">
                                            <span><i class="fa fa-angle-down"></i></span>
                                            <select class="form-control">
                                                <option value="">$</option>
                                                <option value="">£</option>
                                            </select>
                                        </div><!-- end form-group -->
                                    </li>

                                    <li>
                                        <div class="form-group language">
                                            <span><i class="fa fa-angle-down"></i></span>
                                            <select class="form-control">
                                                <option value="">EN</option>
                                                <option value="">UR</option>
                                                <option value="">FR</option>
                                                <option value="">IT</option>
                                            </select>
                                        </div><!-- end form-group -->
                                    </li>
                                </ul>
                            </form>
                        </li>
                    </ul>
                </div><!-- end links -->
            </div><!-- end columns -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end top-bar -->

<nav class="navbar navbar-default main-navbar navbar-custom navbar-white" id="mynavbar-1">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" id="menu-button">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="header-search hidden-lg">
                <a href="javascript:void(0)" class="search-button"><span><i class="fa fa-search"></i></span></a>
            </div>
            <a href="#" class="navbar-brand"><span><i class="fa fa-plane"></i>STAR</span>TRAVELS</a>
        </div><!-- end navbar-header -->

        <div class="collapse navbar-collapse" id="myNavbar1">
            <ul class="nav navbar-nav navbar-right navbar-search-link">
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tour<span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Trong nước</a></li>
                        <li><a href="#">Nước ngoài</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog<span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="flight-homepage.html">Flight Homepage</a></li>
                        <li><a href="flight-listing-left-sidebar.html">List View Left Sidebar</a></li>
                        <li><a href="flight-listing-right-sidebar.html">List View Right Sidebar</a></li>
                        <li><a href="flight-grid-left-sidebar.html">Grid View Left Sidebar</a></li>
                        <li><a href="flight-grid-right-sidebar.html">Grid View Right Sidebar</a></li>
                        <li><a href="flight-detail-left-sidebar.html">Detail Left Sidebar</a></li>
                        <li><a href="flight-detail-right-sidebar.html">Detail Right Sidebar</a></li>
                    </ul>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Hotels<span><i class="fa fa-angle-down"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="hotel-homepage.html">Hotel Homepage</a></li>
                        <li><a href="hotel-listing-left-sidebar.html">List View Left Sidebar</a></li>
                        <li><a href="hotel-listing-right-sidebar.html">List View Right Sidebar</a></li>
                        <li><a href="hotel-grid-left-sidebar.html">Grid View Left Sidebar</a></li>
                        <li><a href="hotel-grid-right-sidebar.html">Grid View Right Sidebar</a></li>
                        <li><a href="hotel-detail-left-sidebar.html">Detail Left Sidebar</a></li>
                        <li><a href="hotel-detail-right-sidebar.html">Detail Right Sidebar</a></li>
                    </ul>
                </li>
                <%--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Tours<span><i class="fa fa-angle-down"></i></span></a>--%>
                    <%--<ul class="dropdown-menu">--%>
                        <%--<li><a href="tour-homepage.html">Tour Homepage</a></li>--%>
                        <%--<li><a href="tour-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
                        <%--<li><a href="tour-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
                        <%--<li><a href="tour-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
                        <%--<li><a href="tour-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
                        <%--<li><a href="tour-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
                        <%--<li><a href="tour-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
                    <%--</ul>--%>
                <%--</li>--%>
                <%--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cruise<span><i class="fa fa-angle-down"></i></span></a>--%>
                    <%--<ul class="dropdown-menu">--%>
                        <%--<li><a href="cruise-homepage.html">Cruise Homepage</a></li>--%>
                        <%--<li><a href="cruise-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
                        <%--<li><a href="cruise-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
                        <%--<li><a href="cruise-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
                        <%--<li><a href="cruise-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
                        <%--<li><a href="cruise-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
                        <%--<li><a href="cruise-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
                    <%--</ul>--%>
                <%--</li>--%>
                <%--<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Cars<span><i class="fa fa-angle-down"></i></span></a>--%>
                    <%--<ul class="dropdown-menu">--%>
                        <%--<li><a href="car-homepage.html">Car Homepage</a></li>--%>
                        <%--<li><a href="car-listing-left-sidebar.html">List View Left Sidebar</a></li>--%>
                        <%--<li><a href="car-listing-right-sidebar.html">List View Right Sidebar</a></li>--%>
                        <%--<li><a href="car-grid-left-sidebar.html">Grid View Left Sidebar</a></li>--%>
                        <%--<li><a href="car-grid-right-sidebar.html">Grid View Right Sidebar</a></li>--%>
                        <%--<li><a href="car-detail-left-sidebar.html">Detail Left Sidebar</a></li>--%>
                        <%--<li><a href="car-detail-right-sidebar.html">Detail Right Sidebar</a></li>--%>
                    <%--</ul>--%>
                <%--</li>--%>
                <li><a href="javascript:void(0)" class="search-button"><span><i class="fa fa-search"></i></span></a></li>
            </ul>
        </div><!-- end navbar collapse -->
    </div><!-- end container -->
</nav><!-- end navbar -->

<div class="sidenav-content">
    <div id="mySidenav" class="sidenav" >
        <h2 id="web-name"><span><i class="fa fa-plane"></i></span>Star Travel</h2>

        <div id="main-menu">
            <div class="closebtn">
                <button class="btn btn-default" id="closebtn">&times;</button>
            </div><!-- end close-btn -->

            <div class="list-group panel">

                <a href="#home-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-home link-icon"></i></span>Home<span><i class="fa fa-chevron-down arrow"></i></span></a>
                <div class="collapse sub-menu" id="home-links">
                    <a href="index.html" class="list-group-item">Main Homepage</a>
                    <a href="flight-homepage.html" class="list-group-item">Flight Homepage</a>
                    <a href="hotel-homepage.html" class="list-group-item">Hotel Homepage</a>
                    <a href="tour-homepage.html" class="list-group-item">Tour Homepage</a>
                    <a href="cruise-homepage.html" class="list-group-item">Cruise Homepage</a>
                    <a href="car-homepage.html" class="list-group-item">Car Homepage</a>
                    <a href="landing-page.html" class="list-group-item">Landing Page</a>
                </div><!-- end sub-menu -->

                <a href="#flights-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-plane link-icon"></i></span>Flights<span><i class="fa fa-chevron-down arrow"></i></span></a>
                <div class="collapse sub-menu" id="flights-links">
                    <a href="flight-homepage.html" class="list-group-item">Flight Homepage</a>
                    <a href="flight-listing-left-sidebar.html" class="list-group-item">List View Left Sidebar</a>
                    <a href="flight-listing-right-sidebar.html" class="list-group-item">List View Right Sidebar</a>
                    <a href="flight-grid-left-sidebar.html" class="list-group-item">Grid View Left Sidebar</a>
                    <a href="flight-grid-right-sidebar.html" class="list-group-item">Grid View Right Sidebar</a>
                    <a href="flight-detail-left-sidebar.html" class="list-group-item">Detail Left Sidebar</a>
                    <a href="flight-detail-right-sidebar.html" class="list-group-item">Detail Right Sidebar</a>
                </div><!-- end sub-menu -->

                <a href="#hotels-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-building link-icon"></i></span>Hotels<span><i class="fa fa-chevron-down arrow"></i></span></a>
                <div class="collapse sub-menu" id="hotels-links">
                    <a href="hotel-homepage.html" class="list-group-item">Hotel Homepage</a>
                    <a href="hotel-listing-left-sidebar.html" class="list-group-item">List View Left Sidebar</a>
                    <a href="hotel-listing-right-sidebar.html" class="list-group-item">List View Right Sidebar</a>
                    <a href="hotel-grid-left-sidebar.html" class="list-group-item">Grid View Left Sidebar</a>
                    <a href="hotel-grid-right-sidebar.html" class="list-group-item">Grid View Right Sidebar</a>
                    <a href="hotel-detail-left-sidebar.html" class="list-group-item">Detail Left Sidebar</a>
                    <a href="hotel-detail-right-sidebar.html" class="list-group-item">Detail Right Sidebar</a>
                </div><!-- end sub-menu -->

                <a href="#tours-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-globe link-icon"></i></span>Tours<span><i class="fa fa-chevron-down arrow"></i></span></a>


                <div class="collapse sub-menu" id="tours-links">
                    <a href="tour-homepage.html" class="list-group-item">Tour Homepage</a>
                    <a href="tour-listing-left-sidebar.html" class="list-group-item">List View Left Sidebar</a>
                    <a href="tour-listing-right-sidebar.html" class="list-group-item">List View Right Sidebar</a>
                    <a href="tour-grid-left-sidebar.html" class="list-group-item">Grid View Left Sidebar</a>
                    <a href="tour-grid-right-sidebar.html" class="list-group-item">Grid View Right Sidebar</a>
                    <a href="tour-detail-left-sidebar.html" class="list-group-item">Detail Left Sidebar</a>
                    <a href="tour-detail-right-sidebar.html" class="list-group-item">Detail Right Sidebar</a>
                </div><!-- end sub-menu -->

                <a href="#cruise-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-ship link-icon"></i></span>Cruise<span><i class="fa fa-chevron-down arrow"></i></span></a>
                <div class="collapse sub-menu" id="cruise-links">
                    <a href="cruise-homepage.html" class="list-group-item">Cruise Homepage</a>
                    <a href="cruise-listing-left-sidebar.html" class="list-group-item">List View Left Sidebar</a>
                    <a href="cruise-listing-right-sidebar.html" class="list-group-item">List View Right Sidebar</a>
                    <a href="cruise-grid-left-sidebar.html" class="list-group-item">Grid View Left Sidebar</a>
                    <a href="cruise-grid-right-sidebar.html" class="list-group-item">Grid View Right Sidebar</a>
                    <a href="cruise-detail-left-sidebar.html" class="list-group-item">Detail Left Sidebar</a>
                    <a href="cruise-detail-right-sidebar.html" class="list-group-item">Detail Right Sidebar</a>
                </div><!-- end sub-menu -->

                <a href="#cars-links" class="list-group-item" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-car link-icon"></i></span>Cars<span><i class="fa fa-chevron-down arrow"></i></span></a>
                <div class="collapse sub-menu" id="cars-links">
                    <a href="car-homepage.html" class="list-group-item">Car Homepage</a>
                    <a href="car-listing-left-sidebar.html" class="list-group-item">List View Left Sidebar</a>
                    <a href="car-listing-right-sidebar.html" class="list-group-item">List View Right Sidebar</a>
                    <a href="car-grid-left-sidebar.html" class="list-group-item">Grid View Left Sidebar</a>
                    <a href="car-grid-right-sidebar.html" class="list-group-item">Grid View Right Sidebar</a>
                    <a href="car-detail-left-sidebar.html" class="list-group-item">Detail Left Sidebar</a>
                    <a href="car-detail-right-sidebar.html" class="list-group-item">Detail Right Sidebar</a>
                </div><!-- end sub-menu -->

                <a href="#pages-links" class="list-group-item active" data-toggle="collapse" data-parent="#main-menu"><span><i class="fa fa-clone link-icon"></i></span>Pages<span><i class="fa fa-chevron-down arrow"></i></span></a>
                <div class="collapse sub-menu" id="pages-links">
                    <div class="list-group-heading list-group-item">Standard <span>Pages</span></div>
                    <a href="about-us.html"  class="list-group-item">About Us</a>
                    <a href="contact-us.html"  class="list-group-item">Contact Us</a>
                    <a href="blog-listing-left-sidebar.html"  class="list-group-item">Blog Listing Left Sidebar</a>
                    <a href="blog-listing-right-sidebar.html"  class="list-group-item">Blog Listing Right Sidebar</a>
                    <a href="blog-detail-left-sidebar.html"  class="list-group-item">Blog Detail Left Sidebar</a>
                    <a href="blog-detail-right-sidebar.html"  class="list-group-item">Blog Detail Right Sidebar</a>
                    <div class="list-group-heading list-group-item">User <span>Dashboard</span></div>
                    <a href="dashboard.html"  class="list-group-item">Dashboard</a>
                    <a href="user-profile.html"  class="list-group-item">User Profile</a>
                    <a href="booking.html"  class="list-group-item">Booking</a>
                    <a href="wishlist.html"  class="list-group-item">Wishlist</a>
                    <a href="cards.html"  class="list-group-item">Cards</a>
                    <div class="list-group-heading list-group-item">Special <span>Pages</span></div>
                    <a href="#"  class="list-group-item active">Login</a>
                    <a href="registration.html"  class="list-group-item">Registration</a>
                    <a href="forgot-password.html"  class="list-group-item">Forgot Password</a>
                    <a href="error-page.html"  class="list-group-item">404 Page</a>
                    <a href="coming-soon.html"  class="list-group-item">Coming Soon</a>
                    <div class="list-group-heading list-group-item">Extra <span>Pages</span></div>
                    <a href="before-you-fly.html" class="list-group-item">Before Fly</a>
                    <a href="travel-insurance.html" class="list-group-item">Travel Insurance</a>
                    <a href="holidays.html" class="list-group-item">Holidays</a>
                    <a href="thank-you.html" class="list-group-item">Thank You</a>
                </div><!-- end sub-menu -->
            </div><!-- end list-group -->
        </div><!-- end main-menu -->
    </div><!-- end mySidenav -->
</div><!-- end sidenav-content -->


<!--============= PAGE-COVER =============-->
<section class="page-cover" id="cover-login">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-title">Login Page</h1>
                <ul class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Login Page</li>
                </ul>
            </div><!-- end columns -->
        </div><!-- end row -->
    </div><!-- end container -->
</section><!-- end page-cover -->


<!--===== INNERPAGE-WRAPPER ====-->
<section class="innerpage-wrapper">
    <div id="login" class="innerpage-section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="flex-content">
                        <div class="custom-form custom-form-fields">
                            <h3>Login</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <form>

                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Username" id="userName" name="userName" required/>
                                    <span><i class="fa fa-user"></i></span>
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Password" id="password" name="password"  required/>
                                    <span><i class="fa fa-lock"></i></span>
                                </div>

                                <div class="checkbox">
                                    <label><input type="checkbox"> Remember me</label>
                                </div>

                                <button class="btn btn-orange btn-block">Login</button>
                            </form>

                            <div class="other-links">
                                <p class="link-line">New Here ? <a href="#">Signup</a></p>
                                <a class="simple-link" href="#">Forgot Password ?</a>
                            </div><!-- end other-links -->
                        </div><!-- end custom-form -->

                        <div class="flex-content-img custom-form-img">
                            <img src="<c:url value='/templates/userfiles/_thumbs/Images/test.jpg'/> " class="img-responsive" alt="registration-img" />
                        </div><!-- end custom-form-img -->
                    </div><!-- end form-content -->

                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end login -->
</section><!-- end innerpage-wrapper -->


<!--======================= FOOTER =======================-->
<section id="footer" class="ftr-heading-o ftr-heading-mgn-1">

    <div id="footer-top" class="banner-padding ftr-top-black ftr-text-white">
        <div class="container">
            <div class="row">

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 footer-widget ftr-contact">
                    <h3 class="footer-heading">CONTACT US</h3>
                    <ul class="list-unstyled">
                        <li><span><i class="fa fa-map-marker"></i></span>29 Land St, Lorem City, CA</li>
                        <li><span><i class="fa fa-phone"></i></span>+00 123 4567</li>
                        <li><span><i class="fa fa-envelope"></i></span>info@starhotel.com</li>
                    </ul>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-2 col-lg-2 footer-widget ftr-links">
                    <h3 class="footer-heading">COMPANY</h3>
                    <ul class="list-unstyled">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Flight</a></li>
                        <li><a href="#">Hotel</a></li>
                        <li><a href="#">Tours</a></li>
                        <li><a href="#">Cruise</a></li>
                        <li><a href="#">Cars</a></li>
                    </ul>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 footer-widget ftr-links ftr-pad-left">
                    <h3 class="footer-heading">RESOURCES</h3>
                    <ul class="list-unstyled">
                        <li><a href="#">Blogs</a></li>
                        <li><a href="#">Contact Us</a></li>
                        <li><a href="#">Login</a></li>
                        <li><a href="#">Register</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 footer-widget ftr-about">
                    <h3 class="footer-heading">ABOUT US</h3>
                    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.</p>
                    <ul class="social-links list-inline list-unstyled">
                        <li><a href="#"><span><i class="fa fa-facebook"></i></span></a></li>
                        <li><a href="#"><span><i class="fa fa-twitter"></i></span></a></li>
                        <li><a href="#"><span><i class="fa fa-google-plus"></i></span></a></li>
                        <li><a href="#"><span><i class="fa fa-pinterest-p"></i></span></a></li>
                        <li><a href="#"><span><i class="fa fa-instagram"></i></span></a></li>
                        <li><a href="#"><span><i class="fa fa-linkedin"></i></span></a></li>
                        <li><a href="#"><span><i class="fa fa-youtube-play"></i></span></a></li>
                    </ul>
                </div><!-- end columns -->

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-top -->

    <div id="footer-bottom" class="ftr-bot-black">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="copyright">
                    <p>© 2017 <a href="#">StarTravel</a>. All rights reserved.</p>
                </div><!-- end columns -->

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" id="terms">
                    <ul class="list-unstyled list-inline">
                        <li><a href="#">Terms & Condition</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                    </ul>
                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end footer-bottom -->

</section><!-- end footer -->


<!-- Page Scripts Starts -->
<script src="<c:url value="/js/jquery.min.js"/>"></script>
<script src="<c:url value="/js/bootstrap.min.js"/>"></script>
<script src="<c:url value="/js/custom-navigation.js"/>"></script>
<!-- Page Scripts Ends -->
</body>
</html>

</body>
</html>
