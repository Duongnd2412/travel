<%--
  Created by IntelliJ IDEA.
  User: admin
  Date: 14-Jan-21
  Time: 3:22 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Trang chủ</title>
    <%@include file="/common/font/head.jsp" %>
</head>
<body id="main-homepage">


<!--============= TOP-BAR ===========-->
<%@include file="/common/font/header.jsp" %>
<!-- end navbar -->

<!--========================= FLEX SLIDER =====================-->
<section class="flexslider-container" id="flexslider-container-4" style="top: 0px ">

    <div class="flexslider slider tour-slider" id="slider-4" >
        <ul class="slides">
            <li class="item-1 back-size" style="background:	linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0)),url(<c:url value="/images/sapa99.jpg"/>) 50% 50%;
                    background-size:cover;
                    height:100%;">
                <div class="meta">
                    <div class="container">
                        <span class="highlight-price highlight-2">Giá Từ 1.000.000đ</span>
                        <h3>Du Lịch Sapa</h3>
                        <p>Du lịch Sapa. Thị trấn trong mây-Sapa ẩn chứa bao điều kỳ diệu của thiên nhiên với địa hình của núi đồi, màu xanh của rừng tạo nên một vùng có nhiều cảnh sắc thơ mộng. Núi non hùng vĩ cùng trải nghiệm độc đáo với cuộc sống của đồng bào dân tộc thiểu số luôn hấp dẫn khách du lịch khi đến với Sapa.</p>
                    </div><!-- end container -->
                </div><!-- end meta -->
            </li><!-- end item-1 -->

            <li class="item-2 back-size" style="background:	linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0)),url(<c:url value="/images/phu_quoc_999.jpg"/>) 50% 50%;background-size:cover;height:100%;">
                <div class=" meta">
                    <div class="container">
                        <span class="highlight-price highlight-2">Giá Từ 2.100.000đ</span>
                        <h3>Du Lịch Phú Quốc</h3>
                        <p>Du lịch Phú Quốc. Phú Quốc là điểm đến dành cho du khách yêu thích hình thức du lịch nghỉ dưỡng và khám phá sinh thái tuyệt vời. Hành trình đến với thiên nhiên và chiêm ngưỡng Mũi Ông Đội, Đá Chào - thế giới san hô và cá biển sặc sỡ, biển Bãi Sao cát trắng mịn, dáng cong, nước xanh ngọc bích.</p>
                    </div><!-- end container -->
                </div><!-- end meta -->
            </li><!-- end item-2 -->
            <li class="item-2 back-size" style="background:	linear-gradient(rgba(0,0,0,0),rgba(0,0,0,0)),url(<c:url value="/images/tokyo999.jpeg"/>) 50% 50%;background-size:cover;height:100%;">
                <div class=" meta">
                    <div class="container">
                        <span class="highlight-price highlight-2">Giá Từ 7.100.000đ</span>
                        <h3>Du Lịch Tokyo</h3>
                        <p>Du lịch Nhật Bản. Khám phá một Tokyo hiện đại nhưng vẫn cảm nhận từng cung bậc cảm xúc khi dạo bước bên ngôi cổ tự Asakusa, Hoàng cung và núi Phú Sĩ hùng vĩ. Hành trình khám phá cảnh sắc từ Tokyo đến Kyoto, kết nối Osaka với đền đài, chùa thiêng đẫm không khí thiền tịnh và các công trình xuyên biển</p>
                    </div><!-- end container -->
                </div><!-- end meta -->
            </li><!-- end item-2 -->


        </ul>
    </div><!-- end slider -->
    <div class="search-tabs" id="search-tabs-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <ul class="nav nav-tabs">
                        <%--                                <li><a href="#flights" data-toggle="tab"><span><i class="fa fa-plane"></i></span><span class="st-text">Flights</span></a></li>--%>
                        <li><a href="#hotels" data-toggle="tab"><span><i class="fa fa-building"></i></span><span class="st-text">Hotels</span></a></li>
                        <li class="active"><a href="#tours" data-toggle="tab"><span><i class="fa fa-suitcase"></i></span><span class="st-text">Tours</span></a></li>
                        <%--                                <li><a href="#cruise" data-toggle="tab"><span><i class="fa fa-ship"></i></span><span class="st-text">Cruise</span></a></li>--%>
                        <%--                                <li><a href="#cars" data-toggle="tab"><span><i class="fa fa-car"></i></span><span class="st-text">Cars</span></a></li>--%>
                    </ul>

                    <div class="tab-content">
                        <div id="tours" class="tab-pane in active">
                            <form>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-4">
                                        <div class="form-group left-icon">
                                            <input type="text" class="form-control" placeholder="Thành Phố, Quốc Gia" />
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                    </div><!-- end columns -->
                                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                        <div class="form-group right-icon">
                                            <select class="form-control">
                                                <option selected>Month</option>
                                                <option>January</option>
                                                <option>February</option>
                                                <option>March</option>
                                                <option>April</option>
                                                <option>May</option>
                                                <option>June</option>
                                                <option>July</option>
                                                <option>August</option>
                                                <option>September</option>
                                                <option>October</option>
                                                <option>November</option>
                                                <option>December</option>
                                            </select>
                                            <i class="fa fa-angle-down"></i>
                                        </div>
                                    </div><!-- end columns -->
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 search-btn">
                                        <button class="btn btn-orange">Search</button>
                                    </div><!-- end columns -->

                                </div><!-- end row -->
                            </form>
                        </div><!-- end tours -->

                        <div id="cruise" class="tab-pane">
                            <form>
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                                        <div class="row">

                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control" placeholder="From" >
                                                    <i class="fa fa-map-marker"></i>
                                                </div>
                                            </div><!-- end columns -->

                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control" placeholder="To" >
                                                    <i class="fa fa-map-marker"></i>
                                                </div>
                                            </div><!-- end columns -->

                                        </div><!-- end row -->
                                    </div><!-- end columns -->

                                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                                        <div class="row">

                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control dpd1" placeholder="Check In" >
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div><!-- end columns -->

                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control dpd2" placeholder="Check Out" >
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div><!-- end columns -->

                                        </div><!-- end row -->
                                    </div><!-- end columns -->

                                    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                        <div class="form-group right-icon">
                                            <select class="form-control">
                                                <option selected>Người Lớn</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                            </select>
                                            <i class="fa fa-angle-down"></i>
                                        </div>
                                    </div><!-- end columns -->

                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 search-btn">
                                        <button class="btn btn-orange">Search</button>
                                    </div><!-- end columns -->

                                </div><!-- end columns -->
                            </form>
                        </div><!-- end cruises -->

                        <div id="cars" class="tab-pane">
                            <form>
                                <div class="row">

                                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-6">
                                        <div class="row">

                                            <div class="col-sm-6 col-md-4">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control" placeholder="Country" />
                                                    <i class="fa fa-globe"></i>
                                                </div>
                                            </div><!-- end columns -->

                                            <div class="col-sm-6 col-md-4">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control" placeholder="City" />
                                                    <i class="fa fa-map-marker"></i>
                                                </div>
                                            </div><!-- end columns -->

                                            <div class="col-sm-12 col-md-4">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control" placeholder="Location" />
                                                    <i class="fa fa-street-view"></i>
                                                </div>
                                            </div><!-- end columns -->

                                        </div><!-- end row -->
                                    </div><!-- end columns -->

                                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4">
                                        <div class="row">

                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control dpd1" placeholder="Check In" >
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div><!-- end columns -->

                                            <div class="col-xs-6 col-sm-6 col-md-6">
                                                <div class="form-group left-icon">
                                                    <input type="text" class="form-control dpd2" placeholder="Check Out" >
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div><!-- end columns -->

                                        </div><!-- end row -->
                                    </div><!-- end columns -->

                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-2 search-btn">
                                        <button class="btn btn-orange">Search</button>
                                    </div><!-- end columns -->

                                </div><!-- end row -->
                            </form>
                        </div><!-- end cars -->

                    </div><!-- end tab-content -->

                </div><!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </div>
    <!-- end search-tabs -->

</section><!-- end flexslider-container -->




<!--=============== TOUR OFFERS ===============-->
<div style="padding-top: 50px">
    <section id="tour-offers" class="section-padding" style="background: white">
        <div class="container">
            <div class="row">
                <!-- tour -->
                <div class="col-sm-12">
                    <div class="page-heading">
                        <h2>Tour Hot</h2>
                        <hr class="heading-line" />
                    </div><!-- end page-heading -->

                    <div class="owl-carousel owl-theme owl-custom-arrow tour-hot" id="owl-tour-offers" >

                        <div class="item" id="tour1">

                        </div><!-- end item -->
                        <div class="item" id="tour2">

                        </div><!-- end item -->
                        <div class="item" id="tour3">

                        </div><!-- end item -->
                        <div class="item" id="tour4">

                        </div><!-- end item -->
                        <div class="item" id="tour5">

                        </div><!-- end item -->
                        <div class="item" id="tour6">

                        </div><!-- end item -->

                    </div><!-- end owl-tour-offers -->

                    <div class="text-center">
                        <a href="/page/tour-all" class="btn btn-black">View All Tour</a>
                    </div><!-- end view-all -->
                </div>
                <!-- end tour -->

                <!-- hotel -->
                <div class="col-sm-12 " style="margin-top: 50px">
                    <div class="page-heading">
                        <h2>Hotel Hot</h2>
                        <hr class="heading-line" />
                    </div><!-- end page-heading -->

                    <div class="owl-carousel owl-theme owl-custom-arrow tour-hot" id="owl-hotel-offers" >

                        <div class="item" id="hotel1">

                        </div><!-- end item -->
                        <div class="item" id="hotel2">

                        </div><!-- end item -->
                        <div class="item" id="hotel3">

                        </div><!-- end item -->
                        <div class="item" id="hotel4">

                        </div><!-- end item -->
                        <div class="item" id="hotel5">

                        </div><!-- end item -->
                        <div class="item" id="hotel6">

                        </div><!-- end item -->

                    </div><!-- end owl-tour-offers -->

                    <div class=" text-center">
                        <a href="/page/hotel-all" class="btn btn-black">View All Hotel</a>
                    </div><!-- end view-all -->
                </div>
                <!-- end hotel -->

                <!-- blog -->
                <div class="col-sm-12 " style="margin-top: 50px">
                    <div class="page-heading">
                        <h2 >Điểm đến lý tưởng</h2>
                        <hr class="heading-line" />
                    </div><!-- end page-heading -->

                    <div class="owl-carousel owl-theme owl-custom-arrow tour-hot" id="owl-car-offers" >

                        <div class="item" id="blog1">

                        </div><!-- end item -->
                        <div class="item" id="blog2">

                        </div><!-- end item -->
                        <div class="item" id="blog3">

                        </div><!-- end item -->
                        <div class="item" id="blog4">

                        </div><!-- end item -->
                        <div class="item" id="blog5">

                        </div><!-- end item -->
                        <div class="item" id="blog6">

                        </div><!-- end item -->

                    </div><!-- end owl-tour-offers -->

                    <div class=" text-center">
                        <a href="/page/blog-all" class="btn btn-black">View All</a>
                    </div>
                    <!-- end view-all -->
                </div>
                <!-- end blog -->
                <!-- end columns -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section>
</div>
<!-- end tour-offers -->

<%@include file="/common/font/footer.jsp" %>
<%@include file="/common/font/scrip.jsp" %>
<script src="<c:url value='/js/fonts/home.js'/> "></script>
</body>
</html>
