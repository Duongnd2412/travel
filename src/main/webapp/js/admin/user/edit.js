jQuery(function ($) {
    $(document).ready(function (){
        getRole();

        getUser();

        $(document).on('click', '#btnAdd', function () {
            var data = getDataForm();
            console.log(data);
            $.ajax({
                url: "/admin/user",
                type: "POST",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Thêm mới thành công");
                    window.location.href = "/admin/user-list";
                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })


        });
        $(document).on('click','#btnUpdate',function () {
            var data = getDataForm();
            console.log(data);
            $.ajax({
                url: "/admin/user/"+data.id,
                type: "PUT",
                headers:{"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Cập nhật  thành công");
                    window.location.href = "/admin/user-list";
                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })

        });
        function getDataForm() {
            var data = {};
            var formData = $('#formSubmit').serializeArray();
            var dataList = [];
            $.each(formData, function (i, v) {
                data["" + v.name + ""] = v.value;
                if (v.name == 'roleId') {
                    dataList.push(v.value);
                }
            });
            data['roleId'] = dataList;
            return data;
        }
        function getRole(listRole){
            $.ajax({
                url: '/admin/roles',
                type: 'GET',
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: 'application/json',
                success: function (res) {
                    var data =[];
                    var dataSelect = [];
                    $(res.data).each(function (index, v) {
                        data.push({label:v.description, value:v.id})
                    });
                    $(listRole).each(function (index, v) {
                        dataSelect.push(v);
                    });
                    $("#roleId").multiselect('dataprovider', data);
                    $('#roleId').multiselect('select', dataSelect);

                },
                error: function (e) {
                    console.log(e);
                }
            });
        }

        function getUser() {
            var idUser = $('#idUser').val();
            if (idUser) {
                $.ajax({
                    url: "/admin/user/" + idUser,
                    type: "GET",
                    headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                    contentType: "application/json",
                    beforeSend: function () {
                        $(".loader").css("display", "block");
                    },
                    success: function (response) {
                        var dt = response.data;
                        console.log(dt);
                        $('#address').val(dt.address);
                        $('#email').val(dt.email);
                        $('#name').val(dt.name);
                        $('#phone').val(dt.phone);
                        $('#sex').val(dt.sex);
                        $('#status').val(dt.status);
                        $('#userName').val(dt.userName);
                        $('#password').val(dt.password);
                        var s = '<img id="thumbnail"'
                            +'src="'+dt.avatar+'"'
                            +'  class="img-fluid" style="max-width: 300px; max-height: 300px;" />';
                        $('.avatar').html(s);
                        getRole(dt.roleId);



                    },
                    error: function (response) {
                        alert("Thất bại");
                        console.log(response);
                    }

                })
            }
        }

        // $("#formSubmit").validate({
        //     rules: {
        //         name: {
        //             required: true,
        //             maxlength:500
        //         },
        //         startPlace:{
        //             required: true
        //         },
        //         destinationPlace: {
        //             required: true
        //         },
        //         description: {
        //             required: true
        //         },
        //         content: {
        //             required: true
        //         }
        //
        //     },
        //     messages: {
        //         name: {
        //             required: "Chưa nhập tên",
        //             maxlength:"Tên không được vượt quá 500 kí tự"
        //         },
        //         startPlace:{
        //             required: "Chưa nhập điểm xuất phát"
        //         },
        //         destinationPlace: {
        //             required: "Chưa nhập điểm đến"
        //         },
        //         description: {
        //             required: "Chưa nhập mô tả"
        //         },
        //         content: {
        //             required: "Chưa nhập nội dung"
        //         }
        //     },
        //
        // });
    })
});
