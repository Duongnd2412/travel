jQuery(function ($) {
    $(document).ready(function () {
        var data =getDataSearch();
        getListCustomer(data);

        function  getListCustomer(data){
            $.ajax({
                url: '/admin/customers',
                type:'POST',
                headers:{"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $('.loader').css("display", "block");
                    $('#pagination-test').empty();
                    $('#pagination-test').removeData("twbs-pagination");
                    $('#pagination-test').unbind("page");
                },
                success: function (res){
                    $('.loader').css("display", "none");
                    var dt = res.data;
                    if (dt.totalPage != 0) {
                        paging(dt.totalPage,res.currentPage +1);
                    }
                    console.log(res);
                    showCustomer(dt.data)
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }

        function paging(totalPage,currentPage) {
            $('#pagination-demo').twbsPagination({
                totalPages: totalPage,
                startPage: currentPage,
                visiblePages: 10,
                last:'Cuối cùng',
                next:'Tiếp theo',
                first:'Đầu tiên',
                prev:'Phía trước',
                onPageClick: function (event, page) {
                    if (page) {
                        if (currentPage != page) {
                            var data = getDataSearch();
                            data["pageIndex"] = page;
                            getListCustomer(data);
                        }
                    }

                }
            });
        }
        function fomatDate(date) {
            var day = '';
            var month = "";
            var year = '';
            $(date).each(function (index, v) {
                if (index == 0) {
                    year = v;
                } else if (index == 1) {
                    month = v;
                } else {
                    day = v;
                }
            });
            return day + '/' + month + '/' + year;
        }

        function showCustomer(data) {
            var s = '';
            if (data.length === 0) {
                var s1 = `<div class="alert alert-warning text-center w-100 mt-3" style="color: #f6821f;  background-color: #fff3cd; border-color: #ffeeba; margin-top: 200px;">
             <i class="fa fa-exclamation-triangle"></i>  
             <a style="color: #f6821f;">Không tìm thấy blog nào !</a>`;
                s += `   <tr style="background-color: white">
                <td colspan="100">` + s1 + `</td>
                </tr> `;
                $("#data-list").html(s1);
            }
            $(data).each(function (index, v) {
                s+= '<tr>'
                    +'<td>'+fomatDate(v.createdDate)+'</td>'
                    +'<td>'+v.fullName+'</td>'
                    +'<td>'+v.email+'</td>'
                    +'<td>'+v.phone+'</td>'
                    +'<td>'+v.nameTour+'</td>'
                    +'<td class="text-center">'+v.numberAdult+'</td>'
                    +'<td class="text-center">'+v.numberChildren+'</td>'
                    +'<td class="text-center">'+v.numberRoom+'</td>'
                +'</tr>';

            });
            $('#data-list').html(s);

        }

        $('#btn_search').click(function () {
            var data =getDataSearch();
            getListCustomer(data);
        });

        function getDataSearch(){
            var data = {};
            var formSearch = $('#search').serializeArray();

            $.each(formSearch,function (i,v) {
                data[""+v.name+""] = v.value;
            });
            data["isTourCustomer"] =1;

            return data;
        }
    })
})