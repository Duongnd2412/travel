jQuery(function ($) {
    $(document).ready(function () {
        var data = getDataSearch();
        getListBlog(data);

        function getListBlog(data) {
            $.ajax({
                url: '/admin/blogs?userName='+ data.userName+'&status='+data.status,
                type: 'GET',
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: "application/json",
                beforeSend: function () {
                    $('.loader').css("display", "block");
                    $('#pagination-test').empty();
                    $('#pagination-test').removeData("twbs-pagination");
                    $('#pagination-test').unbind("page");
                },
                success: function (res) {
                    $('.loader').css("display", "none");
                    var dt = res.data.data;
                    if (dt.totalItem != 0) {
                        paging(dt.totalPage,res.currentPage);
                    }
                    showBlog(dt)
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }
        $('#btn_search').click(function () {
            var data = getDataSearch();
            getListBlog(data);
        });
        function paging(totalPage,currentPage) {
            $('#pagination-demo').twbsPagination({
                totalPages: totalPage,
                startPage: currentPage,
                visiblePages: 10,
                last:'Cuối cùng',
                next:'Tiếp theo',
                first:'Đầu tiên',
                prev:'Phía trước',
                onPageClick: function (event, page) {
                    if (page) {
                        if (currentPage != page) {
                            var data = getDataSearch();
                            data["pageIndex"] = page;
                            console.log(data);
                            getListBlog(data);
                        }
                    }

                }
            });
        }
        $('#btnDelete').click(function () {
            var data = {};
            var ids = $('tbody input[type=checkbox]:checked').map(function () {
                return $(this).val();
            }).get();
            if (ids.length == 0 ) {
                alert("Chưa có blog nào được chọn");
                throw "Chưa có danh mục";
            }
            else {
                if (confirm("Xác nhận xóa")) {
                    data["idBlog"] = ids;
                    deleteBlog(ids);
                }
            }
        });
        function deleteBlog(data) {
            $.ajax({
                url: '/admin/blog/delete',
                type: 'DELETE',
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: 'application/json',
                data: JSON.stringify(data),
                beforeSend: function () {
                    $('.loader').css("display", "block");
                },
                success: function (res) {
                    $('.loader').css("display", "none");

                    if (res.data == true){
                        alert("Đã xóa blog");
                        window.location.href = "/admin/blog-list";
                    } else {
                        alert("Chỉ có thể xóa blog trong trạng thái ẩn");
                    }

                },
                error: function () {
                    console.log('false');
                    // window.location.href = "/admin/course-plan/list";
                }
            });
        }

        function showBlog(data) {
            var s = '';
            if (data.length === 0) {
                var s1 = `<div class="alert alert-warning text-center w-100 mt-3" style="color: #f6821f;  background-color: #fff3cd; border-color: #ffeeba; margin-top: 200px;">
             <i class="fa fa-exclamation-triangle"></i>  
             <a style="color: #f6821f;">Không tìm thấy blog nào !</a>`;
                s += `   <tr style="background-color: white">
                <td colspan="100">` + s1 + `</td>
                </tr> `;
                $("#data-list").html(s1);
            }
            $(data).each(function (index, v) {
                s += '<tr>'
                    + '<td><input type="checkbox" name="idCoursePlan" id="checkbox_' + v.id + '" value="' + v.id + '"></td>'
                    + '<td>' + v.title + '</td>'
                    + '<td>' + v.keyword + '</td>';
                if (v.status == 'ACTIVE') {
                    s += '<td>Công khai</td>';
                } else {
                    s += '<td>Ẩn</td>';
                }

                s += '<td>' + v.description + '</td>'
                    + '<td>' + v.createdBy + '</td>'
                    + '<td><button><a href="/admin/blog-edit/' + v.id + '">Chi tiết</a></button></td>'
                    + '</tr>';

            });
            $('#data-list').html(s);

        }

        function getDataSearch() {
            var data = {};
            var formSearch = $('#search').serializeArray();
            $.each(formSearch, function (i, v) {
                data["" + v.name + ""] = v.value;
            });

            return data;
        }
    })
})