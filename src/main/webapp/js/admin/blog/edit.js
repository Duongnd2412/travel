jQuery(function ($) {
    $(document).ready(function () {
        getBlog();
        $(document).on('click', '#btnAdd', function () {
            var data = getDataForm();
            $.ajax({
                url: "/admin/blog",
                type: "POST",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Thêm mới thành công");
                    window.location.href = "/admin/blog-list";
                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })


        });
        $(document).on('click', '#btnUpdate', function () {
            var data = getDataForm();
            console.log(data);
            $.ajax({
                url: "/admin/blog-update/" + data.idBlog,
                type: "PUT",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Thêm mới thành công");
                    window.location.href = "/admin/blog-list";
                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })

        });

        function getBlog() {
            var idBLog = $('#idBlog').val();
            if (idBLog) {
                $.ajax({
                    url: "/admin/blog/" + idBLog,
                    type: "GET",
                    headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                    contentType: "application/json",
                    beforeSend: function () {
                        $(".loader").css("display", "block");
                    },
                    success: function (response) {
                        var dt = response.data;
                        $('#title').val(dt.title);
                        $('#keyword').val(dt.keyword);
                        $('#status').val(dt.status);
                        $('#description').val(dt.description);
                        $('#content').val(dt.content);
                        var s = '<img id="thumbnail"'
                            +'src="'+dt.thumbnail+'"'
                            +'  class="img-fluid" style="max-width: 300px; max-height: 300px;" />';
                        $('.avatar').html(s);

                    },
                    error: function (response) {
                        alert("Thất bại");
                        console.log(response);
                    }

                })
            }
        }

        function getDataForm() {
            var data = {};
            var formData = $('#formSubmit').serializeArray();
            $.each(formData, function (i, v) {
                data["" + v.name + ""] = v.value;
            });
            data["content"] = editor.getData();
            data["thumbnail"] = $('#thumbnail').attr("src");
            return data;
        }
    })
})