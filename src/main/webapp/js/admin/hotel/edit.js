jQuery(function ($) {
    $(document).ready(function () {
        getHotel();
        $(document).on('click', '#btnAdd', function () {
            var data = getDataForm();
            $.ajax({
                url: "/admin/hotel",
                type: "POST",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Thêm mới thành công");
                    window.location.href = "/admin/hotel-list";
                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })


        });
        $(document).on('click', '#btnUpdate', function () {
            var data = getDataForm();
            console.log(data);
            $.ajax({
                url: "/admin/hotel-update/"+data.idHotel,
                type: "PUT",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Cập nhật thành công");
                    window.location.href = "/admin/hotel-list";
                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })


        });

        function getHotel() {
            var idHotel = $('#idHotel').val();
            if (idHotel) {
                $.ajax({
                    url: "/admin/hotel/" + idHotel,
                    type: "GET",
                    headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                    contentType: "application/json",
                    beforeSend: function () {
                        $(".loader").css("display", "block");
                    },
                    success: function (response) {
                        var dt = response.data;
                        console.log(dt);
                        if (dt.isHot == true) {
                            $('#isHot').val(1);
                        }else {
                            $('#isHot').val(0);
                        }
                        $('#localeType').val(dt.localeType);
                        $('#name').val(dt.name);
                        $('#description').val(dt.description);
                        $('#publicPhone').val(dt.publicPhone);
                        $('#privatePhone').val(dt.privatePhone);
                        $('#address').val(dt.address);
                        $('#price').val(dt.price);
                        var s = '<img id="thumbnail"'
                            +'src="'+dt.thumbnail+'"'
                            +'  class="img-fluid" style="max-width: 300px; max-height: 300px;" />';
                        $('.avatar').html(s);

                    },
                    error: function (response) {
                        alert("Thất bại");
                        console.log(response);
                    }

                })
            }
        }

        function getDataForm() {
            var data = {};
            var formData = $('#formSubmit').serializeArray();
            $.each(formData, function (i, v) {
                data["" + v.name + ""] = v.value;
            });
            data["description"] = editor.getData();
            data["thumbnail"] = $('#thumbnail').attr("src");
            return data;
        }
    })
})