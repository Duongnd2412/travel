jQuery(function ($) {
    $(document).ready(function () {
        var data = getDataSearch();
        getListHotel(data);

        function getListHotel(data) {
            $.ajax({
                url: '/admin/hotels',
                type: 'POST',
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $('.loader').css("display", "block");
                    // $('#pagination-demo').empty();
                    // $('#pagination-demo').removeData("twbs-pagination");
                    // $('#pagination-demo').unbind("page");
                },
                success: function (res) {
                    $('.loader').css("display", "none");
                    console.log(res)
                    var dt = res.data;
                    if (dt.totalItem != 0) {
                        paging(dt.totalPage,dt.currentPage+1);
                    }
                    showHotel(dt.data);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }
        function paging(totalPage,currentPage) {
            $('#pagination-demo').twbsPagination({
                totalPages: totalPage,
                startPage: currentPage,
                visiblePages: 10,
                last:'Cuối cùng',
                next:'Tiếp theo',
                first:'Đầu tiên',
                prev:'Phía trước',
                onPageClick: function (event, page) {
                    if (page) {
                        if (currentPage != page) {
                            var data = getDataSearch();
                            currentPage = page;
                            data["pageIndex"] = page;
                            console.log(data);
                            getListHotel(data);
                        }
                    }

                }
            });
        }

        function showHotel(data) {
                
            var s = '';
            if (data.length === 0) {
                var s1 = `<div class="alert alert-warning text-center w-100 mt-3" style="color: #f6821f;  background-color: #fff3cd; border-color: #ffeeba; margin-top: 200px;">
             <i class="fa fa-exclamation-triangle"></i>  
             <a style="color: #f6821f;">Không tìm thấy tour nào !</a>`;
                s += `   <tr style="background-color: white">
                <td colspan="100">` + s1 + `</td>
                </tr> `;
                $("#data-list").html(s1);
            }
            $(data).each(function (index, v) {
                s += '<tr>'
                    + '<td><input type="checkbox" name="idCoursePlan" id="checkbox_' + v.id + '" value="' + v.id + '"></td>'
                if(v.isHot == "1"){
                    s += '<td style="color: blue"><i class="fa fa-circle"></i></td>';
                }else {
                    s += '<td></td>';
                }
                    s+= '<td>' + v.name + '</td>'
                    + '<td>' + v.privatePhone + '</td>'
                    + '<td>' + v.publicPhone + '</td>';
                if (v.localeType == 1) {
                    s+='<td>Trong nước</td>';
                }else {
                    s+='<td>Nước ngoài</td>';
                }
                  s+=  '<td><button><a href="/admin/hotel-edit/' + v.id + '">Chi tiết</a></button></td>'
                    + '</tr>';

            });
            $('#data-list').html(s);

        }

        $('#btn_search').click(function () {
            var data = getDataSearch();
            getListHotel(data);
        });
        $('#btnDelete').click(function () {
            var data = {};
            var ids = $('tbody input[type=checkbox]:checked').map(function () {
                return $(this).val();
            }).get();
            if (ids.length == 0 ) {
                alert("Chưa có khách sạn nào được chọn");
                throw "Chưa có danh mục";
            }
            else {
                if (confirm("Xác nhận xóa")) {
                    data["idHotel"] = ids;
                    deleteHotel(ids);
                }
            }
        });
        function deleteHotel(data) {
            $.ajax({
                url: '/admin/hotel/delete',
                type: 'DELETE',
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: 'application/json',
                data: JSON.stringify(data),
                beforeSend: function () {
                    $('.loader').css("display", "block");
                },
                success: function (res) {
                    $('.loader').css("display", "none");
                    alert("Đã xóa hotel");
                    window.location.href = "/admin/hotel-list";

                },
                error: function () {
                    console.log('false');

                }
            });
        }

        function getDataSearch() {
            var data = {};
            var formData = $('#search').serializeArray();


            $.each(formData, function (i, v) {
                data["" + v.name + ""] = v.value;

            });
            console.log(data);
            return data;
        }
    })
})