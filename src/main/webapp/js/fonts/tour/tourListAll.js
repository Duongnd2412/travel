var id = 0;
id += $('#idCategory').val();
jQuery(function ($) {
    $(document).ready(function () {
        if (id != 0) {
            var data = {};
            getListTourSearch(data);
        }else {
            var data = getDataSearch();
            getListTour(data);
        }
        getCategoyTour();
        getTourCategory();
        $('#btn_search').click(function () {
            var data = getDataSearch();
            getListTour(data);
        });
        function getTourCategory() {
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                // headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: "application/json",

                success: function (response) {
                    var tn = '';
                    var nn = '';
                    $(response).each(function (index, v) {
                        if (v.categoryType == 1) {
                            tn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>';
                        }else {
                            nn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>'
                        }

                    });
                    $('#trong-nuoc').html(tn);
                    $('#nuoc-ngoai').html(nn);


                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }
        function paging(totalPage,currentPage) {
            console.log(totalPage);
            console.log(currentPage);
            $('#pagination-demo').twbsPagination({
                totalPages: totalPage,
                startPage: currentPage,
                visiblePages: 10,
                last:'Cuối cùng',
                next:'Tiếp theo',
                first:'Đầu tiên',
                prev:'Phía trước',
                onPageClick: function (event, page) {
                    if (page) {
                        if (currentPage != page) {
                            var data ={};
                            if (id != 0) {
                                data["pageIndex"] = page;
                                getListTourSearch(data);
                            }else {
                                data = getDataSearch();
                                currentPage = page;
                                data["pageIndex"] = page;
                                getListTour(data);
                            }
                        }
                    }

                }
            });
        }
        function getListTourSearch(data) {
            data['tourCategoryId']=id;
            $.ajax({
                url: '/customer/tours',
                type: 'POST',
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: "application/json",
                success: function (res) {
                    var dt = res.data;
                    if (res.totalItem != 0) {
                        paging(res.totalPage ,res.currentPage +1);
                    }
                    showTour(dt);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }
        function getListTour(data) {
            $.ajax({
                url: '/customer/tours',
                type: 'POST',
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: "application/json",
                success: function (res) {
                    console.log(res);
                    var dt = res.data;
                    if (res.totalItem != 0) {
                        paging(res.totalPage,res.currentPage +1);
                    }
                    showTour(dt);

                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }
        $('#tourType').click(function () {
            getCategoyTour();
        });

        function getCategoyTour(){
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    var s = '';
                    s+='<option disabled selected>-- Thể loại --</option>';
                    $(response).each(function (index, v) {
                        if ($('#tourType').val() == 1) {
                            if (v.categoryType == 1) {
                                s += '<option value="' + v.id + '">' + v.name + '</option>'
                            }
                        } else if ($('#tourType').val() == 2) {
                            if (v.categoryType == 2) {
                                s += '<option value="' + v.id + '">' + v.name + '</option>'
                            }
                        }
                        else {

                            s += '<option value="' + v.id + '">' + v.name + '</option>';
                        }

                    });
                    $('#tourCategoryId').html(s);

                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }



        function showTour(data) {
            console.log(data);
            var s ='';
            var remote='';
            $('#tour1').html(remote);
            $('#tour2').html(remote);
            $('#tour3').html(remote);
            $('#tour4').html(remote);
            $('#tour5').html(remote);
            $('#tour6').html(remote);
            $("#noTour").html(remote);
            if (data.length === 0) {
                var s1 = `<div class="alert alert-warning text-center w-100 mt-3" style="color: #f6821f;  background-color: #fff3cd; border-color: #ffeeba; margin-top: 200px;">
             <i class="fa fa-exclamation-triangle"></i>  
             <a style="color: #f6821f;">Không tìm thấy tour nào !</a>`;
                s += `   <tr style="background-color: white">
                <td colspan="100">` + s1 + `</td>
                </tr> `;
                $("#noTour").html(s1);
            }
            $(data).each(function (index, v) {

                var price = v.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                var description = v.description.substring(0,400);
           s+= '<div >'
                +'    <div class="grid-block main-block t-grid-block">'
               +'   <div class="main-img t-grid-img">'
               +'     <a href="/page/tour-detail/'+v.id+'">'
               +'     <img src="'+v.thumbnail+'" class="img-responsive" alt="hotel-img"  style="height: 190px"/>'
               +'     </a>'
               +'     <div class="main-mask">'
               +'     <ul class="list-unstyled list-inline offer-price-1">'
               +'     <li class="price" style="font-size: 15px">Chi phí: '+price+'vnđ<span class="divider"></span><span class="pkg"></span></li>'
               +' </ul>'
               +' </div>'
               +' </div>'

               +'  <div class="block-info t-grid-info" style="height: 200px">'
               +'     <div class="rating">'

               +' </div>'

               +' <h3 class="block-title"><a href="/page/tour-detail/'+v.id+'">'+v.name+'</a></h3>'
               +'<p class="block-minor">'+v.title+'</p>'
               +'<a href="/page/tour-detail/'+v.id+'">'
               +'      <p style="font-size: 15px">Thời gian: '+v.time+'</p>'
               +'      <p style="font-size: 15px">Phương tiện: '+v.vehicle+'</p>'
               +'</a>'
               // +' <div class="grid-btn">'
               // +'     <a href="/page/tour-detail/'+v.id+'" class="btn btn-orange btn-block btn-lg">View More</a>'
               // +' </div>'
               +' </div>'
               +' </div>'
               +' </div>'
                if (index == 0) {
                    $('#tour1').html(s);
                    s= '';
                }
                if (index == 1) {
                    $('#tour2').html(s);
                    s= '';
                }
                if (index == 2) {
                    $('#tour3').html(s);
                    s= '';
                }
                if (index == 3) {
                    $('#tour4').html(s);
                    s= '';
                }
                if (index == 4) {
                    $('#tour5').html(s);
                    s= '';
                }
                if (index == 5) {
                    $('#tour6').html(s);
                    s= '';
                }
            });

        }
        function getDataSearch(){
            var data = {};
            var formSearch = $('#search').serializeArray();

            $.each(formSearch,function (i,v) {
                data[""+v.name+""] = v.value;
            });

            return data;
        }
    })
})