jQuery(function ($) {
    $(document).ready(function () {
        var check = 1;
        getTourCategory();
        getTour(check);
        getListTour();

        $(document).on('click', '#noiDung', function () {
            $('#noiDung').css('color','#faa61a');
            $('#chinhSach').css('color','black');
            getTour(1);
        });
        $(document).on('click', '#chinhSach', function () {
            $('#chinhSach').css('color','#faa61a');
            $('#noiDung').css('color','black');
            getTour(2);
        });

        function getTourCategory() {
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                // headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: "application/json",

                success: function (response) {
                    console.log(response);
                    var tn = '';
                    var nn = '';
                    $(response).each(function (index, v) {
                        if (v.categoryType == 1) {
                            tn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>';
                        }else {
                            nn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>'
                        }

                    });
                    $('#trong-nuoc').html(tn);
                    $('#nuoc-ngoai').html(nn);


                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }
        function getListTour() {
            var data ={};
            var idTour = $('#idTour').val();
            data["id"]=idTour;
            if (idTour) {
                $.ajax({
                    url: "/customer/tours",
                    type: "POST",
                    data: JSON.stringify(data),
                    dataType: 'json',
                    contentType: "application/json",
                    beforeSend: function () {
                        $(".loader").css("display", "block");
                    },
                    success: function (response) {
                        var dt = response.data;

                        showListTour(dt);

                    },
                    error: function (response) {
                        alert("Thất bại");
                        console.log(response);
                    }

                })
            }
        }
        function showListTour(data) {
            console.log(data);
            var s ='';
            var remote='';
            $('#tour1').html(remote);
            $('#tour2').html(remote);
            $('#tour3').html(remote);
            $('#tour4').html(remote);
            $('#tour5').html(remote);
            $('#tour6').html(remote);


            $(data).each(function (index, v) {
                var price = v.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                s+=  '<div class="list-content">'
                    +'    <div class="main-img list-img t-list-img" style="height: 262px">'
                    +'    <a href="/page/tour-detail/'+v.id+'">'
                    +'    <img src="'+v.thumbnail+'" class="img-responsive" alt="tour-img" />'
                    +'     </a>'
                    +'     <div class="main-mask">'
                    +'     <ul class="list-unstyled list-inline offer-price-1">'
                    +'     <li class="price">'+price+'₫ /người</span></li>'
                    +' </ul>'
                    +' </div>'
                    +' </div>'

                    +'  <div class="list-info t-list-info">'
                    +'     <h3 class="block-title"><a href="/page/tour-detail/'+v.id+'">'+v.title+'</a></h3>'
                    +' <p class="block-minor">Địa điểm: '+v.destinationPlace+'</p>'
                    +' <p class="block-minor">Thời gian: '+v.time+'</p>'
                    +' <p class="block-minor">Phương tiện: '+v.vehicle+'</p>'
                    +' <p>'+v.description.substring(0,100)+'...</p>'
                    +' <a href="/page/tour-detail/'+v.id+'" class="btn btn-orange btn-lg">View More</a>'
                    +' </div>'
                    +' </div>'


                if (index == 0) {
                    $('#tour1').html(s);
                    s= '';
                }
                if (index == 1) {
                    $('#tour2').html(s);
                    s= '';
                }
                if (index == 2) {
                    $('#tour3').html(s);
                    s= '';
                }
                if (index == 3) {
                    $('#tour4').html(s);
                    s= '';
                }
                if (index == 4) {
                    $('#tour5').html(s);
                    s= '';
                }
                if (index == 5) {
                    $('#tour6').html(s);
                    s= '';
                }
            });
        }

        $(document).on('click', '#btnAdd', function () {
            var data = getDataForm();
            var id =$('#idTour').val();
            $.ajax({
                url: "/customer/order",
                type: "POST",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Book tour thành công");
                    window.location.href = "/page/tour-detail/"+id;
                },
                error: function (response) {
                    alert("Thất bại");
                    // window.location.href = "/admin/course-plan/list";
                    console.log(response);
                }

            })


        });




        function getDataForm() {
            var data = {};
            var formData = $('#formSubmit').serializeArray();
            var idTour = $('#idTour').val();
            $.each(formData, function (i, v) {
                data["" + v.name + ""] = v.value;
            });
            data["tourId"] = idTour;
            data["isTourCustomer"] =1;
            console.log(data);
            return data;
        }
        function showTour1(dt) {
            var price = dt.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            var priceChildren ='0';
            if (dt.childPrice){
                priceChildren = dt.childPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
            var s ='';
            s+='<div>'
                +'<h2>'+dt.title+'</h2>'
                +'<div class="col-sm-12 col-md-12" >'
                +'    <img src="'+dt.thumbnail+'" class="img-responsive" alt="tour-img" />'
                +' </div>'
                +'<div class="col-sm-12 col-md-12" >'
                +dt.content
                +' </div>'
                +' </div>';
            $('#data-tour').html(s);
            $('#prices').html('<span>'+price+'₫ / người</span>');
            $('#priceChildren').html('<span>'+priceChildren+'₫ / người</span>');
            var infor = '';
            infor +='<h2>Thông tin</h2>'
                +'<span style="font-size: 18px">Xuất phát: '+dt.startPlace+'</span><br>'
                +'<span style="font-size: 18px">Điểm đến: '+dt.destinationPlace+'</span><br>'
                +'<span style="font-size: 18px">Phương tiện: '+dt.vehicle+'</span>';
            $('#infor').html(infor);
        }
        function showTour2(dt) {
            var price = dt.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            var priceChildren ='0';
            if (dt.childPrice){
                priceChildren = dt.childPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            }
            var s ='';
            s+='<div>'
                +'<div class="col-sm-12 col-md-12" >'
                +'<textarea type="text" style="height: 525px" readonly class="form-control" id="policy" name="policy" >'
                +dt.policy
                +'</textarea>'
                +' </div>'
                +' </div>';

            $('#data-tour').html(s);
            $('#prices').html('<span>'+price+'₫ / người</span>');
            $('#priceChildren').html('<span>'+priceChildren+'₫ / người</span>');
            var infor = '';
            infor +='<h2>Thông tin</h2>'
                +'<span style="font-size: 18px">Xuất phát: '+dt.startPlace+'</span><br>'
                +'<span style="font-size: 18px">Điểm đến: '+dt.destinationPlace+'</span><br>'
                +'<span style="font-size: 18px">Phương tiện: '+dt.vehicle+'</span>';
            $('#infor').html(infor);
        }

        function getTour(check) {
            var idTour = $('#idTour').val();
            if (idTour) {
                $.ajax({
                    url: "/customer/tour/" +idTour,
                    type: "GET",
                    contentType: "application/json",
                    beforeSend: function () {
                        $(".loader").css("display", "block");
                    },
                    success: function (response) {
                        var dt = response.data;
                        console.log(response)
                        if (check == 1) {
                            $('#noiDung').css('color','#faa61a');2
                            showTour1(dt);
                        }else {
                            showTour2(dt);
                        }


                    },
                    error: function (response) {
                        alert("Thất bại");
                        console.log(response);
                    }

                })
            }
        }
    })
})