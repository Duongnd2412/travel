jQuery(function ($) {
    $(document).ready(function () {
        getTourCategory();
        getHotel();
        getListHotel();

        function getTourCategory() {
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                contentType: "application/json",

                success: function (response) {
                    var tn = '';
                    var nn = '';
                    $(response).each(function (index, v) {
                        if (v.categoryType == 1) {
                            tn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>';
                        }else {
                            nn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>'
                        }

                    });
                    $('#trong-nuoc').html(tn);
                    $('#nuoc-ngoai').html(nn);


                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }
        $(document).on('click', '#btnAdd', function () {
            var data = getDataForm();
            var id =$('#idHotel').val();
            $.ajax({
                url: "/customer/order",
                type: "POST",
                headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                data: JSON.stringify(data),
                dataType: "JSON",
                contentType: "application/json",
                beforeSend: function () {
                    $(".loader").css("display", "block");
                },
                success: function (response) {
                    $(".loader").css("display", "none");
                    alert("Book hotel thành công");
                    window.location.href = "/page/hotel-detail/"+id;
                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })


        });
        function getDataForm() {
            var data = {};
            var formData = $('#formSubmit').serializeArray();
            var idHotel = $('#idHotel').val();
            $.each(formData, function (i, v) {
                data["" + v.name + ""] = v.value;
            });
            data["hotelId"] = idHotel;
            data["isTourCustomer"] =0;
            console.log(data);
            return data;
        }

        function getListHotel() {
            var idHotel = $('#idHotel').val();
            if (idHotel) {
                $.ajax({
                    url: "/hotels/?id=" +idHotel,
                    type: "GET",
                    contentType: "application/json",
                    beforeSend: function () {
                        $(".loader").css("display", "block");
                    },
                    success: function (response) {
                        var dt = response.data;
                        showListHotel(dt.data);

                    },
                    error: function (response) {
                        alert("Thất bại");
                        console.log(response);
                    }

                })
            }
        }

        function getHotel() {
            var idHotel = $('#idHotel').val();
            if (idHotel) {
                $.ajax({
                    url: "/hotel/" +idHotel,
                    type: "GET",
                    contentType: "application/json",
                    beforeSend: function () {
                        $(".loader").css("display", "block");
                    },
                    success: function (response) {
                        var dt = response.data;
                        var s ='';
                        var price = dt.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                        $('#prices').html('<span>'+price+'₫ /phòng/24h</span>');
                        s+='<div>'
                            +'<h2>'+dt.name+'</h2>'
                            +'<div class="col-sm-12 col-md-12">'
                            +dt.description
                            +' </div>'
                            +' </div>';
                        $('#data-tour').html(s);
                        var infor = '';
                        infor +='<h2>Thông tin</h2>'
                            +'<span style="font-size: 18px">Địa điểm: '+dt.address+'</span><br>'
                            +'<span style="font-size: 18px">Số máy bàn: '+dt.publicPhone+'</span><br>'
                            +'<span style="font-size: 18px">Số di động: '+dt.privatePhone+'</span>';
                        $('#infor').html(infor);
                    },
                    error: function (response) {
                        alert("Thất bại");
                        console.log(response);
                    }

                })
            }
        }
        function showListHotel(data) {
            console.log(data)
            var s ='';
            var remote='';
            $('#hotel1').html(remote);
            $('#hotel2').html(remote);
            $('#hotel3').html(remote);
            $('#hotel4').html(remote);
            $('#hotel5').html(remote);
            $('#hotel6').html(remote);


            $(data).each(function (index, v) {
                var price = v.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');

          s+=  '<div class="list-content">'
                +'    <div class="main-img list-img t-list-img" style="height: 199px">'
              +'    <a href="/page/hotel-detail/'+v.id+'">'
              +'    <img src="'+v.thumbnail+'" class="img-responsive" alt="tour-img" />'
              +'     </a>'
              +'     <div class="main-mask">'
              +'     <ul class="list-unstyled list-inline offer-price-1">'
              +'     <li class="price">'+price+'₫ /phòng</span></li>'
              +' </ul>'
              +' </div>'
              +' </div>'

              +'  <div class="list-info t-list-info">'
              +'     <h3 class="block-title"><a href="/page/hotel-detail/'+v.id+'">'+v.name+'</a></h3>'
              +' <p class="block-minor">Địa điểm: '+v.address+'</p>'
              +' <p>Điện thoại: '+v.publicPhone+'</p>'
              +' <a href="/page/hotel-detail/'+v.id+'" class="btn btn-orange btn-lg">View More</a>'
              +' </div>'
              +' </div>'


                if (index == 0) {
                    $('#hotel1').html(s);
                    s= '';
                }
                if (index == 1) {
                    $('#hotel2').html(s);
                    s= '';
                }
                if (index == 2) {
                    $('#hotel3').html(s);
                    s= '';
                }
                if (index == 3) {
                    $('#hotel4').html(s);
                    s= '';
                }
                if (index == 4) {
                    $('#hotel5').html(s);
                    s= '';
                }
                if (index == 5) {
                    $('#hotel6').html(s);
                    s= '';
                }
            });
        }
    })
})