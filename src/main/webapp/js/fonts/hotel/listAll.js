jQuery(function ($) {
    $(document).ready(function () {
        var data = getDataSearch();
        getTourCategory();
        getListHotel(data);

        function getTourCategory() {
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                contentType: "application/json",
                success: function (response) {
                    console.log(response);
                    var tn = '';
                    var nn = '';
                    $(response).each(function (index, v) {
                        if (v.categoryType == 1) {
                            tn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>';
                        }else {
                            nn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>'
                        }

                    });
                    $('#trong-nuoc').html(tn);
                    $('#nuoc-ngoai').html(nn);


                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }

        $('#btn_search').click(function () {
            var data = getDataSearch();
            getListHotel(data);
        });


        function  getListHotel(data){
            var url ='';
            if (data.pageIndex) {
                url='/hotels?name='+data.name+'&pageIndex='+data.pageIndex;
            }else {
               url= '/hotels?name='+data.name;
            }
            $.ajax({
                url:url,
                type:'GET',
                contentType: "application/json",
                success: function (res){
                    var dt = res.data;

                    console.log(res);
                    if (dt.totalItem != 0) {
                        paging(dt.totalPage,res.currentPage +1);
                    }
                    showHotel(dt.data);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }
        function paging(totalPage,currentPage) {
            $('#pagination-demo').twbsPagination({
                totalPages: totalPage,
                startPage: currentPage,
                visiblePages: 10,
                last:'Cuối cùng',
                next:'Tiếp theo',
                first:'Đầu tiên',
                prev:'Phía trước',
                onPageClick: function (event, page) {
                    if (page) {
                        if (currentPage != page) {
                            var data = getDataSearch();
                            data["pageIndex"] = page;
                            getListHotel(data);
                        }
                    }

                }
            });
        }
        function getDataSearch(){
            var data = {};
            var formSearch = $('#search').serializeArray();

            $.each(formSearch,function (i,v) {
                data[""+v.name+""] = v.value;
            });

            return data;
        }

        function showHotel(data) {
            console.log(data);
            var s ='';
            var remote='';
            $('#tour1').html(remote);
            $('#tour2').html(remote);
            $('#tour3').html(remote);
            $('#tour4').html(remote);
            $('#tour5').html(remote);
            $('#tour6').html(remote);
            $("#noTour").html(remote);
            if (data.length === 0) {
                var s1 = `<div class="alert alert-warning text-center w-100 mt-3" style="color: #f6821f;  background-color: #fff3cd; border-color: #ffeeba; margin-top: 200px;">
             <i class="fa fa-exclamation-triangle"></i>  
             <a style="color: #f6821f;">Không tìm thấy tour nào !</a>`;
                s += `   <tr style="background-color: white">
                <td colspan="100">` + s1 + `</td>
                </tr> `;
                $("#noTour").html(s1);
            }

            $(data).each(function (index, v) {
                console.log(v);
                console.log(v.price);
                var price = v.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
                s+= '<div >'
                    +'    <div class="grid-block main-block t-grid-block">'
                    +'   <div class="main-img t-grid-img">'
                    +'     <a href="/page/hotel-detail/'+v.id+'">'
                    +'     <img src="'+v.thumbnail+'" class="img-responsive" alt="hotel-img"  style="height: 190px"/>'
                    +'     </a>'
                    +'     <div class="main-mask">'
                    +'     <ul class="list-unstyled list-inline offer-price-1">'
                    +'      <li class="price" style="font-size: 20px">'+price+'₫ /phòng</li>'
                    +' </ul>'
                    +' </div>'
                    +' </div>'

                    +'  <div class="block-info t-grid-info" style="height: 165px">'
                    +'     <div class="rating">'

                    +' </div>'
                    +' <p class="block-minor">Địa điểm: '+v.address+'</p>'
                    +' <h3 class="block-title"><a href="/page/hotel-detail/'+v.id+'">'+v.name+'</a></h3>'

                    // +' <div class="grid-btn">'
                    // +'     <a href="#" class="btn btn-orange btn-block btn-lg">View More</a>'
                    // +' </div>'
                    +' </div>'
                    +' </div>'
                    +' </div>'
                if (index == 0) {
                    $('#tour1').html(s);
                    s= '';
                }
                if (index == 1) {
                    $('#tour2').html(s);
                    s= '';
                }
                if (index == 2) {
                    $('#tour3').html(s);
                    s= '';
                }
                if (index == 3) {
                    $('#tour4').html(s);
                    s= '';
                }
                if (index == 4) {
                    $('#tour5').html(s);
                    s= '';
                }
                if (index == 5) {
                    $('#tour6').html(s);
                    s= '';
                }
            });

        }

    })
});