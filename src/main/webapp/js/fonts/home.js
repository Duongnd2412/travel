jQuery(function ($) {
    $(document).ready(function () {
        getListTourHot();
        getListHotelHot();
        getTourCategory();
        getListBlog();

        function getTourCategory() {
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                contentType: "application/json",

                success: function (response) {
                    console.log(response);
                    var tn = '';
                    var nn = '';
                    $(response).each(function (index, v) {
                        if (v.categoryType == 1) {
                            tn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>';
                        }else {
                            nn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>'
                        }

                    });
                    $('#trong-nuoc').html(tn);
                    $('#nuoc-ngoai').html(nn);


                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }


        function  getListTourHot(){
            $.ajax({
                url: '/customer/tours/relation',
                type:'GET',
                contentType: "application/json",
                success: function (res){
                    var dt = res.data;
                    showTourHot(dt.data);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }
        function  getListHotelHot(){
            $.ajax({
                url: '/hotels/hot',
                type:'GET',
                contentType: "application/json",
                success: function (res){
                    var dt = res.data;
                    // console.log(res);
                    showHotelHot(dt.data);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }
        function showHotelHot(data) {
            var s ='';
            $(data).each(function (index, v) {
                s+='<div class="item">'
                    +'<div class="main-block tour-block">'
                    +' <div class="main-img">'
                    +'  <a href="/page/hotel-detail/'+v.id+'">'
                    +'      <img src="'+v.thumbnail+'" class="img-responsive" alt="tour-img" style="height: 255px"/>'
                    +'  </a>'
                    +' </div>'

                    +' <div class="offer-price-2">'
                    +'  <ul class="list-unstyled">'
                    +'      <li class="price">Phone:'+v.publicPhone+'<a href="/page/hotel-detail/'+v.id+'" ><span class="arrow"><i class="fa fa-angle-right"></i></span></a></li>'
                    +'  </ul>'
                    +' </div>'

                    +' <div class="main-info tour-info">'
                    +'   <div class="main-title tour-title">'
                    +'      <a href="/page/hotel-detail/'+v.id+'">'+v.name+'</a>'
                    // +'      <p>Điểm đến: '+v.destinationPlace+'</p>'
                    +' </div>'
                    +'</div>'
                    +' </div>'
                    +' </div>';
                if (index == 0) {
                    $('#hotel1').html(s);
                    s= '';
                }
                if (index == 1) {
                    $('#hotel2').html(s);
                    s= '';
                }
                if (index == 2) {
                    $('#hotel3').html(s);
                    s= '';
                }
                if (index == 3) {
                    $('#hotel4').html(s);
                    s= '';
                }
                if (index == 4) {
                    $('#hotel5').html(s);
                    s= '';
                }
                if (index == 5) {
                    $('#hotel6').html(s);
                    s= '';
                }
            });

        }
        function showTourHot(data) {
            var s ='';
            console.log(data);

            $(data).each(function (index, v) {
            var price = v.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
            s+='<div class="item">'
                    +'<div class="main-block tour-block">'
                        +' <div class="main-img">'
                        +'  <a href="/page/tour-detail/'+v.id+'">'
                        +'      <img src="'+v.thumbnail+'" class="img-responsive" alt="tour-img" style="height: 255px"/>'
                        +'  </a>'
                        +' </div>'

                        +' <div class="offer-price-2">'
                        +'  <ul class="list-unstyled">'
                        +'      <li class="price">Chi phí: '+price+'vnđ<a href="/page/tour-detail/'+v.id+'"><span class="arrow"><i class="fa fa-angle-right"></i></span></a></li>'
                        +'  </ul>'
                        +' </div>'

                        +' <div class="main-info tour-info" style="height: 180px;">'
                        +'   <div class="main-title tour-title">'
                        +'      <a href="/page/tour-detail/'+v.id+'">'+v.name+'</a>'
                        +'      <p style="font-size: 12px">'+v.title+'</p>'
                        +'      <p style="font-size: 12px">Thời gian: '+v.time+'</p>'
                        +'      <p style="font-size: 12px">Phương tiện: '+v.vehicle+'</p>'
                        +' </div>'
                        +'</div>'
                    +' </div>'
                +' </div>';
                if (index == 0) {
                    $('#tour1').html(s);
                    s= '';
                }
                if (index == 1) {
                    $('#tour2').html(s);
                    s= '';
                }
                if (index == 2) {
                    $('#tour3').html(s);
                    s= '';
                }
                if (index == 3) {
                    $('#tour4').html(s);
                    s= '';
                }
                if (index == 4) {
                    $('#tour5').html(s);
                    s= '';
                }
                if (index == 5) {
                    $('#tour6').html(s);
                    s= '';
                }
            });

        }
        function getListBlog() {
            $.ajax({
                url: '/front/blogs/relation',
                type:'GET',
                // headers:{"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: "application/json",
                success: function (res){
                    var dt = res.data;
                    showBlogHot(dt);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }

        function showBlogHot(dt) {
            var s ='';
            $(dt.data).each(function (index, v) {
                s+='<div class="item">'
                    +'<div class="main-block tour-block">'
                    +' <div class="main-img">'
                    +'  <a href="#">'
                    +'      <img src="'+v.thumbnail+'" class="img-responsive" alt="tour-img" style="height: 255px"/>'
                    +'  </a>'
                    +' </div>'


                    +' <div class="main-info tour-info" style="height: 250px">'
                    +'   <div class="main-title tour-title">'
                    +'      <a href="#" style="font-size: 18px">'+v.title+'</a>'
                    +'      <p style="font-size: 13px">'+v.description.substring(0,100)+'...</p>'
                    +' </div>'
                    +'</div>'
                    +' </div>'
                    +' </div>';
                if (index == 0) {
                    $('#blog1').html(s);
                    s= '';
                }
                if (index == 1) {
                    $('#blog2').html(s);
                    s= '';
                }
                if (index == 2) {
                    $('#blog3').html(s);
                    s= '';
                }
                if (index == 3) {
                    $('#blog4').html(s);
                    s= '';
                }
                if (index == 4) {
                    $('#blog5').html(s);
                    s= '';
                }
                if (index == 5) {
                    $('#blog6').html(s);
                    s= '';
                }
            });

        }

    })
});