jQuery(function ($) {
    $(document).ready(function () {
        var data ={};
        getTourCategory();
        getListBlog(data);

        function getTourCategory() {
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                // headers: {"X-AUTH-TOKEN": localStorage.getItem("access-token")},
                contentType: "application/json",

                success: function (response) {
                    console.log(response);
                    var tn = '';
                    var nn = '';
                    $(response).each(function (index, v) {
                        if (v.categoryType == 1) {
                            tn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>';
                        }else {
                            nn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>'
                        }

                    });
                    $('#trong-nuoc').html(tn);
                    $('#nuoc-ngoai').html(nn);


                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }
        function  getListBlog(data){
            $.ajax({
                url: '/front/blogs',
                type: 'POST',
                data: JSON.stringify(data),
                dataType: 'json',
                contentType: "application/json",
                success: function (res){
                    var dt = res.data;
                    if (dt.totalItem != 0) {
                        paging(dt.totalPage,res.currentPage +1);
                    }
                    showBlog(dt);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }

        function paging(totalPage,currentPage) {
            $('#pagination-demo').twbsPagination({
                totalPages: totalPage,
                startPage: currentPage,
                visiblePages: 10,
                last:'Cuối cùng',
                next:'Tiếp theo',
                first:'Đầu tiên',
                prev:'Phía trước',
                onPageClick: function (event, page) {
                    if (page) {
                        if (currentPage != page) {
                            var data ={};
                            data["pageIndex"] = page;
                            getListBlog(data);
                        }
                    }

                }
            });
        }

        function showBlog(dt) {
            // console.log(data.data);
            var s ='';

            $(dt.data).each(function (index, v) {
            s+='<div class="main-block blog-post blog-list">'
            +'        <div class="main-img blog-post-img">'
                +'        <a href="/page/blog-detail/'+v.id+'">'
                +'  <img src="'+v.thumbnail+'" class="img-responsive" alt="blog-post-image" />'
                +'  </a>'
                +'  <div class="main-mask blog-post-info">'
                +'  <ul class="list-inline list-unstyled blog-post-info">'
                +'  <li><span><i class="fa fa-calendar"></i></span>Ngày viết</li>'
                +'   <li><span><i class="fa fa-user"></i></span>By:'+v.createdBy+'</li>'
                +'   </ul>'
                +'   </div>'
                +'   </div>'

                +'   <div class="blog-post-detail">'
                +'       <h2 class="blog-post-title"><a href="/page/blog-detail/'+v.id+'">'+v.title+'</a></h2>'
                +'   <p>'+v.description+'</p>'
                +'   <a href="/page/blog-detail/'+v.id+'" class="btn btn-orange">View More</a>'
                +'   </div>'
                +'   </div>';

            });
            $('#list-blog').html(s);

        }
    })
});