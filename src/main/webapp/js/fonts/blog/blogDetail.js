jQuery(function ($) {
    $(document).ready(function () {
        getTourCategory();
        getBlog();

        function getTourCategory() {
            $.ajax({
                url: "/customer/tour-category",
                type: "GET",
                contentType: "application/json",

                success: function (response) {
                    console.log(response);
                    var tn = '';
                    var nn = '';
                    $(response).each(function (index, v) {
                        if (v.categoryType == 1) {
                            tn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>';
                        }else {
                            nn +='<li><a href="/page/tour-search/'+v.id+'">'+v.name+'</a></li>'
                        }

                    });
                    $('#trong-nuoc').html(tn);
                    $('#nuoc-ngoai').html(nn);


                },
                error: function (response) {
                    alert("Thất bại");
                    console.log(response);
                }

            })
        }
        function  getBlog(){
            var id = $('#idBlog').val();
            $.ajax({
                url: '/front/blog/'+ id,
                type:'GET',
                contentType: "application/json",
                success: function (res){
                    var dt = res.data;
                    showBlog(dt);
                },
                error: function (e) {
                    alert("false");
                    console.log(e);
                }
            })
        }

        function showBlog(dt) {
            console.log(dt);
            var s ='';
            s+='<div class="main-block blog-post blog-list">'
               +'<h2>'+dt.title+'</h2>'
                +'  <img src="'+dt.thumbnail+'" class="img-responsive" alt="blog-post-image" />'
               +'<span style="font-size: 14px">'+dt.content+'</span>'
                +'</div>';


            $('#list-blog').html(s);

        }
    })
});