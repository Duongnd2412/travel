package travelling.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@EnableAutoConfiguration
@ServletComponentScan
@SpringBootApplication
public class TravellingApplication {

    public static void main(String[] args) {
        SpringApplication.run(TravellingApplication.class, args);
    }

}
