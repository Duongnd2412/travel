package travelling.api.app.mapper;

import lombok.Data;
import org.springframework.stereotype.Component;
import travelling.api.app.entity.Hotel;
import travelling.api.app.entity.HotelCustomerDetail;
import travelling.api.app.model.request.customer.CustomerSaveRequest;
import travelling.api.app.model.response.hotelcustomer.HotelCustomerResponse;
import travelling.api.app.repository.HotelRepository;
import travelling.api.app.util.BeanUtils;

import java.time.LocalDate;

@Component
@Data
public class HotelCustomerDetailMapper {

    private final HotelRepository hotelRepository;

    public HotelCustomerDetail convertToEntity(CustomerSaveRequest customerSaveRequest) {
        HotelCustomerDetail hotelCustomerDetail = new HotelCustomerDetail();
        hotelCustomerDetail.setNumberAdult(customerSaveRequest.getNumberAdult());
        hotelCustomerDetail.setNumberChildren(customerSaveRequest.getNumberChildren());
        hotelCustomerDetail.setNumberBed(customerSaveRequest.getNumberBed());
        hotelCustomerDetail.setNumberRoom(customerSaveRequest.getNumberRoom());
        hotelCustomerDetail.setCreatedDate(LocalDate.now());
        Hotel hotel = hotelRepository.getOne(customerSaveRequest.getHotelId());
        hotelCustomerDetail.setHotel(hotel);

        return hotelCustomerDetail;
    }

    public HotelCustomerResponse mapToResponse(HotelCustomerDetail hotelCustomerDetail){
        HotelCustomerResponse hotelCustomerResponse = new HotelCustomerResponse();
        BeanUtils.refine(hotelCustomerDetail, hotelCustomerResponse, BeanUtils::copyNonNull);

        return hotelCustomerResponse;
    }
}
