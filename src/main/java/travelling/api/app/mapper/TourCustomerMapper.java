package travelling.api.app.mapper;

import lombok.Data;
import org.springframework.stereotype.Component;
import travelling.api.app.entity.TourCustomerDetail;
import travelling.api.app.model.response.tourcustomer.TourCustomerResponse;
import travelling.api.app.util.BeanUtils;

@Component
public class TourCustomerMapper {

    public TourCustomerResponse mapToResponse(TourCustomerDetail tourCustomerDetail){
        TourCustomerResponse tourCustomerResponse = new TourCustomerResponse();
        BeanUtils.refine(tourCustomerDetail, tourCustomerResponse,BeanUtils::copyNonNull);

        return tourCustomerResponse;
    }

}
