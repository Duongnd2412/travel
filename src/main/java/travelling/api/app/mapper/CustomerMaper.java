package travelling.api.app.mapper;

import org.springframework.stereotype.Component;
import travelling.api.app.entity.Customer;
import travelling.api.app.entity.TourCustomerDetail;
import travelling.api.app.model.response.customer.CustomerRespon;
import travelling.api.app.util.BeanUtils;

import java.util.stream.Collectors;

@Component
public class CustomerMaper implements Mapper{

    public CustomerRespon convertResponseTour(Customer customer) {
        CustomerRespon customerRespon = new CustomerRespon();
        BeanUtils.refine(customer, customerRespon, BeanUtils::copyNonNull);
        if (customer.getTourCustomerDetail() != null){
            TourCustomerDetail tourCustomerDetail = customer.getTourCustomerDetail();
            if (tourCustomerDetail.getTour() != null){
                customerRespon.setNameTour(tourCustomerDetail.getTour().getName());
            }
            customerRespon.setNumberAdult(tourCustomerDetail.getNumberAdult());
            customerRespon.setNumberChildren(tourCustomerDetail.getNumberChildren());
            customerRespon.setCreatedDate(tourCustomerDetail.getCreatedDate());
            customerRespon.setNumberRoom(tourCustomerDetail.getNumberRoom());
        }

        return customerRespon;
    }

    public CustomerRespon convertResponseHotel(Customer customer) {
        CustomerRespon customerRespon = new CustomerRespon();
        BeanUtils.refine(customer, customerRespon, BeanUtils::copyNonNull);
        customerRespon.setNameHotel(customer.getHotelCustomerDetail().getHotel().getName());
        customerRespon.setNumberAdult(customer.getHotelCustomerDetail().getNumberAdult());
        customerRespon.setNumberChildren(customer.getHotelCustomerDetail().getNumberChildren());
        customerRespon.setCreatedDate(customer.getHotelCustomerDetail().getCreatedDate());
        customerRespon.setNumberRoom(customer.getHotelCustomerDetail().getNumberRoom());
        return customerRespon;
    }
}
