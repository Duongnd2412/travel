package travelling.api.app.mapper;

import org.springframework.stereotype.Component;
import travelling.api.app.entity.Hotel;
import travelling.api.app.model.request.hotel.HotelSaveRequest;
import travelling.api.app.model.request.hotel.HotelUpdateRequest;
import travelling.api.app.model.response.hotel.HotelResponse;
import travelling.api.app.util.BeanUtils;

@Component
public class HotelMapper {

    public Hotel convertToEntity(HotelSaveRequest hotelSaveRequest) {
        Hotel hotel = new Hotel();
        BeanUtils.refine(hotelSaveRequest, hotel, BeanUtils::copyNonNull);

        return hotel;
    }

    public Hotel convertToEntity(HotelUpdateRequest hotelUpdateRequest) {
        Hotel hotel = new Hotel();
        BeanUtils.refine(hotelUpdateRequest, hotel, BeanUtils::copyNonNull);
        if (hotelUpdateRequest.getIsHot().equals("1")){
            hotel.setIsHot(true);
        }else {
            hotel.setIsHot(false);
        }

        return hotel;
    }

    public HotelResponse convertToResponse(Hotel hotel) {
        HotelResponse hotelResponse = new HotelResponse();
        BeanUtils.refine(hotel, hotelResponse, BeanUtils::copyNonNull);
        hotelResponse.setLocaleType(hotel.getLocaleType());
        if (hotel.getIsHot() == null){
            hotel.setIsHot(false);
        }
        if (hotel.getIsHot()== true){
            hotelResponse.setIsHot("1");
        }else {
            hotelResponse.setIsHot("0");
        }

        return hotelResponse;
    }

}
