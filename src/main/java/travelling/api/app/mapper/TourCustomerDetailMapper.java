package travelling.api.app.mapper;

import lombok.Data;
import org.springframework.stereotype.Component;
import travelling.api.app.entity.Tour;
import travelling.api.app.entity.TourCustomerDetail;
import travelling.api.app.model.request.customer.CustomerSaveRequest;
import travelling.api.app.repository.TourRepository;

import java.time.LocalDate;

@Component
@Data
public class TourCustomerDetailMapper {

    private final TourRepository tourRepository;

    public TourCustomerDetail convertToEntity(CustomerSaveRequest customerSaveRequest) {
        TourCustomerDetail hotelCustomerDetail = new TourCustomerDetail();
        hotelCustomerDetail.setNumberAdult(customerSaveRequest.getNumberAdult());
        hotelCustomerDetail.setNumberChildren(customerSaveRequest.getNumberChildren());
        hotelCustomerDetail.setCreatedDate(LocalDate.now());

        Tour tour = tourRepository.getOne(customerSaveRequest.getTourId());
        hotelCustomerDetail.setTour(tour);

        return hotelCustomerDetail;
    }
}
