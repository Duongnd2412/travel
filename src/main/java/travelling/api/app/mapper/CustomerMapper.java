package travelling.api.app.mapper;

import org.springframework.stereotype.Component;
import travelling.api.app.entity.Customer;
import travelling.api.app.model.request.customer.CustomerSaveRequest;

@Component
public class CustomerMapper {

    public Customer convertToEntity(CustomerSaveRequest customerSaveRequest) {
        Customer customer = new Customer();
        customer.setEmail(customerSaveRequest.getEmail());
        customer.setFullName(customerSaveRequest.getFullName());
        customer.setPhone(customerSaveRequest.getPhone());

        return customer;
    }

}
