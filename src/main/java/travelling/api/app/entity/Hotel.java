package travelling.api.app.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Getter
@Setter
public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column(columnDefinition = "longtext")
    private String description;
    @Column
    private String publicPhone;
    @Column
    private String privatePhone;
    @Column
    private Integer localeType;
    @Column
    private Double price;
    @Column
    private Boolean isHot;
    @Column
    private String address;
    @Column
    private String thumbnail;
    @Column(columnDefinition = "longtext")
    private String albums;
    @Column
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDate createdDate;

    @OneToMany(mappedBy = "hotel",cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
    private List<HotelCustomerDetail> hotelCustomerDetails;
}
