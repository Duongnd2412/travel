package travelling.api.app.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class HotelCustomerDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Long numberAdult;
    @Column
    private Long numberChildren;
    @Column
    private LocalDate createdDate;
    @Column
    private Long numberRoom;
    @Column
    private Long numberBed;

    @ManyToOne
    @JoinColumn(name = "hotel_id")
    private Hotel hotel;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
}
