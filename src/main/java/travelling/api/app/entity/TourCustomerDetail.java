package travelling.api.app.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "tour_customer")
@Getter
@Setter
public class TourCustomerDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Long numberAdult;
    @Column
    private Long numberChildren;
    @Column
    private LocalDate createdDate;
    @Column
    private Long numberRoom;

    @ManyToOne
    @JoinColumn(name = "tour_id")
    private Tour tour;

    @OneToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
}
