package travelling.api.app.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;

@Entity(name = "customer")
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String fullName;
    @Column
    private String phone;
    @Column
    private String email;
    @Column
    private Integer isTourCustomer;


    @OneToOne(mappedBy = "customer")
    private TourCustomerDetail tourCustomerDetail;
    @OneToOne(mappedBy = "customer")
    private HotelCustomerDetail hotelCustomerDetail;
}
