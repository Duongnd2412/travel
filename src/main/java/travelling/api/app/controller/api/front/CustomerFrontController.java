package travelling.api.app.controller.api.front;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.customer.CustomerSaveRequest;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.front.CustomerFrontService;

@RestController
@Data
public class CustomerFrontController extends BaseController {

    private final CustomerFrontService customerFrontService;

    @PostMapping("/customer/order")
    public ResponseEntity<ObjectResponse> saveTour(@RequestBody CustomerSaveRequest customerSaveRequest){

        return success(customerSaveRequest, customerFrontService::save);
    }

}
