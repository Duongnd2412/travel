package travelling.api.app.controller.api.admin;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.hotel.HotelFilterRequest;
import travelling.api.app.model.request.hotel.HotelSaveRequest;
import travelling.api.app.model.request.hotel.HotelUpdateRequest;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.admin.HotelService;

@RestController
@Data
@RequestMapping("/admin")
public class HotelController extends BaseController {
    private final HotelService hotelService;

    @PostMapping("/hotel")
    public ResponseEntity<ObjectResponse> save(@RequestBody HotelSaveRequest hotelSaveRequest) {
        return success(hotelSaveRequest, hotelService::save);
    }

    @PutMapping("/hotel-update/{id}")
    public ResponseEntity<ObjectResponse> update(@RequestBody HotelUpdateRequest hotelUpdateRequest,
                                                 @PathVariable long id) {
        hotelUpdateRequest.setId(id);
        return success(hotelUpdateRequest, hotelService::update);
    }

    @GetMapping("/hotel/{id}")
    public ResponseEntity<ObjectResponse> findById(@PathVariable long id) {
        return success(id, hotelService::getById);
    }

    @PostMapping("/hotels")
    public ResponseEntity<ObjectResponse> getAll(@RequestBody HotelFilterRequest hotelFilterRequest) {
        return success(hotelFilterRequest, hotelService::getAll);
    }

    @DeleteMapping("/hotel/delete")
    public ResponseEntity<ObjectResponse> delete(@RequestBody Long[] ids){
        return success(ids,hotelService::delete);
    }


}
