package travelling.api.app.controller.api.front;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.BaseFilterRequest;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.front.BlogFrontService;

@RestController
@Data
public class BlogFrontController extends BaseController {
    private final BlogFrontService blogFrontService;


    @GetMapping("/front/blog/{id}")
    public ResponseEntity<ObjectResponse> getById(@PathVariable Long id) {
        return success(id, blogFrontService::findById);
    }

    @PostMapping("/front/blogs")
    public ResponseEntity<ObjectResponse> getAll(@RequestBody BaseFilterRequest baseFilterRequest) {
        return success(baseFilterRequest, blogFrontService::findAll);
    }

    @GetMapping("/front/blogs/relation")
    public ResponseEntity<ObjectResponse> getAllRelation() {
        return success(true, blogFrontService::findRelation);
    }

}
