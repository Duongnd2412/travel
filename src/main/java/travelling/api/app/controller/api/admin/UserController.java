package travelling.api.app.controller.api.admin;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelling.api.app.common.role.ADMIN;
import travelling.api.app.common.role.USER;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.user.*;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.admin.UserService;
import travelling.api.app.validate.UserValidate;

@RestController
@Data
@RequestMapping("/admin")
public class UserController extends BaseController {

    private final UserService userService;
    private final UserValidate userValidate;


    @PostMapping("/user")
    public ResponseEntity<ObjectResponse> save(@RequestBody UserSaveRequest userSaveRequest) {
        userSaveRequest = userValidate.validateUserSaveRequest(userSaveRequest);
        return success(userSaveRequest, userService::save);
    }
    @PostMapping("/users")
    public ResponseEntity<ObjectResponse> saveUser(@RequestBody UserSaveRequest userSaveRequest) {
        return success(userSaveRequest, userService::save);
    }

    @PostMapping("/auth")
    public ResponseEntity<ObjectResponse> auth(@RequestBody UserAuthRequest userAuthRequest) {
        userAuthRequest = userValidate.validateUserAuth(userAuthRequest);

        return success(userAuthRequest, userService::auth);
    }

    @GetMapping("/info")
    @USER
    public ResponseEntity<ObjectResponse> getInfo() {
        return success(userService::getInfo);
    }

    @GetMapping("/user/{id}")
    @ADMIN
    public ResponseEntity<ObjectResponse> getById(@PathVariable long id) {
        return success(id, userService::get);
    }

    @PatchMapping("/user/password")
    @USER
    public ResponseEntity<ObjectResponse> changePassword(@RequestBody UserChangePasswordRequest changePasswordRequest) {
        changePasswordRequest = userValidate.validateChangePassword(changePasswordRequest);

        return success(changePasswordRequest, userService::changePassword);
    }

    @PutMapping("/user/{id}")
    @USER
    public ResponseEntity<ObjectResponse> update(@PathVariable Long id, @RequestBody UserUpdateRequest userUpdateRequest) {
        userUpdateRequest.setId(id);
        return success(userUpdateRequest, userService::update);
    }

    @PostMapping("/users/list")
    @ADMIN
    public ResponseEntity<ObjectResponse> getAll(@RequestBody UserFilterRequest filterRequest) {
        return success(filterRequest, userService::getAll);
    }

    @DeleteMapping("/user")
    @ADMIN
    public ResponseEntity<ObjectResponse> delete(@RequestBody Long[] id){
        return success(id,userService::delete);
    }
}
