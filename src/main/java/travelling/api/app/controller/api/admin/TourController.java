package travelling.api.app.controller.api.admin;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelling.api.app.common.role.ADMIN;
import travelling.api.app.common.role.USER;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.tour.TourFilterRequest;
import travelling.api.app.model.request.tour.TourSaveRequest;
import travelling.api.app.model.request.tour.TourUpdateRequest;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.admin.TourService;

@RestController
@Data
@RequestMapping("/admin")
public class TourController extends BaseController {

    private final TourService tourService;

    @PostMapping("/tour")
    @ADMIN
    public ResponseEntity<ObjectResponse> save(@RequestBody TourSaveRequest tourSaveRequest) {
        return success(tourSaveRequest, tourService::save);
    }

    @PutMapping("/tour-update/{id}")
    @USER
    public ResponseEntity<ObjectResponse> update(@PathVariable long id, @RequestBody TourUpdateRequest tourUpdateRequest) {
        tourUpdateRequest.setId(id);

        return success(tourUpdateRequest, tourService::update);
    }


    @PatchMapping("/tour/{id}/hot")
    @ADMIN
    public ResponseEntity<ObjectResponse> updateHot(@RequestParam Boolean hot, @PathVariable long id) {
        TourUpdateRequest tourUpdateRequest = new TourUpdateRequest();
        tourUpdateRequest.setHot(hot);
        tourUpdateRequest.setId(id);
        return success(tourUpdateRequest, tourService::updateTourToHot);
    }

    @PatchMapping("/tour/{id}/status")
    @ADMIN
    public ResponseEntity<ObjectResponse> updateStatus(@RequestParam String status, @PathVariable long id) {
        TourUpdateRequest tourUpdateRequest = new TourUpdateRequest();
        tourUpdateRequest.setId(id);
        tourUpdateRequest.setStatus(status);

        return success(tourUpdateRequest, tourService::updateTourStatus);
    }

    @GetMapping("/tour/{id}")
    @ADMIN
    public ResponseEntity<ObjectResponse> getById(@PathVariable Long id) {
        return success(id, tourService::getById);
    }

    @PostMapping("/tours/test")
    public ResponseEntity<ObjectResponse> getAll(@RequestBody TourFilterRequest tourFilterRequest) {
        return success(tourFilterRequest, tourService::getAll);
    }

    @DeleteMapping("/tour/delete")
    public ResponseEntity<ObjectResponse> delete(@RequestBody Long[] ids){
        return success(ids,tourService::delete);
    }
}
