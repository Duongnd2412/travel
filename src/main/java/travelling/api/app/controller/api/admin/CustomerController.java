package travelling.api.app.controller.api.admin;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelling.api.app.common.role.ADMIN;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.customer.CustomerFilter;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.admin.CustomerService;

@RestController
@RequestMapping("/admin")
@Data
public class CustomerController extends BaseController {
    private final CustomerService service;

    @PostMapping("/customers")
    @ADMIN
    public ResponseEntity<ObjectResponse> gets(@RequestBody CustomerFilter filter) {
        return success(filter, service::getAll);
    }
}
