package travelling.api.app.controller.api.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelling.api.app.common.constant.BlogStatus;
import travelling.api.app.common.role.ADMIN;
import travelling.api.app.common.role.USER;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.blog.BlogFilterRequest;
import travelling.api.app.model.request.blog.BlogSaveRequest;
import travelling.api.app.model.request.blog.BlogUpdateRequest;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.admin.BlogService;

@RestController
@RequestMapping("/admin")
public class BlogController extends BaseController {

    private final BlogService blogService;

    @Autowired
    public BlogController(BlogService blogService) {
        this.blogService = blogService;
    }


    @PostMapping("/blog")
    @USER
    @ADMIN
    public ResponseEntity<ObjectResponse> save(@RequestBody BlogSaveRequest blogSaveRequest) {
        return success(blogSaveRequest, blogService::save);
    }

//    @PatchMapping("/blog/{id}/approve")
//    @ADMIN
//    public ResponseEntity<ObjectResponse> approve(@PathVariable long id) {
//        BlogUpdateRequest blogUpdateRequest = BlogUpdateRequest
//                .builder()
//                .idBlog(id)
//                .status(BlogStatus.APPROVE)
//                .build();
//        return success(blogUpdateRequest, blogService::updateStatus);
//    }
//
//    @PatchMapping("/blog/{id}/publish")
//    @ADMIN
//    public ResponseEntity<ObjectResponse> publish(@PathVariable Long id) {
//        BlogUpdateRequest blogUpdateRequest = BlogUpdateRequest
//                .builder()
//                .status(BlogStatus.PUBLISH)
//                .idBlog(id)
//                .build();
//        return success(blogUpdateRequest, blogService::updateStatus);
//    }

    @GetMapping("/blog/{id}")
    @ADMIN
    public ResponseEntity<ObjectResponse> findById(@PathVariable Long id) {
        return success(id, blogService::getById);
    }


    @PutMapping("/blog-update/{id}")
    public ResponseEntity<ObjectResponse> update(@PathVariable Long id, @RequestBody BlogUpdateRequest blogUpdateRequest) {
        blogUpdateRequest.setIdBlog(id);
        return success(blogUpdateRequest, blogService::update);
    }

    @GetMapping("/blogs")
    @ADMIN
    public ResponseEntity<ObjectResponse> gets(@ModelAttribute BlogFilterRequest blogFilterRequest) {
        return success(blogFilterRequest, blogService::getAll);
    }

    @DeleteMapping("/blog/delete")
    public ResponseEntity<ObjectResponse> delete(@RequestBody Long[] ids){
        return success(ids,blogService::delete);
    }
}
