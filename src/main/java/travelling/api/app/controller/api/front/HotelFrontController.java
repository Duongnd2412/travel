package travelling.api.app.controller.api.front;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.hotel.HotelFilterRequest;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.service.admin.HotelService;

@RestController
@Data
public class HotelFrontController extends BaseController {

    private final HotelService hotelService;

    @GetMapping("/hotel/{id}")
    public ResponseEntity<ObjectResponse> findById(@PathVariable long id) {
        return success(id, hotelService::getById);
    }

    @GetMapping("/hotels")
    public ResponseEntity<ObjectResponse> getAll(@ModelAttribute HotelFilterRequest hotelFilterRequest) {
        return success(hotelFilterRequest, hotelService::getAll);
    }

    @GetMapping("/hotels/hot")
    public ResponseEntity<ObjectResponse> getAll(){
        return success(true, hotelService::findHotelHot);
    }
}
