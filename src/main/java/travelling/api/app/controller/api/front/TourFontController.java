package travelling.api.app.controller.api.front;

import lombok.Data;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.request.tour.TourFilterCategoryRequest;
import travelling.api.app.model.request.tour.TourFilterRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.model.response.tour.TourResponse;
import travelling.api.app.service.admin.TourService;
import travelling.api.app.service.front.TourFrontService;

import java.util.List;

@RestController
@RequestMapping("/customer")
@Data
public class TourFontController extends BaseController {

    private final TourFrontService tourFrontService;
    private final TourService tourService;

    @GetMapping("/tour/{id}")
    public ResponseEntity<ObjectResponse> getById(@PathVariable Long id)
    {
        return success(id, tourService::getById);
    }

//    @PostMapping("/tours")
//    public ResponseEntity<ObjectResponse> getAll(@RequestBody TourFilterRequest tourFilterRequest) {
//        return success(tourFilterRequest, tourService::getAll);
//    }

    @PostMapping("/tours")
    public ResponseEntity<ListResponse<TourResponse>> getAll(@RequestBody TourFilterRequest tourFilterRequest) {
        return ResponseEntity.ok(tourService.getAll(tourFilterRequest));
    }


    @PostMapping("/tours-category")
    public ResponseEntity<ObjectResponse> getByCategoryId(@RequestBody TourFilterCategoryRequest tourFilterCategoryRequest) {
        return success(tourFilterCategoryRequest, tourFrontService::findAllByTourCategory);
    }

    @GetMapping("/tours/relation")
    public ResponseEntity<ObjectResponse> getRelation() {
        return success(true, tourFrontService::findAllTourRelation);
    }
}
