package travelling.api.app.controller.api.front;

import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import travelling.api.app.controller.BaseController;
import travelling.api.app.model.response.ObjectResponse;
import travelling.api.app.model.response.tourcategory.TourCategoryResponse;
import travelling.api.app.service.front.TourCategoryFrontService;

import java.util.List;

@RestController
@Data
public class TourCategoryFrontController extends BaseController {

    private final TourCategoryFrontService categoryFrontService;

//    @GetMapping("")
//    public ResponseEntity<ObjectResponse> findAll(@RequestParam int categoryType) {
//        return success(categoryType, categoryFrontService::findAllByCategoryType);
//    }
    @GetMapping("/customer/tour-category")
    public ResponseEntity<List<TourCategoryResponse>> getAll() {
        return new ResponseEntity<>(categoryFrontService.findAllByCategoryType(), HttpStatus.OK);
    }

}
