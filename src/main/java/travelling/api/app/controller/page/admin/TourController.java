package travelling.api.app.controller.page.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("adminTour")
@RequestMapping("/admin")
public class TourController {
    @GetMapping("/tour-list")
    public String tourPage() {
        return "admin/tour/list";
    }

    @GetMapping("/tour-create")
    public String create() {
        return "admin/tour/edit";
    }

    @GetMapping("/tour-edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("admin/tour/edit");
        mav.addObject("idTour", id);
        return mav;
    }
}
