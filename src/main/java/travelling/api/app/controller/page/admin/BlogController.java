package travelling.api.app.controller.page.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("adminBlog")
@RequestMapping("/admin")
public class BlogController {
    @GetMapping("/blog-list")
    public String blogPage() {
        return "admin/blog/list";
    }

    @GetMapping("/blog-create")
    public String create() {
        return "admin/blog/edit";
    }

    @GetMapping("/blog-edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("admin/blog/edit");
        mav.addObject("idBlog", id);
        return mav;
    }
}
