package travelling.api.app.controller.page.customer;


import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import travelling.api.app.model.request.tour.TourFilterRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.tour.TourDetailResponse;
import travelling.api.app.model.response.tour.TourResponse;
import travelling.api.app.service.admin.TourService;

import java.util.ArrayList;
import java.util.List;

@Controller("pageTour")
@RequestMapping("/page")
@Data
public class TourController {

    private final TourService tourService;

    @GetMapping("/home")
    public String tourPage(){
        return "client/home";
    }

    @GetMapping("/tour-all")
    public String tourPageAll(){
        return "client/tour/tourListAll";
    }

    @GetMapping("/tour-detail/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("client/tour/tourDetail");
        mav.addObject("idTour", id);
        return mav;
    }

    @GetMapping("/tour-search/{id}")
    public ModelAndView search(@PathVariable("id") Long idCategory) {
        ModelAndView mav = new ModelAndView("client/tour/tourListAll");
        mav.addObject("idCategory", idCategory);
        return mav;
    }



//    @GetMapping("/tour/{id}")
//    public ModelAndView getById(@PathVariable Long id) {
//        ModelAndView mav = new ModelAndView("tour-detail");
//        TourResponse tour = tourService.getById(id);
//        TourDetailResponse tourResponse = tourService.findTourById(id);
//        List<TourResponse> tourResponses = tourService.findAll();
//        mav.addObject("tour",tour);
//        mav.addObject("tours",tourResponse);
//        mav.addObject("data",tourResponses);
//        return mav;
//    }
//
//    @GetMapping(value = "/tour")
//    public ModelAndView tour(){
//        ModelAndView modelAndView = new ModelAndView("tour-home");
//        List<TourResponse> tourResponses = tourService.findAll();
//        modelAndView.addObject("tours",tourResponses);
//        return modelAndView;
//    }
//
//    @GetMapping("/tour-list")
//    public ModelAndView tourList(){
//        ModelAndView mav = new ModelAndView("tour-list");
//        TourFilterRequest tourFilterRequest = new TourFilterRequest();
//        ListResponse<TourResponse> tourResponse;
//        tourResponse = tourService.getAll(tourFilterRequest);
//        mav.addObject("tours",tourResponse);
//        return mav;
//    }
}
