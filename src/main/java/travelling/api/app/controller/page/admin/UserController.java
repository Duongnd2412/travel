package travelling.api.app.controller.page.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("adminUser")
@RequestMapping("/admin")
public class UserController {
    @GetMapping("/user-list")
    public String tourPage() {
        return "admin/user/list";
    }

    @GetMapping("/user-create")
    public String create() {
        return "admin/user/edit";
    }

    @GetMapping("/user-edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("admin/user/edit");
        mav.addObject("idUser", id);
        return mav;
    }
}
