package travelling.api.app.controller.page.customer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/page")
public class Registration {
    @GetMapping("/registration")
    public ModelAndView get(){
        ModelAndView mav = new ModelAndView("registration");
        return mav;
    }
}
