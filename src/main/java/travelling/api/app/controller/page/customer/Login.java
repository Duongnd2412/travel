package travelling.api.app.controller.page.customer;

import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@Data
public class Login {


    @GetMapping("/customer-login-page")
    public ModelAndView customerLoginPage() {
        ModelAndView mav = new ModelAndView("/client/login");

        return mav;
    }

}
