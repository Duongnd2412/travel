package travelling.api.app.controller.page.admin;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("adminHotel")
@RequestMapping("/admin")
public class HotelController {
    @GetMapping("/hotel-list")
    public String tourPage() {
        return "admin/hotel/list";
    }

    @GetMapping("/hotel-create")
    public String create() {
        return "admin/hotel/edit";
    }

    @GetMapping("/hotel-edit/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("admin/hotel/edit");
        mav.addObject("idHotel", id);
        return mav;
    }
}
