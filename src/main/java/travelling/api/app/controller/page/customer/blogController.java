package travelling.api.app.controller.page.customer;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller("pageBlog")
@RequestMapping("/page")
public class blogController {

    @GetMapping("/blog-all")
    public String hotelPage(){
        return "client/blog/listAll";
    }

    @GetMapping("/blog-detail/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("client/blog/blogDetail");
        mav.addObject("idBlog", id);
        return mav;
    }
}
