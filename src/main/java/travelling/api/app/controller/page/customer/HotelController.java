package travelling.api.app.controller.page.customer;


import lombok.Data;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller("pageHotel")
@RequestMapping("/page")
@Data
public class HotelController {

    @GetMapping("/hotel-all")
    public String hotelPage(){
        return "client/hotel/listAll";
    }



    @GetMapping("/hotel-detail/{id}")
    public ModelAndView edit(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("client/hotel/hotelDetail");
        mav.addObject("idHotel", id);
        return mav;
    }
}
