package travelling.api.app.model.response.tour;

import lombok.Data;

import javax.persistence.Column;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class TourResponse {
    private Long id;
    private Double price;

    private Double childPrice;
    private String policy;
    private Double babyPrice;
    private String discount;
    private Long duration;
    private Long seats;
    private List<String> albums = new ArrayList<>();
    private String name;
    private String title;
    private String keyword;
    private String thumbnail;
    private String createdBy;
    private String description;
    private String startPlace;
    private String destinationPlace;
    private String content;
    private LocalDate createdDate;
    private String isHot;

    private String vehicle;
    private String status;
    private Integer tourType;
    private String time;
    private Long tourCategoryId;
}
