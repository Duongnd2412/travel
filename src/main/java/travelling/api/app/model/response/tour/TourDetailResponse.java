package travelling.api.app.model.response.tour;

import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class TourDetailResponse {
    private Long id;
    private String name;
    private Double price;
    private Double childPrice;
    private Double babyPrice;
    private long priceBaby;
    private long priceChildren;
    private String discount;
    private long seats;
    private String title;
    private String content;
    private String vehicle;
    private String keyword;
    private String startPlace;
    private String destinationPlace;
    private Long tourCategoryId;
    private String description;
    private List<String> albums = new ArrayList<>();
    private String thumbnail;
    private String status;
    private Boolean isHot;
    private Integer tourType;
    private String createdBy;
    private LocalDate createdDate;
    private LocalDate modifiedDate;
    private String modifiedBy;
}

