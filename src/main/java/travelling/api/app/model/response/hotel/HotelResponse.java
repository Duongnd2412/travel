package travelling.api.app.model.response.hotel;

import lombok.Data;

@Data
public class HotelResponse {
    private Long id;
    private String name;
    private String isHot;
    private String description;
    private String publicPhone;
    private String privatePhone;
    private Integer localeType;
    private String thumbnail;
    private String albums;
    private String address;
    private Double price;
}
