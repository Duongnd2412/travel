package travelling.api.app.model.response.user;

import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

import java.time.LocalDate;
import java.util.List;

@Data
public class UserResponse extends BaseFilterRequest {
    private Long id;
    private String userName;
    private String password;
    private String email;
    private String name;
    private String phone;
    private String address;
    private String avatar;
    private String status;
    private String sex;
    private LocalDate createdDate;
    private List<Long> roleId;
}
