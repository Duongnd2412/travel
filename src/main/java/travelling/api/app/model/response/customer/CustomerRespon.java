package travelling.api.app.model.response.customer;

import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

import java.time.LocalDate;
import java.util.List;

@Data
public class CustomerRespon extends BaseFilterRequest {
    private String fullName;

    private String phone;

    private String email;

    private LocalDate createdDate;

    private String nameTour;

    private String nameHotel;

    private Long numberAdult;
    private Long numberChildren;
    private Long numberRoom;


}
