package travelling.api.app.model.response.tourcustomer;

import lombok.Data;

import java.time.LocalDate;

@Data
public class TourCustomerResponse {
    private Long id;
    private Long numberAdult;
    private Long numberChildren;
    private LocalDate createdDate;
    private Long numberRoom;
}
