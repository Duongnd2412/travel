package travelling.api.app.model.response.hotelcustomer;

import lombok.Data;

import java.time.LocalDate;

@Data
public class HotelCustomerResponse {
    private Long id;
    private Long numberAdult;
    private Long numberChildren;
    private LocalDate createdDate;
    private Long numberRoom;
    private Long numberBed;
}
