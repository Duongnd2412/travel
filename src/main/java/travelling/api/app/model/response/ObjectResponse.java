package travelling.api.app.model.response;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ObjectResponse {
    private ResponseStatus status;
    private Object data;

    @Getter
    @Setter
    @ToString
    public static class ResponseStatus {
        private String code;
        private String message;
        private int statusCode;
    }
}
