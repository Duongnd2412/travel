package travelling.api.app.model.request.tour;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class TourUpdateRequest {
    @JsonIgnore
    private Long id;
    @JsonIgnore
    private Boolean hot;

    private String isHot;

    private String status;
    private String title;
    private String content;
    private String description;
    private String policy;
    private String keyword;
    private String albums;
    private String thumbnail;
    private Long tourCategoryId;
    private Integer tourType;
    private String destinationPlace;
    private String startPlace;
    private String name;
    private String vehicle;
    private Double price;
    private Double childPrice;
    private Double babyPrice;
}
