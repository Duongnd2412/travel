package travelling.api.app.model.request.customer;

import lombok.Data;

@Data
public class CustomerSaveRequest {
    private String fullName;
    private String phone;
    private String email;
    private Long numberAdult;
    private Long numberChildren;
    private Long numberRoom;
    private Long numberBed;
    private Long tourId ;
    private Long hotelId ;
    private Integer isTourCustomer;
}
