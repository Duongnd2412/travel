package travelling.api.app.model.request.hotel;

import lombok.Data;

@Data
public class HotelUpdateRequest {
    private Long id;
    private String name;
    private String isHot;
    private String description;
    private String thumbnail;
    private String publicPhone;
    private String privatePhone;
    private String albums;
    private Integer localeType;
    private Double price;
    private String address;
}
