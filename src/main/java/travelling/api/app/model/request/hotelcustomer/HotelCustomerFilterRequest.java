package travelling.api.app.model.request.hotelcustomer;

import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

@Data
public class HotelCustomerFilterRequest extends BaseFilterRequest {
}
