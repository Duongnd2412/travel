package travelling.api.app.model.request.user;

import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

@Data
public class UserFilterRequest extends BaseFilterRequest {
    private String userName;
    private String name;
}
