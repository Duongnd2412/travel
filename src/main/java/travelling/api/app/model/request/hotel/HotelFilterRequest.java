package travelling.api.app.model.request.hotel;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

@Data
public class HotelFilterRequest extends BaseFilterRequest {
    @JsonProperty("name")
    private String name = null;
    @JsonProperty("localeType")
    private Long localeType;
    @JsonProperty("address")
    private String address;
    private Long id;
}
