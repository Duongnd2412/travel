package travelling.api.app.model.request.user;


import lombok.Data;

import java.util.Set;

@Data
//@Builder
public class UserUpdateRequest {
    private Long id;
    private String userName;
    private String email;
    private String phone;
    private String avatar;
    private String name;
    private String status;
    private String address;
    private String sex;
    private Set<Long> roleId;
}
