package travelling.api.app.model.request.blog;

import lombok.Builder;
import lombok.Data;

@Data
//@Builder
public class BlogUpdateRequest {
    private Long idBlog;
    private String status;
    private String thumbnail;
    private String title;
    private String keyword;
    private String content;
    private String description;
}
