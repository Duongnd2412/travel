package travelling.api.app.model.request.tour;

import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

@Data
public class TourFilterCategoryRequest extends BaseFilterRequest {
    private Long categoryId;
}
