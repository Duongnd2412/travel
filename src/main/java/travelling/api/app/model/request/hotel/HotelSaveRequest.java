package travelling.api.app.model.request.hotel;

import lombok.Data;

@Data
public class HotelSaveRequest {
    private String name;
    private String publicPhone;
    private String privatePhone;
    private Integer localeType;
    private String description;
    private String thumbnail;
    private String albums;
    private Double price;
}
