package travelling.api.app.model.request.tourcustomer;

import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

@Data
public class TourCustomerFilterRequest extends BaseFilterRequest {

}
