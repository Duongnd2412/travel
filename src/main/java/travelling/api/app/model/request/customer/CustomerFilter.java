package travelling.api.app.model.request.customer;

import lombok.Data;
import travelling.api.app.model.request.BaseFilterRequest;

@Data
public class CustomerFilter extends BaseFilterRequest {
    private String phone;
    private String fullName;
    private Integer isTourCustomer;
}
