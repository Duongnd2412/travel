package travelling.api.app.model.request.user;

import lombok.Data;

import java.util.Set;

@Data
public class UserSaveRequest {
    private String userName;
    private String password;
    private String email;
    private String phone;
    private String name;
    private String status;
    private String address;
    private String avatar;
    private String sex;
    private Set<Long> roleId;
}
