package travelling.api.app.service.admin.impl;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.Customer;
import travelling.api.app.mapper.CustomerMaper;
import travelling.api.app.model.request.customer.CustomerFilter;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.customer.CustomerRespon;
import travelling.api.app.repository.CustomerRepository;
import travelling.api.app.repository.specification.CustomerSpecification;
import travelling.api.app.service.admin.CustomerService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class CustomerServiceImpl implements CustomerService {
    private final CustomerMaper customerMaper;
    private final CustomerRepository repository;


    @Override
    public ListResponse<CustomerRespon> getAll(CustomerFilter customerFilter) {
        Sort sort = Sort.by(Sort.Order.desc("id"));
        Pageable pageable = PageRequest.of(customerFilter.getPageIndex() -1,10,sort);
        List<CustomerRespon> customers = new ArrayList<>();
        if (customerFilter.getIsTourCustomer() ==1){
            customers = repository.findAll(CustomerSpecification.filter(customerFilter),pageable)
                    .stream()
                    .map(items -> customerMaper.convertResponseTour(items)).collect(Collectors.toList());
        }else {
            customers = repository.findAll(CustomerSpecification.filter(customerFilter),pageable)
                    .stream()
                    .map(items -> customerMaper.convertResponseHotel(items)).collect(Collectors.toList());
        }

        int count = countCustomer(customerFilter);
        Integer totalPage = (int) Math.ceil((double) count / 10);
        return ListResponse.of(totalPage,count, customers);
    }

    private int countCustomer(CustomerFilter customerFilter) {
        return (int) repository.count(CustomerSpecification.filter(customerFilter));
    }
}
