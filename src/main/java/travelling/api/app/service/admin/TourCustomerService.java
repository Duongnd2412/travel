package travelling.api.app.service.admin;

import travelling.api.app.model.request.tourcustomer.TourCustomerFilterRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.tourcustomer.TourCustomerResponse;

public interface TourCustomerService {
    ListResponse<TourCustomerResponse> findAll(TourCustomerFilterRequest filterRequest);
}
