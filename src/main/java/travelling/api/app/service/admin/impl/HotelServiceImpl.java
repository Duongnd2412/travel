package travelling.api.app.service.admin.impl;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.Hotel;
import travelling.api.app.exception.ObjectNotFoundException;
import travelling.api.app.mapper.HotelMapper;
import travelling.api.app.model.request.hotel.HotelFilterRequest;
import travelling.api.app.model.request.hotel.HotelSaveRequest;
import travelling.api.app.model.request.hotel.HotelUpdateRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.hotel.HotelResponse;
import travelling.api.app.repository.HotelRepository;
import travelling.api.app.repository.specification.HotelSpecification;
import travelling.api.app.service.admin.HotelService;
import travelling.api.app.util.CopyModelUtil;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Data
public class HotelServiceImpl implements HotelService {

    private final HotelRepository hotelRepository;
    private final HotelMapper hotelMapper;

    @Override
    public void save(HotelSaveRequest hotelSaveRequest) {
        Hotel hotel = hotelMapper.convertToEntity(hotelSaveRequest);
        hotelRepository.save(hotel);
    }

    @Override
    public HotelResponse getById(long id) {
        Optional<Hotel> hotel = hotelRepository.findById(id);

        hotel.orElseThrow(() -> new ObjectNotFoundException("Hotel not found with " + id));

        return hotelMapper.convertToResponse(hotel.get());
    }

    @Override
    public ListResponse<HotelResponse> getAll(HotelFilterRequest hotelFilterRequest) {
        PageRequest pageRequest = PageRequest.of(hotelFilterRequest.getPageIndex(), 6, Sort.by("id").descending());
        Page<Hotel> page = hotelRepository.findAll(HotelSpecification.filter(hotelFilterRequest),pageRequest.previousOrFirst());
        List<HotelResponse> hotelResponses = page.stream().map(hotelMapper::convertToResponse).collect(Collectors.toList());
        Long count = page.getTotalElements();
        Integer totalPage = (int) Math.ceil((double) count / 6);
        return ListResponse.of(totalPage,count, hotelResponses);
    }

    @Override
    public void update(HotelUpdateRequest hotelUpdateRequest) {
        Hotel hotel = hotelRepository.getOne(hotelUpdateRequest.getId());
        Hotel newHotel = hotelMapper.convertToEntity(hotelUpdateRequest);

        CopyModelUtil.copyOldToNewModel(hotel, newHotel);

        hotelRepository.save(newHotel);
    }

    @Override
    public ListResponse<HotelResponse> findHotelHot(Boolean check) {
        PageRequest pageRequest = PageRequest.of(1, 6, Sort.by("createdDate").descending());
        Page<Hotel> hotels = hotelRepository.findAll(HotelSpecification.filterHotelHot(check),pageRequest.previousOrFirst());
        List<HotelResponse> hotelResponses = hotels.stream().map(hotelMapper::convertToResponse).collect(Collectors.toList());
        return ListResponse.of(hotels.getTotalElements(), hotelResponses);
    }

    @Override
    public boolean delete(Long[] id) {
        try {
            for (Long ids : id) {
                hotelRepository.deleteById(ids);
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
