package travelling.api.app.service.admin;

import travelling.api.app.model.request.customer.CustomerFilter;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.customer.CustomerRespon;

public interface CustomerService {

    ListResponse<CustomerRespon> getAll(CustomerFilter customerFilter);
}
