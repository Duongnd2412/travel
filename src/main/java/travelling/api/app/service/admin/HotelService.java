package travelling.api.app.service.admin;


import travelling.api.app.model.request.hotel.HotelFilterRequest;
import travelling.api.app.model.request.hotel.HotelSaveRequest;
import travelling.api.app.model.request.hotel.HotelUpdateRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.hotel.HotelResponse;

public interface HotelService {
    void save(HotelSaveRequest hotelSaveRequest);

    HotelResponse getById(long id);

    ListResponse<HotelResponse> getAll(HotelFilterRequest hotelFilterRequest);

    void update(HotelUpdateRequest hotelUpdateRequest);

    ListResponse<HotelResponse> findHotelHot(Boolean check);
    boolean delete(Long[] id);
}
