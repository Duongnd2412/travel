package travelling.api.app.service.admin;


import travelling.api.app.model.request.blog.BlogFilterRequest;
import travelling.api.app.model.request.blog.BlogSaveRequest;
import travelling.api.app.model.request.blog.BlogUpdateRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.blog.BlogDetailResponse;
import travelling.api.app.model.response.blog.BlogResponse;

public interface BlogService {

    void save(BlogSaveRequest blogSaveRequest);

    void updateStatus(BlogUpdateRequest blogUpdateRequest);

    BlogDetailResponse getById(long id);

    ListResponse<BlogResponse> getAll(BlogFilterRequest blogFilterRequest);

    void update(BlogUpdateRequest blogUpdateRequest);

    boolean delete(Long[] id);
}
