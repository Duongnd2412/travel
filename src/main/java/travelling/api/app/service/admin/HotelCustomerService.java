package travelling.api.app.service.admin;

import travelling.api.app.model.request.hotelcustomer.HotelCustomerFilterRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.hotelcustomer.HotelCustomerResponse;



public interface HotelCustomerService {
    ListResponse<HotelCustomerResponse> findAll(HotelCustomerFilterRequest filterRequest);
}
