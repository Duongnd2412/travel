package travelling.api.app.service.admin.impl;


import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import travelling.api.app.common.constant.MessageConstant;
import travelling.api.app.entity.Tour;
import travelling.api.app.entity.TourCategory;
import travelling.api.app.entity.TourDetail;
import travelling.api.app.exception.ObjectNotFoundException;
import travelling.api.app.mapper.TourMapper;
import travelling.api.app.model.request.tour.TourFilterRequest;
import travelling.api.app.model.request.tour.TourSaveRequest;
import travelling.api.app.model.request.tour.TourUpdateRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.tour.TourDetailResponse;
import travelling.api.app.model.response.tour.TourResponse;
import travelling.api.app.repository.TourCategoryRepository;
import travelling.api.app.repository.TourDetailRepository;
import travelling.api.app.repository.TourRepository;
import travelling.api.app.repository.specification.TourSpecification;
import travelling.api.app.service.admin.TourService;
import travelling.api.app.service.admin.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Data
public class TourServiceImpl implements TourService {

    private final TourMapper tourMapper;
    private final UserService userService;
    private final TourRepository tourRepository;

    @Autowired
    private TourCategoryRepository tourCategoryRepository;

    private final TourDetailRepository tourDetailRepository;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(TourSaveRequest tourSaveRequest) {
        Tour tour = tourMapper.convertToEntity(tourSaveRequest);

        tourRepository.save(tour);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTourToHot(TourUpdateRequest hotRequest) {
        Tour tour = tourRepository.getOne(hotRequest.getId());
        tour.setIsHot(hotRequest.getHot());

        tourRepository.save(tour);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateTourStatus(TourUpdateRequest tourUpdateRequest) {
        Tour tour = tourRepository.getOne(tourUpdateRequest.getId());
        tour.setStatus(tourUpdateRequest.getStatus());

        tourRepository.save(tour);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public TourResponse getById(long id) {
//        JwtPayload jwtPayload = userService.parseToken();

        Optional<Tour> tour = tourRepository.findById(id);
        if (!tour.isPresent()) {
            throw new ObjectNotFoundException(MessageConstant.TOUR_NOT_FOUND.value());
        }
        TourResponse tourResponse = tourMapper.convertToResponse(tour.get());
        return tourResponse;
    }

    @Override
    public TourDetailResponse findTourById(long id) {
        Optional<TourDetail> tour = tourDetailRepository.findById(id);
        if (!tour.isPresent()) {
            throw new ObjectNotFoundException(MessageConstant.TOUR_NOT_FOUND.value());
        }
        TourDetailResponse tourDetailResponse = tourMapper.convertToDetail(tour.get());
        return tourDetailResponse;
    }

    @Override
    public List<TourResponse> findAll() {
        return tourRepository.findAll().stream().map(tourMapper::convertToResponse).collect(Collectors.toList());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ListResponse<TourResponse> getAll(TourFilterRequest tourFilterRequest) {
        PageRequest pageRequest = PageRequest.of(tourFilterRequest.getPageIndex()-1, 6, Sort.by("id").descending());
        Page<Tour> page = tourRepository.findAll(TourSpecification.tourFilter(tourFilterRequest), pageRequest);
        List<TourResponse> tourResponses = page.get().map(tourMapper::convertToResponse).collect(Collectors.toList());
        long totalItem = page.getTotalElements();
        Integer totalPage = (int) Math.ceil((double) totalItem / 6);
        return ListResponse.of(totalPage,totalItem, tourResponses);
    }

    @Override
    public void update(TourUpdateRequest tourUpdateRequest) {
        Tour tour = tourRepository.getOne(tourUpdateRequest.getId());
        TourCategory tourCategory = tourCategoryRepository.findById(tourUpdateRequest.getTourCategoryId()).get();
        tour.setTitle(tourUpdateRequest.getTitle());
        tour.setContent(tourUpdateRequest.getContent());
        tour.setDescription(tourUpdateRequest.getDescription());
        tour.setPolicy(tourUpdateRequest.getPolicy());
        tour.setKeyword(tourUpdateRequest.getKeyword());
        tour.setAlbums(tourUpdateRequest.getAlbums());
        tour.setThumbnail(tourUpdateRequest.getThumbnail());
        tour.setName(tourUpdateRequest.getName());
        tour.setTourType(tourUpdateRequest.getTourType());
        tour.setTourCategory(tourCategory);
        tour.setStartPlace(tourUpdateRequest.getStartPlace());
        tour.setDestinationPlace(tourUpdateRequest.getDestinationPlace());
        tour.setVehicle(tourUpdateRequest.getVehicle());
        tour.setPrice(tourUpdateRequest.getPrice());
        tour.setStatus(tourUpdateRequest.getStatus());
        tour.setChildPrice(tourUpdateRequest.getChildPrice());
        if (tourUpdateRequest.getIsHot().equals("1")){
            tour.setIsHot(true);
        }else {
            tour.setIsHot(false);
        }

        tourRepository.save(tour);
    }

    @Override
    public boolean delete(Long[] id) {
        try {
            for (Long ids : id) {
                Tour tour = tourRepository.getOne(ids);
                if (tour.getStatus().equals("OFF")) {
                    tourRepository.deleteById(ids);
                } else {
                    return false;
                }

            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }
}
