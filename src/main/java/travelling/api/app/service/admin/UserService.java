package travelling.api.app.service.admin;

import travelling.api.app.model.request.user.*;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.user.UserResponse;
import travelling.api.security.jwt.model.JwtPayload;

public interface UserService {
    void save(UserSaveRequest userSaveRequest);

    String auth(UserAuthRequest userAuthRequest);

    UserResponse getInfo();

    UserResponse get(Long id);

    void changePassword(UserChangePasswordRequest changePassword);

    JwtPayload parseToken();

    void update(UserUpdateRequest userUpdateRequest);

    ListResponse<UserResponse> getAll(UserFilterRequest filterRequest);

    boolean delete(Long[] id);
}
