package travelling.api.app.service.admin.impl;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.HotelCustomerDetail;
import travelling.api.app.mapper.HotelCustomerDetailMapper;
import travelling.api.app.model.request.hotelcustomer.HotelCustomerFilterRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.hotelcustomer.HotelCustomerResponse;
import travelling.api.app.repository.HotelCustomerDetailRepository;
import travelling.api.app.service.admin.HotelCustomerService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class HotelCustomerServiceImpl implements HotelCustomerService {

    private final HotelCustomerDetailRepository hotelCustomerDetailRepository;
    private final HotelCustomerDetailMapper hotelCustomerDetailMapper;

    @Override
    public ListResponse<HotelCustomerResponse> findAll(HotelCustomerFilterRequest filterRequest) {
        int page = filterRequest.getPageIndex();
        int size = filterRequest.getPageSize();
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<HotelCustomerDetail> hotelCustomerDetails = hotelCustomerDetailRepository.findAll(pageRequest.previousOrFirst());
        List<HotelCustomerResponse> tourCustomerResponses = hotelCustomerDetails.get().map(hotelCustomerDetailMapper::mapToResponse).collect(Collectors.toList());
        return ListResponse.of(hotelCustomerDetails.getTotalElements(), tourCustomerResponses);
    }
}
