package travelling.api.app.service.admin.impl;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.TourCustomerDetail;
import travelling.api.app.mapper.TourCustomerMapper;
import travelling.api.app.model.request.tourcustomer.TourCustomerFilterRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.tourcustomer.TourCustomerResponse;
import travelling.api.app.repository.TourCustomerDetailRepository;
import travelling.api.app.service.admin.TourCustomerService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class TourCustomerServiceImpl implements TourCustomerService {

    private final TourCustomerDetailRepository tourCustomerDetailRepository;
    private final TourCustomerMapper tourCustomerMapper;

    @Override
    public ListResponse<TourCustomerResponse> findAll(TourCustomerFilterRequest filterRequest) {
        int page = filterRequest.getPageIndex();
        int size = filterRequest.getPageSize();
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<TourCustomerDetail> tourCustomerDetails = tourCustomerDetailRepository.findAll(pageRequest.previousOrFirst());
        List<TourCustomerResponse> tourCustomerResponses = tourCustomerDetails.get().map(tourCustomerMapper::mapToResponse).collect(Collectors.toList());
        return ListResponse.of(tourCustomerDetails.getTotalElements(), tourCustomerResponses);
    }
}
