package travelling.api.app.service.admin.impl;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import travelling.api.app.common.constant.MessageConstant;
import travelling.api.app.entity.Blog;
import travelling.api.app.exception.ObjectNotFoundException;
import travelling.api.app.mapper.BlogMapper;
import travelling.api.app.model.request.blog.BlogFilterRequest;
import travelling.api.app.model.request.blog.BlogSaveRequest;
import travelling.api.app.model.request.blog.BlogUpdateRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.blog.BlogDetailResponse;
import travelling.api.app.model.response.blog.BlogResponse;
import travelling.api.app.repository.BlogRepository;
import travelling.api.app.repository.specification.BlogSpecification;
import travelling.api.app.service.admin.BlogService;
import travelling.api.app.service.admin.UserService;
import travelling.api.security.jwt.model.JwtPayload;

import java.util.List;
import java.util.Optional;

@Service
@Data
public class BlogServiceImpl implements BlogService {

    private final BlogMapper blogMapper;
    private final UserService userService;
    private final BlogRepository blogRepository;


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void save(BlogSaveRequest blogSaveRequest) {
        Blog blog = blogMapper.convertToEntity(blogSaveRequest);
        blogRepository.save(blog);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStatus(BlogUpdateRequest blogUpdateRequest) {
        blogRepository.update(blogUpdateRequest.getIdBlog(), blogUpdateRequest.getStatus());
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public BlogDetailResponse getById(long id) {
//        JwtPayload jwtPayload = userService.parseToken();
//        if (jwtPayload.getRole().contains(RoleConstant.USER)) {
//            throw new AccessDeniedException(MessageConstant.ACCESS_DENIED.value());
//        }
        Optional<Blog> blog = blogRepository.findById(id);
        if (!blog.isPresent()) {
            throw new ObjectNotFoundException(MessageConstant.BLOG_NOT_FOUND.value());
        }
        BlogDetailResponse blogDetailResponse = blogMapper.convertToDetailResponse(blog.get());
        return blogDetailResponse;
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public ListResponse<BlogResponse> getAll(BlogFilterRequest blogFilterRequest) {
        PageRequest pageRequest = PageRequest.of(blogFilterRequest.getPageIndex()-1, 10, Sort.by("id").descending());
        Page<Blog> blogs = blogRepository.findAll(BlogSpecification.filterBlog(blogFilterRequest), pageRequest);
        List<BlogResponse> blogResponses = blogMapper.convertToList(blogMapper::convertResponse, blogs.getContent());
        long count = blogs.getTotalElements();
        Integer totalPage = (int) Math.ceil((double) count / 10);
        return ListResponse.of(totalPage,count, blogResponses);
    }

    @Override
    public void update(BlogUpdateRequest blogUpdateRequest) {
        Blog blog = blogRepository.getOne(blogUpdateRequest.getIdBlog());
        blog.setStatus(blogUpdateRequest.getStatus());
        blog.setContent(blogUpdateRequest.getContent());
        blog.setDescription(blogUpdateRequest.getDescription());
        blog.setKeyword(blogUpdateRequest.getKeyword());
        blog.setThumbnail(blogUpdateRequest.getThumbnail());
        blog.setTitle(blogUpdateRequest.getTitle());
//        blog.setUrl(blogUpdateRequest.getUrl());

        blogRepository.save(blog);
    }

    @Override
    public boolean delete(Long[] id) {
        try {
            for (Long ids : id){
                Blog blog = blogRepository.findById(ids).get();
                if (blog.getStatus().equals("OFF")){
                    blogRepository.deleteById(ids);
                }else {
                    return false;
                }

            }
            return true;
        }catch (Exception e){
            return false;
        }

    }


}
