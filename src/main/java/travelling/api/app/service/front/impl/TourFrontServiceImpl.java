package travelling.api.app.service.front.impl;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.Tour;
import travelling.api.app.mapper.TourMapper;
import travelling.api.app.model.request.tour.TourFilterCategoryRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.tour.TourResponse;
import travelling.api.app.repository.TourRepository;
import travelling.api.app.repository.specification.TourSpecification;
import travelling.api.app.service.front.TourFrontService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class TourFrontServiceImpl implements TourFrontService {

    private final TourRepository tourRepository;
    private final TourMapper tourMapper;

    @Override
    public ListResponse<TourResponse> findAllByTourCategory(TourFilterCategoryRequest tourFilterCategoryRequest) {
        PageRequest pageRequest = PageRequest.of(tourFilterCategoryRequest.getPageIndex(), tourFilterCategoryRequest.getPageSize(), Sort.by("createdDate").descending());
        Page<Tour> page = tourRepository.findAll(TourSpecification.filterCategoryId(tourFilterCategoryRequest.getCategoryId()), pageRequest);
        List<TourResponse> tourResponses = page.stream().map(tourMapper::convertToResponse).collect(Collectors.toList());
        return ListResponse.of(page.getTotalElements(), tourResponses);
    }

    @Override
    public ListResponse<TourResponse> findAllTourRelation(Boolean hot) {
        PageRequest pageRequest = PageRequest.of(1, 6, Sort.by("createdDate").descending());
        Page<Tour> tours = tourRepository.findAll(TourSpecification.filterTourHot(hot), pageRequest.previousOrFirst());
        List<TourResponse> tourResponse = tours.stream().map(tourMapper::convertToResponse).collect(Collectors.toList());
        return ListResponse.of(tours.getTotalElements(), tourResponse);
    }
}
