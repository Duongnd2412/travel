package travelling.api.app.service.front.impl;

import lombok.Data;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.TourCategory;
import travelling.api.app.mapper.TourCategoryMapper;
import travelling.api.app.model.response.tourcategory.TourCategoryResponse;
import travelling.api.app.repository.TourCategoryRepository;
import travelling.api.app.service.front.TourCategoryFrontService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class TourCategoryFrontServiceImpl implements TourCategoryFrontService {

    private final TourCategoryRepository tourCategoryRepository;
    private final TourCategoryMapper tourCategoryMapper;

    @Override
    public List<TourCategoryResponse> findAllByCategoryType() {
        return tourCategoryRepository.findAll()
                .stream()
                .map(items -> tourCategoryMapper.convertToResponse(items))
                .collect(Collectors.toList());
    }
}
