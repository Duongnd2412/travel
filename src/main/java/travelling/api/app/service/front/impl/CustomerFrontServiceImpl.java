package travelling.api.app.service.front.impl;

import lombok.Data;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.Customer;
import travelling.api.app.entity.HotelCustomerDetail;
import travelling.api.app.entity.TourCustomerDetail;
import travelling.api.app.mapper.CustomerMapper;
import travelling.api.app.mapper.HotelCustomerDetailMapper;
import travelling.api.app.mapper.TourCustomerDetailMapper;
import travelling.api.app.model.request.customer.CustomerSaveRequest;
import travelling.api.app.repository.CustomerRepository;
import travelling.api.app.repository.HotelCustomerDetailRepository;
import travelling.api.app.repository.TourCustomerDetailRepository;
import travelling.api.app.service.front.CustomerFrontService;

@Service
@Data
public class CustomerFrontServiceImpl implements CustomerFrontService {

    private final CustomerRepository customerRepository;
    private final TourCustomerDetailRepository tourCustomerDetailRepository;
    private final HotelCustomerDetailRepository hotelCustomerDetailRepository;
    private final CustomerMapper customerMapper;
    private final TourCustomerDetailMapper tourCustomerDetailMapper;
    private final HotelCustomerDetailMapper hotelCustomerDetailMapper;


    @Override
    public void save(CustomerSaveRequest customerSaveRequest) {
        Customer customer = customerMapper.convertToEntity(customerSaveRequest);
        customer.setIsTourCustomer(customerSaveRequest.getIsTourCustomer());
        customer = customerRepository.save(customer);
        if (customerSaveRequest.getTourId() != null){
            TourCustomerDetail tourCustomerDetail = tourCustomerDetailMapper.convertToEntity(customerSaveRequest);
            tourCustomerDetail.setCustomer(customer);
            tourCustomerDetailRepository.save(tourCustomerDetail);
        }else {
            HotelCustomerDetail hotelCustomerDetail = hotelCustomerDetailMapper.convertToEntity(customerSaveRequest);
            hotelCustomerDetail.setCustomer(customer);
            hotelCustomerDetailRepository.save(hotelCustomerDetail);
        }

    }
}
