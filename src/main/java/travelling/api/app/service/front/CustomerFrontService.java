package travelling.api.app.service.front;

import travelling.api.app.model.request.customer.CustomerSaveRequest;

public interface CustomerFrontService {
    void save(CustomerSaveRequest customerSaveRequest);
}
