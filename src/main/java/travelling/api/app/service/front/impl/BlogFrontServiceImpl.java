package travelling.api.app.service.front.impl;

import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import travelling.api.app.entity.Blog;
import travelling.api.app.mapper.BlogMapper;
import travelling.api.app.model.request.BaseFilterRequest;
import travelling.api.app.model.request.customer.CustomerFilter;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.blog.BlogResponse;
import travelling.api.app.repository.BlogRepository;
import travelling.api.app.repository.specification.CustomerSpecification;
import travelling.api.app.service.front.BlogFrontService;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class BlogFrontServiceImpl implements BlogFrontService {

    private final BlogRepository blogRepository;
    private final BlogMapper blogMapper;

    @Override
    public BlogResponse findById(long id) {
        return blogMapper.convertResponse(blogRepository.getOne(id));
    }

    @Override
    public ListResponse<BlogResponse> findAll(BaseFilterRequest filterRequest) {
        Sort sort = Sort.by(Sort.Order.desc("id"));
        Pageable pageable = PageRequest.of(filterRequest.getPageIndex() -1,5,sort);
        List<BlogResponse> blogResponses = blogRepository.findAll(pageable).stream().map(items -> blogMapper.convertResponse(items)).collect(Collectors.toList());
        Long count = blogRepository.count();
        Integer totalPage = (int) Math.ceil((double) count / 5);
        return ListResponse.of(totalPage,count, blogResponses);

    }



    @Override
    public ListResponse<BlogResponse> findRelation(boolean hot) {
        PageRequest pageRequest = PageRequest.of(0, 6, Sort.by("createdDate").descending());
        Page<Blog> blogs = blogRepository.findAll(pageRequest);
        List<BlogResponse> blogResponses = blogs.stream().map(blogMapper::convertResponse).collect(Collectors.toList());
        return ListResponse.of(blogs.getTotalElements(), blogResponses);
    }
}
