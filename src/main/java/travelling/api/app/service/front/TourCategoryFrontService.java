package travelling.api.app.service.front;

import travelling.api.app.model.response.tourcategory.TourCategoryResponse;

import java.util.List;

public interface TourCategoryFrontService {
    List<TourCategoryResponse> findAllByCategoryType();
}
