package travelling.api.app.service.front;

import travelling.api.app.model.request.tour.TourFilterCategoryRequest;
import travelling.api.app.model.response.ListResponse;
import travelling.api.app.model.response.tour.TourResponse;

public interface TourFrontService {

    ListResponse<TourResponse> findAllByTourCategory(TourFilterCategoryRequest tourFilterCategoryRequest);

    ListResponse<TourResponse> findAllTourRelation(Boolean hot);
}
