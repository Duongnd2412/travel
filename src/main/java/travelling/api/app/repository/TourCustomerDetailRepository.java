package travelling.api.app.repository;

import travelling.api.app.entity.TourCustomerDetail;

public interface TourCustomerDetailRepository extends BaseRepository<TourCustomerDetail, Long> {
}
