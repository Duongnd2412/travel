package travelling.api.app.repository;

import org.springframework.stereotype.Repository;
import travelling.api.app.entity.Customer;

@Repository
public interface CustomerRepository extends BaseRepository<Customer, Long> {
}
