package travelling.api.app.repository.specification;


import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import travelling.api.app.entity.Tour;
import travelling.api.app.model.request.tour.TourFilterRequest;

public class TourSpecification {

    public static Specification<Tour> tourFilter(TourFilterRequest tourFilterRequest) {
        return Specification.where(withFromPrice(tourFilterRequest.getFromPrice()))
                .and(withToPrice(tourFilterRequest.getToPrice()))
                .and(withTourType(tourFilterRequest.getTourType()))
                .and(withStatus(tourFilterRequest.getStatus()))
                .and(withCreatedBy(tourFilterRequest.getCreatedBy()))
                .and(withName(tourFilterRequest.getName()))
                .and(withTourCategoryId(tourFilterRequest.getTourCategoryId()))
                .and(withNotId(tourFilterRequest.getId()));
    }

    public static Specification<Tour> filterCategoryId(long categoryId) {
        return Specification.where(withTourCategoryId(categoryId))
                .and(withStatus("ACTIVE"));
    }

    public static Specification<Tour> filterTourHot(Boolean hot) {
        return Specification.where(withHot(hot))
                .and(withStatus("ACTIVE"));
    }

    private static Specification<Tour> withFromPrice(Double fromPrice) {
        if (fromPrice == 0 || fromPrice == null ) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(root.get("price"), fromPrice);
    }

    private static Specification<Tour> withToPrice(Double toPrice) {
        if (toPrice == 0) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(root.get("price"), toPrice);
    }

    private static Specification<Tour> withHot(Boolean hot) {
        if (hot == null) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("isHot"), hot);
    }

    private static Specification<Tour> withTourType(String tourType) {
        if (StringUtils.isBlank(tourType)) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("tourType"), tourType);
    }

    private static Specification<Tour> withStatus(String status) {
        if (StringUtils.isBlank(status)) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("status"), status);
    }

    private static Specification<Tour> withCreatedBy(String createdBy) {
        if (StringUtils.isBlank(createdBy)) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("createdBy"), createdBy);
    }
    private static Specification<Tour> withNotId(Long id) {
        if (id == null) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.notEqual(root.get("id"), id);
    }

    private static Specification<Tour> withName(String name) {
        if (StringUtils.isBlank(name)) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("name"), "%" + name + "%");
    }

    private static Specification<Tour> withTourCategoryId(Long categoryId) {
        if (categoryId == null) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.join("tourCategory").get("id"), categoryId);
    }
}
