package travelling.api.app.repository.specification;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import travelling.api.app.entity.Customer;
import travelling.api.app.model.request.customer.CustomerFilter;

public class CustomerSpecification {

    public static Specification<Customer> filter(CustomerFilter filter) {
        return Specification.where(withFullName(filter.getFullName()))
                .and(Specification.where(withPhone(filter.getPhone())))
                .and(Specification.where(withIsTourCustomer(filter.getIsTourCustomer())));
    }


    private static Specification<Customer> withFullName(String fullName) {
        if (StringUtils.isBlank(fullName)) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("fullName"), "%" + fullName + "%");
    }

    private static Specification<Customer> withPhone(String phone) {
        if (StringUtils.isBlank(phone)) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("phone"),  phone);
    }
    private static Specification<Customer> withIsTourCustomer(Integer isTourCustomer) {
        if (isTourCustomer == null) return null;
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("isTourCustomer"),isTourCustomer);
    }

}
