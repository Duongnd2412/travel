package travelling.api.app.repository.specification;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import travelling.api.app.entity.Hotel;
import travelling.api.app.entity.Tour;
import travelling.api.app.model.request.hotel.HotelFilterRequest;

public class HotelSpecification {
    public static Specification<Hotel> filter(HotelFilterRequest filter) {
        return Specification.where(withName(filter.getName()))
                .and(withLocalType(filter.getLocaleType()))
                .and(withAddress(filter.getAddress()))
                .and(withNotId(filter.getId()));
    }
    public static Specification<Hotel> filterHotelHot(Boolean hot) {
        return Specification.where(withHot(hot));
    }


    private static Specification<Hotel> withName(String name) {
        if (StringUtils.isBlank(name)) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("name"), "%" + name + "%");
    }

    private static Specification<Hotel> withLocalType(Long localType){
        if (localType == null || localType == 0) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("localeType"), localType);
    }

    private static Specification<Hotel> withAddress(String address){
        if (StringUtils.isEmpty(address)) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.like(root.get("address"), "%"+address+"%");
    }
    private static Specification<Hotel> withNotId(Long id){
        if (id == null) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.notEqual(root.get("id"), id);
    }
    private static Specification<Hotel> withHot(Boolean hot) {
        if (hot == null) return null;

        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("isHot"), hot);
    }


}
