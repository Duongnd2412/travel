package travelling.api.app.repository;

import travelling.api.app.entity.HotelCustomerDetail;

public interface HotelCustomerDetailRepository extends BaseRepository<HotelCustomerDetail, Long> {
}
