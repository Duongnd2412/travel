package travelling.api.app.repository;

import travelling.api.app.entity.User;

public interface UserRepository extends BaseRepository<User, Long> {
    long countAllByUserName(String userName);

    long countAllByPhone(String phone);

    long countAllByEmail(String email);
}
