package travelling.api.app.validate;


import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import travelling.api.app.exception.ValidatorException;
import travelling.api.app.model.request.user.UserAuthRequest;
import travelling.api.app.model.request.user.UserChangePasswordRequest;
import travelling.api.app.model.request.user.UserSaveRequest;
import travelling.api.app.repository.UserRepository;


@Component
@Data
public class UserValidate {

    private final UserRepository userRepository;

    public static boolean validateUsername(String username) {
        return StringUtils.isBlank(username);
    }

    public static boolean validateEmail(String email) {
        return StringUtils.isBlank(email);
    }

    public static boolean validatePhone(String phone) {
        return StringUtils.isBlank(phone);
    }

    public static boolean validateUserName(String userName) {
        return StringUtils.isBlank(userName);
    }

    public static boolean validatePassword(String password) {
        return StringUtils.isBlank(password) && password.length() < 6;
    }

    public UserAuthRequest validateUserAuth(UserAuthRequest userAuthRequest) {
        return Validator
                .of(userAuthRequest)
                .validate(UserAuthRequest::getUserName, UserValidate::validateUserName, () -> new ValidatorException("userName not empty"))
                .validate(UserAuthRequest::getPassword, UserValidate::validatePassword, () -> new ValidatorException("password not empty"))
                .get();
    }

    public UserChangePasswordRequest validateChangePassword(UserChangePasswordRequest userChangePasswordRequest) {
        return Validator
                .of(userChangePasswordRequest)
                .validate(UserChangePasswordRequest::getCurrentPassword, UserValidate::validatePassword, () -> new ValidatorException("currentPassword not empty"))
                .validate(UserChangePasswordRequest::getNewPassword, UserValidate::validatePassword, () -> new ValidatorException("newPassword not empty"))
                .get();
    }

    public UserSaveRequest validateUserSaveRequest(UserSaveRequest userSaveRequest) {
        return Validator
                .of(userSaveRequest)
                .validate(UserSaveRequest::getUserName, UserValidate::validateUsername, () -> new ValidatorException("username not empty"))
                .validate(UserSaveRequest::getUserName, s -> validateUserDuplicate(s), () -> new ValidatorException("username existed"))
                .validate(UserSaveRequest::getPhone, UserValidate::validatePhone, () -> new ValidatorException("phone not empty"))
                .validate(UserSaveRequest::getPhone, s -> validatePhone(s), () -> new ValidatorException("phone existed"))
                .validate(UserSaveRequest::getEmail, UserValidate::validateEmail, () -> new ValidatorException("email not empty"))
                .validate(UserSaveRequest::getEmail, s -> validateUserDuplicateEmail(s), () -> new ValidatorException("email existed"))
                .get();
    }

    public boolean validateUserDuplicate(String userName) {
        return userRepository.countAllByUserName(userName) > 0;
    }

    public boolean validateUserDuplicatePhone(String phone) {
        return userRepository.countAllByPhone(phone) > 0;
    }

    public boolean validateUserDuplicateEmail(String email) {
        return userRepository.countAllByEmail(email) > 0;
    }


}
